const schema = {
    username: 'string:min=2,max=100,required'
};

function *set({$api, $configuration, value:{username}, $debug})  {
  const { identity: identityEndpoint } = yield $configuration('endpoints');

  try {
    const response = yield $api.post(`${identityEndpoint}/request-token`, {
      Username: username,
      Portal: 'borrower-portal',
    });
    return response.body;
  } catch(e) {
    $debug(e);
    throw {      
      code: 401,
      "details": [
        {
          "path": "username",
          message: 'Invalid Username'
        }
        ]
    }
  }
};

module.exports = [schema, set];
