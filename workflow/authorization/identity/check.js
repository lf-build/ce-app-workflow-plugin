const schema = {
    username: 'string:min=2,max=100,required'
};

function *set({$api, $configuration, value:{username}, $debug})  {
  const { identity: identityEndpoint } = yield $configuration('endpoints');
  const response = yield $api.post(`${identityEndpoint}/check`, {
    username
  });
  return { 
    username, 
    exists: !response.body.available 
  };
};

module.exports = [schema, set];
