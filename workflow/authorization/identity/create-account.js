const schema = {
    username: 'string:min=2,max=100,required',
    password: 'string:min=2,max=100,required',
    name: 'string:min=2,max=100,required',
    email: 'email:min=2,max=100,required',
    realm: 'string:min=2,max=100',
};

function *set({
  $api,
  $configuration,
  value:{
    username, 
    password, 
    name,
    email,
    realm: role
  }, $debug})  {
  const { identity: identityEndpoint } = yield $configuration('endpoints');
  if(!role){
    role = 'Borrower';
  }
  try{
    const response = yield $api.put(`${identityEndpoint}`, {
      username, 
      password, 
      name,
      email,
      roles: [role]
    });
    return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Invalid Username or Password',
      code: 401,
    }
  }
};

module.exports = [schema, set];
