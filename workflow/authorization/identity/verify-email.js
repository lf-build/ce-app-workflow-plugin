const schema = {
    token: 'string:min=2,max=100,required',
};

function *set({
  $api,
  $configuration,
  value:{
    token,
  },
  $debug})  {
  const { 'email-verification': emailVerificationEndpoint } = yield $configuration('endpoints');
  const response = yield $api.post(`${emailVerificationEndpoint}/verification/${token}`);
  return response.body;
};

module.exports = [schema, set];
