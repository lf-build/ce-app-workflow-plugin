const schema = {
};

function *set({$api, $configuration, $debug})  {
  const { identity: identityEndpoint } = yield $configuration('endpoints');
  const response = yield $api.get(identityEndpoint);
  return response.body;
};

module.exports = [schema, set];
