const {
  dbAction
} = require('@sigma-infosolutions/orbit-base/storage');

const schema = {
  username: 'string:min=2,max=100,required',
  otp: 'string:min=2,max=100,required',
  realm: 'string:min=2,max=100',
};

function* set({
  $api,
  $configuration,
  value: {
    username,
    otp,
    realm: portal
  },
  $debug
}) {

  const {
    identity: identityEndpoint,
    'orbit-identity': orbitIdentityEndpoint
  } = yield $configuration('endpoints');

  if (!portal) {
    portal = 'borrower-portal';
  }

  try {

    const readAllStates = (db) => db
      .collection('store')
      .find({
        'on-boarding.sign-up.mobile.mobileNumber': username
      })
      .sort({
        _id: -1
      })
      .toArray();

    let states = yield dbAction(readAllStates);

    const response = yield $api.post(`${identityEndpoint}/otp/login`, {
      username,
      otp,
      portal,
    });
    // return Object.assign({ realm: portal }, response.body);

    // if (response && response.body && response.body.userId) {

    //   let [persistedState] = states;
    //   const randomNo = states.length == 0 ? undefined : persistedState['on-boarding']['sign-up']['user'].randomNumber;      

    //   let loginResponse;
    //   const rememberMe = true;
    //   if (randomNo) {
    //     $debug('user details from approach 1');
    //     loginResponse = yield $api.post(`${orbitIdentityEndpoint}`, {
    //       client: process.env.ORBIT_IDENTITY_CLIENT,
    //       username,
    //       password: randomNo,
    //       realm: portal,
    //       rememberMe
    //     });
    //   } else {
    //     $debug('user details from approach 2');
    //     loginResponse = yield $api.post(`${orbitIdentityEndpoint}`, {
    //       client: process.env.ORBIT_IDENTITY_CLIENT,
    //       username: process.env.ORBIT_IDENTITY_USER,
    //       password: process.env.ORBIT_IDENTITY_PASSWORD,
    //       realm: portal,
    //       rememberMe
    //     });
    //   }

      return Object.assign({ realm: portal, username }, response.body);
    // }

  } catch (e) {
    $debug(e);
    throw {
      code: 401,
      "details": [{
        "path": "username",
        message: 'Invalid Username or OTP'
      }]
    }
  }
};

module.exports = [schema, set];