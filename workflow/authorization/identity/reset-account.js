const schema = {
    username: 'string:min=2,max=100,required',
    password: 'string:min=2,max=100,required',
    confirmPassword: 'string:min=2,max=100',
    token: 'string:min=2,max=100,required',
};

function *set({
  $api,
  $configuration,
  value:{
    username,
    password,
    token,
  },
  $debug})  {
  const portal = 'borrower-portal';

  const { identity: identityEndpoint } = yield $configuration('endpoints');
  const response = yield $api.post(`${identityEndpoint}/reset-password/${token}`, {
    username,
    password,
    portal,
  });
  return response.body;
};

module.exports = [schema, set];
