const schema = {
  'title': {
    '@type': 'string',
    valid: [
      'Mr', 'Mrs', 'Ms'
    ],
    required: true
  },
  'maritalStatus': {
    '@type': 'string',
    valid: [
      'single', 'married'
    ],
    required: true
  },
  'firstName': 'string: min=1, max=100, required',
  'middleName': 'string: min=1, max=100',
  'lastName': 'string: min=1, max=100, required',
  'personalEmail': 'email: min=2, max=100, required'
};

function* set({
  $api,
  $set,
  $facts: {
    'sign-up': signUp
  },
  $configuration,
  value,
  $debug
}) {
  const {
    'application-filters': applicationFiltersEndpoint
  } = yield $configuration('endpoints');
  let applicationNumber = undefined;

  try {
    const abc = yield signUp.$get();
    const {
      application: {
        applicationNumber: existingApplicationNumber
      }
    } = abc;
    applicationNumber = existingApplicationNumber;
  } catch (e) {
    $debug('in catch, some error occurred ', e);
  }
  return yield $set('basicInformation', value);
};

module.exports = [schema, set];