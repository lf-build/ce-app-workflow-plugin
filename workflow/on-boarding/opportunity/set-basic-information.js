const schema = {
  'title': {
    '@type': 'string',
    valid: [
      'Mr', 'Mrs', 'Ms'
    ],
    required: true
  },
  'maritalStatus': {
    '@type': 'string',
    valid: [
      'single', 'married'
    ],
    required: true
  },
  'firstName': 'string: min=1, max=100, required',
  'middleName': 'string: min=1, max=100',
  'lastName': 'string: min=1, max=100, required',
  'personalEmail': 'email: min=2, max=100, required'
};

function* set({
  $api,
  $set,
  $facts: {
    'sign-up': signUp
  },
  $configuration,
  value,
  $debug
}) {
  const {
    'application-filters': applicationFiltersEndpoint
  } = yield $configuration('endpoints');
  let applicationNumber = undefined;

  try {
    const abc = yield signUp.$get();
    const {
      application: {
        applicationNumber: existingApplicationNumber
      }
    } = abc;
    applicationNumber = existingApplicationNumber;
  } catch (e) {
    $debug('in catch, some error occurred ', e);
  }

  const duplicateApplicationResponse = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Email/${value.personalEmail}`);
  const [duplicateApplication] = duplicateApplicationResponse.body && duplicateApplicationResponse.body.length ? duplicateApplicationResponse.body : [{}];

  // Case 1: filters return bogus (empty) response:- do not enter the if (do not throw error)
  // Case 2: filters return existing application:- 
  //  case 2.1: applicationNumber is undefined (no application associated with current workflow id): enter the if (throw error)
  //  case 2.2: applicationNumber is defined (application is associated with current workflow id)
  //    case 2.2.1: session application number is equal to filter application number :- do not enter the if (do not throw error)
  //    case 2.2.2: session application number is not equal to filter application number :- enter the if (throw error)

  $debug(`filter applicationNumber (${duplicateApplication.applicationNumber}), workflow attached to applicationNumber (${applicationNumber})`);
  if (duplicateApplication.applicationNumber && (!applicationNumber || duplicateApplication.applicationNumber !== applicationNumber)) {
    throw {
      code: 422,
      "details": [{
        "path": "personalEmail",
        message: 'E-mail ID is already registered, please login'
      }]
    };
  }

  return yield $set('basicInformation', value);
};

module.exports = [schema, set];