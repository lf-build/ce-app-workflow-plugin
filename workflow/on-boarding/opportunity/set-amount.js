const schema = {
  amount: 'number:min=50000,max=1500000,required'
};

function* set({
  $set,
  value: {
    amount
  }
}) {
  return yield $set('amount', amount);
};

module.exports = [schema, set];