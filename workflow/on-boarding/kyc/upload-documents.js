var FormData = require('form-data');
const schema = {
    documents: {
        '@items': {
          factName: 'string:min=2,max=100,required',
          methodName: 'string:min=2,max=100,required',
          stipulationType: 'string:min=2,max=100,required',
          file: 'string:min=2,max=100,required',
          dataUrl: 'string:min=2,max=100000000,required',
          type: 'string:min=2,max=100,required',
        },
        max: 5,
        min: 1,
    },
    kycDocument: 'boolean: required',
};
function *set({
  $api, 
  $set, 
  $facts: {
    application,
  },
  value:{
    documents,
  },
  $debug,
  $configuration})  {

  const {
    'verification-engine': verificationEngineEndPoint,
  } = yield $configuration('endpoints');

  // const { application: { applicationNumber } } = yield application.$get();
  const { application: { applicationNumber } } = yield application.$get();

  for(index in documents) {
    const document = documents[index];
    const formData = new FormData();
    formData.append('file', new Buffer(document.dataUrl, 'base64'), document.file);
    const response = yield $api.post(`${verificationEngineEndPoint}/application/${applicationNumber}/${document.factName}/${document.methodName}/upload-document`, formData, null);
    $debug('Document Updalod', response);
  }

  return {
    status: 'success',
  };
};

module.exports = [schema, set];
