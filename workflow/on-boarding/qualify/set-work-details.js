const schema = {
  'employer': {
    cin: 'string: min=2, max=100, required',
    name: 'string: min=2, max=100, required',
  },
  'monthlyTakeHomeSalary': 'number: min=0, max=1000000, required',
  'officialEmail': 'email: max=100, required',
};

function* set({
  $set,
  value: {
    employer,
    monthlyTakeHomeSalary,
    officialEmail
  },
  $api,
  $configuration
}) {

  const {
    'application-processor': applicationProcessEndpoint,
    'company-db': companydbEnpoint
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = {
    application: {
      applicationNumber: '000015'
    }
  }; //yield application.$get();

  if (typeof employer === 'string') {
    employer = {
      name: employer,
      cin: employer,
    }
  }

  const isEmailVerified = yield $api.get(`${applicationProcessEndpoint}/application/verify-email/${officialEmail}`);

  // here we check if Official Email Id is a blacklisted Email Id
  if (isEmailVerified && !isEmailVerified.body) {
    throw {
      code: 422,
      "details": [{
        "path": "officialEmail",
        message: 'Generic company e-mail ID is not allowed'
      }]
    };
  }

  // making request to the DB to get the Employer details, as here we will be having only the name of Employer, matching
  // CIN need to be fetched from DB if the same Employer exists in DB.
  const lookupResponse = yield $api.get(`${companydbEnpoint}/application/${applicationNumber}/company/search/byname/${employer.name}`);
  const employerDetails = lookupResponse.body && lookupResponse.body.companySearchResponse &&
    (lookupResponse.body.companySearchResponse.length ? lookupResponse.body.companySearchResponse : undefined);

  if (employerDetails && employerDetails.length === 1) {
    employer = {
      name: employerDetails[0].name,
      cin: employerDetails[0].cin
    };
  }

  return yield $set('work', {
    employer,
    monthlyTakeHomeSalary,
    officialEmail,
  });
};

module.exports = [schema, set];