const schema = {
    'dateOfBirth': 'string: min=0, max=100, required',
    'panNumber': 'string: min=0, max=100, required',
    'aadhaarNumber': 'string: min=0, max=100, required',

    'currentResidentialAddress': {
        'addressLine1': 'string: min=0, max=100, required',
        'addressLine2': 'string: min=0, max=100',
        'locality': 'string: min=0, max=100, required',
        'pinCode': 'string: min=6, max=6, required',
        'city': 'string: min=0, max=100, required',
        'state': 'string: min=0, max=100, required'
    },

    'permanentResidentialAddressSameAsCurrent': 'boolean: required',

    'permanentResidentialAddress': {
        'addressLine1': 'string: min=0, max=100, required',
        'addressLine2': 'string: min=0, max=100',
        'locality': 'string: min=0, max=100, required',
        'pinCode': 'string: min=6, max=6, required',
        'city': 'string: min=0, max=100, required',
        'state': 'string: min=0, max=100, required'
    },
    'date': 'string: min=1, max=2',
    'month': 'string: min=1, max=2',
    'year': 'string: min=4, max=4',
    'sodexoCode': 'string: min=0, max=100',
    'currentAddressYear': 'string:min=0 max=2',
};

function* set({
    $set,
    $configuration,
    $api,
    value: {
        dateOfBirth,
        panNumber,
        aadhaarNumber,
        currentResidentialAddress,
        permanentResidentialAddressSameAsCurrent,
        permanentResidentialAddress,
        sodexoCode,
        currentAddressYear,
    },
    $facts: {
        'sign-up': signUp
    },
    $get,
    $debug
}) {

    const { 'application-filters': applicationFiltersEndpoint } = yield $configuration('endpoints');

    let applicationNumber = undefined;

    try {
        const abc = yield signUp.$get();
        const { application: {
            applicationNumber: existingApplicationNumber
        } } = abc;
        applicationNumber = existingApplicationNumber;
    } catch (e) {
        $debug('in catch, error occurred while fetch details from sign-up node', e);
    }
    // const duplicateApplicationResponse = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Pan/${panNumber}`);

    // if (duplicateApplicationResponse.body.applicationId) {
    const duplicateApplicationResponse = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Pan/${panNumber}`);
    const [duplicateApplication] = duplicateApplicationResponse.body && duplicateApplicationResponse.body.length ? duplicateApplicationResponse.body : [{}];

    // Case 1: filters return empty response:- do not enter the if (do not throw error)
    // Case 2: filters return existing application:- 
    //  case 2.1: applicationNumber is undefined (no application associated with current workflow id): enter the if (throw error)
    //  case 2.2: applicationNumber is defined (application is associated with current workflow id)
    //    case 2.2.1: session application number is equal to filter application number :- do not enter the if (do not throw error)
    //    case 2.2.2: session application number is not equal to filter application number :- enter the if (throw error)

    $debug(`filter applicationNumber (${duplicateApplication.applicationNumber}), workflow attached to applicationNumber (${applicationNumber})`);
    if (duplicateApplication.applicationNumber && (!applicationNumber || duplicateApplication.applicationNumber !== applicationNumber)) {
        throw {
            code: 422,
            "details": [
                {
                    "path": "panNumber",
                    message: 'PAN Number is already registered, please login'
                }
            ]
        };
    }

    const formatDate = (dt) => {
        const [dd, mm, yyyy] = dt.split('/');
        return `${yyyy}-${mm}-${dd}`;
    }
    const returnValue = yield $set('details', {
        dateOfBirth: formatDate(dateOfBirth),
        panNumber,
        aadhaarNumber,
        currentResidentialAddress,
        permanentResidentialAddressSameAsCurrent,
        permanentResidentialAddress: permanentResidentialAddressSameAsCurrent ? currentResidentialAddress : permanentResidentialAddress,
        sodexoCode,
        currentAddressYear,
    });

    $debug(yield $get());
    return returnValue;
};

module.exports = [schema, set];
