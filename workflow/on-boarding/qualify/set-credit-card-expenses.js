const schema = {    
    'hasCreditCard': 'boolean: required',
    'creditCardOutstanding': 'number: min=0, max=1000000'    
};

function *set({$set, value: { hasCreditCard, creditCardOutstanding }})  {
  if(hasCreditCard && (creditCardOutstanding && creditCardOutstanding < 0)) {
    throw {
      code: '422',
      details:[{"path":"creditCardOutstanding","message":"\"creditCardOutstanding\" must be provided when \"hasCreditCard\" is marked as yes"}]
    };
  }
  hasCreditCard = hasCreditCard || false;

  return yield $set('creditCardExpenses',  { hasCreditCard, creditCardOutstanding: hasCreditCard ? creditCardOutstanding : 0  } );
};

module.exports = [schema, set];
