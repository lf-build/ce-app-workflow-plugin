const schema = {
    // 'monthlyExpenses': 'number:min=0,max=1000000,required'
    'otherEmis': 'number:min=0,max=1000000'
};

function *set({$set, value })  {
  // return yield $set('otherExpenses', value);
  if( !value.otherEmis ) {
    value.otherEmis = 0;
  }
  return yield $set('otherExpenses', value);
};

module.exports = [schema, set];
