const schema = {
  'residenceType': 'string: min=2, max=100, required',
  'hasHomeLoan': 'boolean:',
  'homeLoanEmi': 'number: min=0, max=1000000',
  'monthlyRent': 'number: min=0, max=1000000',
};

function* set({
  $set,
  value: {
    residenceType,
    hasHomeLoan,
    homeLoanEmi,
    monthlyRent
  },
  $debug,
}) {
  if (hasHomeLoan && !homeLoanEmi) {
    throw {
      code: '422',
      details: [{
        "path": "homeLoanEmi",
        "message": "\"homeLoanEmi\" must be provided when \"hasHomeLoan\" is marked as yes"
      }]
    };
  }

  if (hasHomeLoan) {
    $debug('hasHomeLoan YES ', {
      residenceType,
      hasHomeLoan,
      homeLoanEmi,
      monthlyRent: 0
    });
  } else {
    $debug('hasHomeLoan NO ', {
      residenceType,
      hasHomeLoan,
      homeLoanEmi: 0,
      monthlyRent: monthlyRent && monthlyRent !== null ? monthlyRent : 0
    });
  }

  return yield $set('residenceExpenses', hasHomeLoan ? {
    residenceType,
    hasHomeLoan,
    homeLoanEmi,
    monthlyRent: 0
  } : {
    residenceType,
    hasHomeLoan,
    homeLoanEmi: 0,
    monthlyRent: monthlyRent && monthlyRent !== null ? monthlyRent : 0
  });
};

module.exports = [schema, set];