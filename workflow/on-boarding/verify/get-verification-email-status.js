const schema = {};

function* set({
    $api,
    $set,
    $setStatus,
    $configuration,
    $debug,
    $facts: {
        'sign-up': signUp,
    }, }) {

    const { 'data-attribute': dataAttributeEndpoint, } = yield $configuration('endpoints');
    const {
        application: {
            applicationNumber
        }
    } = yield signUp.$get();

    let result = undefined;

    try {
        const response = yield $api.get(`${dataAttributeEndpoint}/application/${applicationNumber}/domainSearched`);
        const { body: { domainMatched } } = response;
        result = domainMatched;
        $debug('Verification email sent ', result);
        return result;
    } catch (e) {
        throw {
            message: e,
            result: false,
        }
    }
};

module.exports = [schema, set];
