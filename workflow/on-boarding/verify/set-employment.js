var FormData = require('form-data');
const schema = {
  designation: 'string:min=2,max=100,required',
  workExperience: 'number:min=0,max=100,required',
  employerAddress: {
    'addressLine1': 'string: min=0, max=100, required',
    'addressLine2': 'string: min=0, max=100',
    'locality': 'string: min=0, max=100, required',
    'pinCode': 'string: min=6, max=6, required',
    'city': 'string: min=0, max=100, required',
    'state': 'string: min=0, max=100, required'
  },
  documents: {
    '@items': {
      factName: 'string:min=2,max=100,required',
      methodName: 'string:min=2,max=100,required',
      stipulationType: 'string:min=2,max=100,required',
      file: 'string:min=2,max=100,required',
      dataUrl: 'string:min=2,max=100000000,required',
      type: 'string:min=2,max=100,required',
    },
    max: 5,
  },
  employementYear: 'string:min=0 max=2',
};

function* set({
  $api,
  $set,
  $setStatus,
  $configuration,
  value: {
    designation,
    workExperience,
    employerAddress,
    documents,
    employementYear,
  },
  $facts: {
    'sign-up': signUp,
  },
  $stages: {
    'on-boarding': {
      application: {
        'get-application-details': getApplicationDetails,
      }
    }
  },
  $debug
}) {
  const workExperienceInMonths = workExperience * 12;


  const {
    'application-processor': applicationProcessEndpoint,
    'verification-engine': verificationEngineEndPoint,
  } = yield $configuration('endpoints');

  const { application: { applicationNumber } } = yield signUp.$get();

  let employmentDetail = {};

  try {
    const response = yield getApplicationDetails.$execute({});
    if (response) {
      const { employmentDetail: empDetails } = response;

      if (empDetails) {
        employmentDetail = empDetails;
      }
    }
  } catch (error) {
    $debug('error occurred while getting application details');
  }

  if (documents) {
    for (index in documents) {
      const document = documents[index];
      const formData = new FormData();
      formData.append('file', new Buffer(document.dataUrl, 'base64'), document.file);
      const response = yield $api.post(`${verificationEngineEndPoint}/application/${applicationNumber}/${document.factName}/${document.methodName}/upload-document`, formData, null);
      $debug('Document Updalod', response);
    }
  }

  employmentDetail.lengthOfEmploymentInMonths = workExperienceInMonths;
  employmentDetail.designation = designation;
  employmentDetail.addresses = employmentDetail.addresses || [{}];
  const [addressDetails] = employmentDetail.addresses;
  employmentDetail.addresses[0] = Object.assign(addressDetails, employerAddress, { location: employerAddress.locality, country: 'India' });
  const payload = employmentDetail;

  const result = yield $api.put(`${applicationProcessEndpoint}/application/${applicationNumber}/employmentDetail/${employementYear}/0`, payload);

  return yield $set('employment', {
    designation,
    workExperience,
    employerAddress,
    employementYear: employementYear,
  });
};

module.exports = [schema, set];
