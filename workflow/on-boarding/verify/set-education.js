const schema = {
  heighestEducation: 'string:min=2,max=100,required',
  educationalInstitution: 'string:min=2,max=100,required',
};

function* set({
  $api,
  $set,
  $setStatus,
  $facts: {
    'sign-up': signUp,
  },
  value: {
    heighestEducation,
    educationalInstitution
  },
  $debug,
  $configuration
}) {
  const {
    'application-processor': applicationProcessEndpoint,
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = yield signUp.$get();

  const result = yield $api.put(`${applicationProcessEndpoint}/application/${applicationNumber}/educationInformation`, {
    "LevelOfEducation": heighestEducation,
    "EducationalInstitution": educationalInstitution,
  });


  return yield $set('education', {
    heighestEducation,
    educationalInstitution
  });
};

module.exports = [schema, set];
