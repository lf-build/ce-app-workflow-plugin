const schema = {};

function* set({
    $api,
    $set,
    $setStatus,
    $configuration,
    $debug,
    $facts: {
        'sign-up': signUp,
        opportunity,
        // qualify,
    } }) {
    const { perfios: perfiosEndpoint } = yield $configuration('endpoints');
    const {
        application: {
            applicationNumber
        }
    } = yield signUp.$get();

    const {
        basicInformation: {
            personalEmail
        },
    } = yield opportunity.$get();

    let form = undefined;

    const startProcessPayload = {
        EmailId: personalEmail,
        ReturnUrl: process.env.RETURN_URL || "http://localhost:3000",
    }

    $debug('start process payload ', startProcessPayload);

    try {
        form = yield $api.post(`${perfiosEndpoint}/application/${applicationNumber}/start-process`, startProcessPayload);
        $debug('Net Banking form fetched');
    } catch (e) {
        $debug(e);
        throw {
            code: '422',
            message: 'error occurred while fetching dynamic form for net banking transaction'
        };
    }

    return form;
};

module.exports = [schema, set];
