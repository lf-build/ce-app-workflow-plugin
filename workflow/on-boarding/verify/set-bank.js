var FormData = require('form-data');
const schema = {
  // bankAccountConnect: 'boolean:required',
  bank: {
    id: 'string:min=1,max=100,required',
    name: 'string:min=2,max=100,required',
    institutionType: 'string:min=2,max=100,required',
  },
  basicInformation: {
    firstName: 'string:min=1,max=100,required',
    middleName: 'string:min=1,max=100',
    lastName: 'string:min=1,max=100,required',
  },
  applicationNumber: 'string:min=2,max=100',
  // accountNumber: 'string:min=2,max=100,required',
  // confirmAccountNumber: 'string:min=2,max=100,required',
  // ifscCode: 'string:min=11,max=11,required',
  // bankAddress: 'string:min=2,max=500,required',
  statements: {
    '@items': {
      file: 'string:min=2,max=100,required',
      dataUrl: 'string:min=2,max=100000000,required',
      type: 'string:min=2,max=100,required',
    },
    max: 6,
    min: 1,
  },
  password: 'string: min=6,max=25',
  fromMonth: 'string: min=1,max=4',
  fromYear: 'string: min=4,max=4',
  toMonth: 'string: min=1,max=4',
  toYear: 'string: min=4,max=4',
};

function* set({
  $api,
  $set,
  $setStatus,
  $facts: {
    'sign-up': signUp,
    opportunity,
  },
  value: {
    bank,
    // accountNumber,
    // confirmAccountNumber,
    // ifscCode,
    statements,
    // bankAddress,
    applicationNumber: suppliedApplicationNumber,
    basicInformation: suppliedBasicInformation,
    password,
    fromMonth,
    fromYear,
    toMonth,
    toYear,
  },
  $debug,
  $configuration
}) {
  // if (accountNumber !== confirmAccountNumber) {
  //   throw {
  //     code: 422,
  //     details: [{ "path": "accountNumber", "message": "\"accountNumber\" and \"confirmAccountNumber\" do not match" }]
  //   };
  // }

  const {
    'application-processor': applicationProcessEndpoint,
    perfios: perfiosEndpoint,
  } = yield $configuration('endpoints');

  const year = (new Date()).getFullYear();
  const month = (new Date()).getMonth() + 1;
  const {
    application: {
      applicationNumber
    }
  } = suppliedApplicationNumber ? {
    application: {
      applicationNumber: suppliedApplicationNumber
    }
  } : yield signUp.$get();

  const {
    basicInformation
  } = suppliedBasicInformation ? {
    basicInformation: suppliedBasicInformation
  } : yield opportunity.$get();
  try {
    yield $api.put(`${applicationProcessEndpoint}/application/${applicationNumber}/bankinformation`, {
      BankName: bank.name,
      // AccountNumber: accountNumber,
      AccountNumber: '',
      AccountHolderName: `${basicInformation.firstName} ${basicInformation.middleName || ' '} ${basicInformation.lastName}`,
      // IfscCode: ifscCode,
      AccountType: 'Savings',
      // BankAddresses: {
      //   AddressType: "Bank",
      //   AddressLine1: bankAddress,
      // }
    });
  } catch (e) {
    $debug('in catch, error occurred while calling /bankinformation ', e);
    throw {
      code: 422,
      status: 'apiError',
      message: e,
    };
  }
  let from = undefined;
  let to = undefined;
  if (fromMonth &&
    fromYear &&
    toMonth &&
    toYear) {
    from = fromYear + '-' + fromMonth;
    to = toYear + '-' + toMonth;
  } else {
    const todaysDt = new Date();
    to = `${todaysDt.getFullYear()}-${todaysDt.getMonth() < 9 ? '0' + (todaysDt.getMonth() + 1) : todaysDt.getMonth() + 1}`;
    const fromDt = todaysDt.setMonth(todaysDt.getMonth() - 7);
    const fDt = new Date(fromDt);
    // const from = `${fDt.getFullYear()}-${fDt.getMonth() < 8 ? '0' + (fDt.getMonth() + 2) : fDt.getMonth() + 2}`;
    if (fDt.getMonth() < 8) {
      from = `${fDt.getFullYear()}-${'0' + (fDt.getMonth() + 2)}`;
    } else {
      // here this if will be executed on in case when from month based on current date is December,
      // this if will handle from date i.e. {year}-{month} - here month will come 13 if not handle by if.
      if (fDt.getMonth() === 11) {
        from = to.split('-')[0] + '-01';
      } else {
        from = `${fDt.getFullYear()}-${(fDt.getMonth() + 2)}`;
      }
    }
  }

  let startTransResponse = undefined;
  try {
    startTransResponse = yield $api.post(`${perfiosEndpoint}/application/${applicationNumber}/start-transaction`, {
      institutionId: bank.id,
      yearMonthFrom: from,
      yearMonthTo: to,
    });
  } catch (e) {
    $debug('in catch, error occurred while calling /start-transaction ', e);
    throw {
      code: 422,
      status: 'apiError',
      message: e,
    };
  }

  $debug('start Trans', startTransResponse);

  try {
    for (index in statements) {
      const statement = statements[index];
      const formData = new FormData();
      formData.append('file', new Buffer(statement.dataUrl.replace('data:application/pdf;base64,', ''), 'base64'), statement.file);

      // if password is provided then we will append password to formData
      if (password) {
        formData.append('password', password);
      }

      // const response = yield $api.post(`http://192.168.1.65:5101/application/${applicationNumber}/upload-statement`, formData, null);
      const response = yield $api.post(`${applicationProcessEndpoint}/application/${applicationNumber}/perfios-upload-statement`, formData, null);
      $debug('Statement Updalod', response);
    }
  } catch (e) {
    $debug('in catch, error occurred while calling /perfios-upload-statement ', e);
    throw {
      code: 422,
      status: 'statementError',
      message: e,
    };
  }

  let completeTransResponse = undefined;
  try {
    completeTransResponse = yield $api.post(`${perfiosEndpoint}/application/${applicationNumber}/complete-transaction`, {});
  } catch (e) {
    $debug('in catch, error occurred while calling /complete-transaction ', e);
    throw {
      code: 422,
      status: 'apiError',
      message: e,
    };
  }

  $debug('finish Trans', completeTransResponse);

  return yield $set('banking', {
    bank,
    // accountNumber,
    // confirmAccountNumber,
    // ifscCode,
  });
};

module.exports = [schema, set];