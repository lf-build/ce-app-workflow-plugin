var FormData = require('form-data');
const schema = {
    bank: {
        id: 'string:min=1,max=100,required',
        name: 'string:min=2,max=100,required',
        institutionType: 'string:min=2,max=100,required',
    },
    basicInformation: {
        firstName: 'string:min=1,max=100',
        middleName: 'string:min=1,max=100',
        lastName: 'string:min=1,max=100',
    },
    applicationNumber: 'string:min=2,max=100',
    accountNumber: 'string:min=2,max=100,required',
    confirmAccountNumber: 'string:min=2,max=100,required',
    ifscCode: 'string:min=11,max=11',
    bankAddress: 'string:min=2,max=500'
};
function* set({
  $api,
    $set,
    $setStatus,
    $facts: {
    application,
        opportunity,
  },
    value: {
        bank,
        accountNumber,
        confirmAccountNumber,
        ifscCode,
        bankAddress,
        applicationNumber: suppliedApplicationNumber,
        basicInformation: suppliedBasicInformation
  },
    $debug,
    $configuration }) {
    if (accountNumber !== confirmAccountNumber) {
        throw {
            code: 422,
            details: [{ "path": "accountNumber", "message": "\"accountNumber\" and \"confirmAccountNumber\" do not match" }]
        };
    }

    const { 'application-processor': applicationProcessEndpoint } = yield $configuration('endpoints');

    const { application: { applicationNumber } } = suppliedApplicationNumber ? {
        application: {
            applicationNumber: suppliedApplicationNumber
        }
    } : yield application.$get();

    const { basicInformation } = suppliedBasicInformation ? { basicInformation: suppliedBasicInformation } : yield opportunity.$get();

    try {
        yield $api.put(`${applicationProcessEndpoint}/application/${applicationNumber}/bankinformation`, {
            BankName: bank.name,
            AccountNumber: accountNumber,
            AccountHolderName: `${basicInformation.firstName} ${basicInformation.middleName || ' '} ${basicInformation.lastName}`,
            IfscCode: ifscCode || undefined,
            AccountType: 'Savings',
            BankAddresses: ifscCode ? {
                AddressType: "Bank",
                AddressLine1: bankAddress,
            } : undefined
        });
    } catch (e) {
        $debug('in catch, error occurred while calling /bankinformation ', e);
        throw {
            code: 422,
            status: 'apiError',
            message: e,
        };
    }

    return yield $set('banking', {
        bank,
        accountNumber,
        confirmAccountNumber,
        ifscCode,
    });
};

module.exports = [schema, set];
