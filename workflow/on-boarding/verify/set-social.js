const schema = {};

function* set({
  $api,
  $facts: {
    'sign-up': signUp,
  },
  $configuration,
  $debug,
  $set
}) {
  const {
    lendo: lendoEndpoint,
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = yield signUp.$get();

  try {

    $debug(`${lendoEndpoint}/application/${applicationNumber}/${applicationNumber}/score`);
    const lendoResponse = yield $api.get(`${lendoEndpoint}/application/${applicationNumber}/${applicationNumber}/score`);

    $debug('Lendo initialized');
    // TODO: Need to find some other solution as when to set this information in Workflow DB.
    // const socialResponse = {
    //   status: 'success',
    //   response: lendoResponse.body
    // }
    // $set('social', socialResponse);

    return lendoResponse.body;
  } catch (e) {
    $debug('Lendo not initialied / failed', e);
    throw {
      code: 404
    };
  }
};

module.exports = [schema, set];