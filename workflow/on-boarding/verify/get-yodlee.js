const schema = {
    amount: 'number:min=50000,max=500000,required'
};

function *set({ $api, $set, $setStatus, $configuration, $debug })  {
  yield $setStatus({amount});
  return {amount};
};

module.exports = [schema, set];
