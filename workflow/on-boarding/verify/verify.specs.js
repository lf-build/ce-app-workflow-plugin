const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('Verify', () => {
  describe('set-bank', () => {
    const moduleToTest = require('./set-bank');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-education', () => {
    const moduleToTest = require('./set-education');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-employment', () => {
    const moduleToTest = require('./set-employment');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-social', () => {
    const moduleToTest = require('./set-social');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
