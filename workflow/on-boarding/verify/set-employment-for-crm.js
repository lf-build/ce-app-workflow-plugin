const schema = {
    designation: 'string:min=2,max=100,required',
    workExperience: 'number:min=0,max=100,required',
    employerAddress: {
        'addressLine1': 'string: min=0, max=100, required',
        'addressLine2': 'string: min=0, max=100',
        'locality': 'string: min=0, max=100, required',
        'pinCode': 'string: min=6, max=6, required',
        'city': 'string: min=0, max=100, required',
        'state': 'string: min=0, max=100, required'
    },
    currentAddressYear: 'string:min=0 max=2',
};

function* set({
    $api,
    $set,
    $setStatus,
    $configuration,
    value: {
        designation,
        workExperience,
        employerAddress,
        currentAddressYear,
    },
    $facts: {
        application,
    },
    $debug
}) {
    const workExperienceInMonths = workExperience * 12;

    const {
        application: {
            applicationNumber,
            employmentDetail
        }
    } = yield application.$get();

    return yield $set('employment', {
        designation,
        workExperienceInMonths,
        employerAddress,
        employementYear: currentAddressYear,
    });
};

module.exports = [schema, set];