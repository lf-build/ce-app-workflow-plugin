const schema = {
    heighestEducation: 'string:min=2,max=100,required',
    educationalInstitution: 'string:min=2,max=100,required',
};

function* set({
    $api,
    $set,
    $setStatus,
    $facts: {
        application,
    },
    value: {
        heighestEducation,
        educationalInstitution
    },
    $debug,
    $configuration
}) {

    const {
        application: {
            applicationNumber
        }
    } = yield application.$get();

    return yield $set('education', {
        heighestEducation,
        educationalInstitution
    });
};

module.exports = [schema, set];