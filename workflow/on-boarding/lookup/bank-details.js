const schema = {
  ifscCode: 'string:min=11,max=11,required'
};

function* set({
  $api,
  value: {
    ifscCode,
  },
  $debug
}) {

  try {
    // making an unsecure call here using the below parameter, as faced certificate issue from the 3rd party
    // and resetting after the call is completed.
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const lookupResponse = yield $api.get(`https://ifsc.razorpay.com/${ifscCode}`);
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "1";
    
    lookupResponse.body.ADDRESS = lookupResponse.body.ADDRESS.match(/[a-zA-Z0-9 ,.\/]*/g).filter((m) => m).join('');
    return lookupResponse.body && Object.assign({
      address: `${lookupResponse.body.ADDRESS}${lookupResponse.body.ADDRESS.toLowerCase().indexOf(lookupResponse.body.CITY.toLowerCase()) === -1 ? `, ${lookupResponse.body.CITY}` : ''}${lookupResponse.body.ADDRESS.toLowerCase().indexOf(lookupResponse.body.STATE.toLowerCase()) === -1 ? `, ${lookupResponse.body.STATE}` : ''}`,
    }, lookupResponse.body);
  } catch (error) {
    throw {
      code: 424,
      error
    };
  }
};

module.exports = [schema, set];
