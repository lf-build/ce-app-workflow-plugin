const apiFactory = require("@sigma-infosolutions/orbit/api")
const api = apiFactory()
const schema = {
  searchText: 'string:min=0,max=200,required'
};

function* set({
  $api,
  value: {
    searchText,
  },
  $facts: {
    application,
  },
  $debug,
  $configuration
}) {
  const {
    'company-db': companydbEnpoint,
    'search-api': searchApi
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = {
    application: {
      applicationNumber: '000015'
    }
  }; //yield application.$get();
  try {
    // const lookupResponse = yield $api.get(`${companydbEnpoint}/application/${applicationNumber}/company/search/byname/${searchText}`);
    // return lookupResponse.body && lookupResponse.body.companySearchResponse && (lookupResponse.body.companySearchResponse.length ? lookupResponse.body.companySearchResponse : [{
    //   cin: searchText.toUpperCase(),
    //   name: searchText.toUpperCase()
    // }]);

    const searchTexts = searchText.split(' ');


    let searchApiCustQuery = `(and (or(prefix '${searchText}')'${searchText}')(or ''(prefix '')))`;
    if (searchTexts.length > 1) {
      searchApiCustQuery = `(and (or(prefix '${searchTexts[0]}')'${searchTexts[0]}') (or '${searchTexts[1]}' (prefix '${searchTexts[1]}')))`;
    }
    const autoCompleteApiResponse = yield api.get(`${searchApi}${searchApiCustQuery}`);
    const res = autoCompleteApiResponse.body.hits.hit.map((company) => ({
      cin: company.fields.name,
      name: company.fields.name,
    }));
    return res && (res.length > 0 ? res : [{
      cin: searchText.toUpperCase(),
      name: searchText.toUpperCase()
    }]);

  } catch (error) {
    throw {
      code: 424,
      error
    };
  }
};

module.exports = [schema, set];