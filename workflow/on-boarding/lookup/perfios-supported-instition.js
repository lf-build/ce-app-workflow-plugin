const banks = require('./banklookup');
const schema = {};

function* set({
  $api,
  $debug,
  $configuration
}) {

  try {
    // const {
    //   perfios: perfiosEndpoint
    // } = yield $configuration('endpoints');
    // $debug(`${perfiosEndpoint}/supported-instition`);
    // return (yield $api.get(`${perfiosEndpoint}/supported-instition`)).body.instiutions;

    return banks.instiutions;
  } catch (error) {
    throw {
      code: 424,
      error
    };
  }
};

module.exports = [schema, set];