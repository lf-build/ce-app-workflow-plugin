const schema = {
  pincode: 'string:min=6,max=6,required'
};
const fs = require('fs');
const path = require('path');

const csv = fs.readFileSync(path.join(__dirname, 'pincodes.csv'), 'utf8').replace(/\r/g,'').split('\n');

const pincodes = csv.reduce((a, c) => { 
	const [city, state, pin] = c.split(',');
    if(a[pin] === undefined) {
      a[pin] = {
        state: state.toLowerCase(),
        cities: []
      };
    }
    a[pin].cities.push(city.toLowerCase());
    return a;
}, {});

function *set({
  value: {
    pincode,
  },
  $debug
})  {

  try {
    const pincodeDetails = pincodes[pincode.match(/.{1,6}/g)[0]];
    if(pincodeDetails !== undefined) {
      return pincodeDetails;
    }
    throw {
      code: 404,
      message: `City,State mapping not found for ${pincode}`
    };
  } catch (error) {
    throw {
      code: 424,
      error
    };
  }
};

module.exports = [schema, set];
