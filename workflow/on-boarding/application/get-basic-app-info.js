const schema = {};

function* set({
    $api,
    $set,
    $stages: {
        'on-boarding': {
            application: {
                'get-application-details': getApplicationDetails,
            }
        }
    },
    $facts: {
        opportunity,
        'sign-up': signUp
    },
    value,
    $debug
}) {

    try {
        const {
            basicInformation: {
                title,
                maritalStatus,
                firstName,
                middleName,
                lastName,
                personalEmail
            }
        } = yield opportunity.$get();

        const {
            mobile: {
                mobileNumber,
            },
            application: {
                applicationNumber,
                applicantId
            }
        } = yield signUp.$get();


        const appInfo = {
            applicantId,
            firstName,
            middleName,
            lastName,
            applicationNumber,
            personalEmail,
            mobileNumber
        };

        try {
            const response = yield getApplicationDetails.$execute({});
            if (response) {
                const { employmentDetail } = response;

                if (employmentDetail) {
                    const { name } = employmentDetail;
                    appInfo.employerName = name;
                } else {
                    appInfo.employerName = '';
                }
            }
        } catch (error) {
            $debug('error occurred while getting application details');
        }

        return appInfo;
    } catch (e) {
        $debug(e);
        throw {
            code: 422,
            message: e
        }
    }
};

module.exports = [schema, set];