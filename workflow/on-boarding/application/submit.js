const SHA256 = require('crypto-js/sha256');

const schema = {
  ip: {
    '@type': 'string',
    regex: /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/g,
  }
};

function* set({
  $api,
  $facts: {
    opportunity,
    'sign-up': signUp,
    qualify
  },
  value: {
    ip
  },
  $configuration,
  $debug,
  $set
}) {

  // return {   "applicantId": "58a3168b63128f0007da48f7",   "applicationNumber":
  // "0000107",   "type": "lead" };
  const {
    amount,
    reason: {
      reason,
      reasonText
    },
    basicInformation: {
      title,
      maritalStatus,
      firstName,
      middleName,
      lastName,
      personalEmail
    }
  } = yield opportunity.$get();

  const {
    signupStatus: {
      username,
      exsists
    },
    mobile: {
      mobileNumber,
      verification: {
        status: mobileVerificationStatus,
        date: mobileVerificationDate
      }
    },
    user,
    application: {
      applicationNumber,
      trackingCode,
      trackingCodeMedium,
      trackingCodeCampaign,
      trackingCodeTerm,
      trackingCodeContent,
      applicantId,
      GCLId
    }
  } = yield signUp.$get();

  const {
    work: {
      employer: {
        name: employerName,
        cin
      },
      monthlyTakeHomeSalary,
      officialEmail
    },
    residenceExpenses: {
      residenceType,
      hasHomeLoan,
      homeLoanEmi,
      monthlyRent
    },
    // creditCardExpenses: {
    //   hasCreditCard,
    //   creditCardOutstanding
    // },
    // otherExpenses: {
    //   // monthlyExpenses,
    //   otherEmis
    // },
    details: {
      dateOfBirth,
      aadhaarNumber,
      panNumber,
      currentResidentialAddress: {
        locality: currentLocality,
        addressLine1: currentAddressLine1,
        addressLine2: currentAddressLine2,
        pinCode: currentPinCode,
        city: currentCity,
        state: currentState
      },
      permanentResidentialAddressSameAsCurrent,
      permanentResidentialAddress: {
        locality: permanentLocality,
        addressLine1: permanentAddressLine1,
        addressLine2: permanentAddressLine2,
        pinCode: permanentPinCode,
        city: permanentCity,
        state: permanentState
      },
      sodexoCode,
      currentAddressYear,
    }
  } = yield qualify.$get();

  const payload = {
    applicationNumber,
    purposeOfLoan: reason,
    OtherPurposeDescription: reasonText,
    requestedAmount: amount,
    requestedTermType: 'Monthly',
    requestedTermValue: '12.5',
    aadhaarNumber: aadhaarNumber,
    dateOfBirth: dateOfBirth,
    salutation: title,
    firstName: firstName,
    middleName: middleName,
    lastName: lastName,
    gender: title === 'Mr' ?
      'Male' :
      'Female',
    maritalStatus: maritalStatus,
    userName: '', //username,
    password: '!@#123qwe',
    userId: user.id,
    permanentAccountNumber: panNumber,
    personalMobile: mobileNumber,
    isMobileVerified: mobileVerificationStatus,
    mobileVerificationTime: mobileVerificationDate,
    mobileVerificationNotes: 'Testing',
    personalEmail: personalEmail,
    residenceType: residenceType,
    educationalInstitution: null,
    levelOfEducation: null,
    currentAddressLine1: currentAddressLine1,
    currentAddressLine2: currentAddressLine2,
    currentAddressLine3: null,
    currentAddressLine4: null,
    currentCity: currentCity,
    currentCountry: 'India',
    currentLandMark: null,
    currentLocation: currentLocality,
    currentPinCode: currentPinCode,
    currentState: currentState,
    permanentAddressLine1: permanentAddressLine1,
    permanentAddressLine2: permanentAddressLine2,
    permanentAddressLine3: null,
    permanentAddressLine4: null,
    permanentCity: permanentCity,
    permanentCountry: 'India',
    permanenttLandMark: null,
    permanentLocation: permanentLocality,
    permanentPinCode: permanentPinCode,
    permanentState: permanentState,
    employmentAsOfDate: null,
    cinNumber: cin,
    // designation: 'Manager',
    employmentStatus: 'Salaried',
    // lengthOfEmploymentInMonths: 60,
    employerName: employerName,
    workEmail: officialEmail,
    income: monthlyTakeHomeSalary,
    paymentFrequency: 'Monthly',
    workAddressLine1: null,
    workAddressLine2: null,
    workAddressLine3: null,
    workAddressLine4: null,
    workCity: null,
    workCountry: "India",
    workLandMark: null,
    workLocation: null,
    workPinCode: null,
    workState: null,
    // creditCardBalances: creditCardOutstanding,   -- commented as per CE-1370
    // debtPayments: (homeLoanEmi || 0) + (otherEmis || 0),   -- commented as per CE-1370
    debtPayments: (homeLoanEmi || 0),
    monthlyExpenses: 0, // monthlyExpenses
    monthlyRent: monthlyRent && monthlyRent !== null ? monthlyRent : 0, // in case when home loan is No or Company accomodation then setting monthly rent as 0
    sourceReferenceId: 'Organic',
    sourceType: 'Organic',
    systemChannel: 'BorrowerPortal',
    trackingCode: trackingCode,
    trackingCodeMedium: trackingCodeMedium,
    trackingCodeCampaign: trackingCodeCampaign,
    trackingCodeTerm: trackingCodeTerm,
    trackingCodeContent: trackingCodeContent,
    GCLId: GCLId,
    sodexoCode,
    currentAddressYear: currentAddressYear,
  };

  const {
    'application-processor': applicationProcessorEndpoint,
    consent: consentEndpoint, //http://192.168.1.65:5208
  } = yield $configuration('endpoints');

  try {
    $debug('monthly rent in update application payload ', payload.monthlyRent);
    $debug(`calling - ${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`);
    const appResponse = yield $api.put(`${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`, payload);
    const returnValue = {
      type: appResponse.status.code === 202 ?
        'lead' :
        'eligible',
      application: appResponse.body
    };
    $debug(`completed calling - ${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`);
    $debug('setting /update-application response in workflow');
    yield $set(returnValue);
    $debug('completed setting /update-application response in workflow');

    // Sign consent
    const dt = new Date();
    const hindustanDt = new Date(((dt.getTime() + (dt.getTimezoneOffset() * 60000)) + 330 * 60000));

    const [dd,
      mm,
      yyyy
    ] = [
        hindustanDt.getDate(),
        hindustanDt.getMonth() + 1,
        hindustanDt.getFullYear()
      ];
    const [hh,
      mn
    ] = [
        hindustanDt.getHours(),
        hindustanDt.getMinutes()
      ];

    $debug('making call to consent');
    $debug(`Details from /update-application response : 
          ApplicationNumber - ${returnValue.application.applicationNumber},
          ApplicantId - ${returnValue.application.applicantId}`);

    $debug(`Details from workflow : 
          ApplicationNumber - ${applicationNumber},
          ApplicantId - ${applicantId}`);

    const applicantIdForConsent = returnValue.application.applicantId || applicantId;

    const consentResponse = yield $api.post(`${consentEndpoint}/application/${returnValue.application.applicationNumber}/applicationsubmitconsent/sign`, {
      IPAddress: ip,
      SignedBy: applicantIdForConsent,
      firstName,
      middleName,
      lastName,
      mobileNumber,
      generatedDate: `${dd}-${mm}-${yyyy}`,
      generatedTime: `${hh}:${mn}`
    });

    $debug('consent signed.', consentResponse);

    // Application has to take care or not generating offer if type is lead.

    /* Just for DEV purpose
      if(appResponse.status.code === 202) {
        try {
          const appResponse = yield $api.post(`http://192.168.1.65:6002/application/${returnValue.application.applicationNumber}/200.02`);
        returnValue.type = 'eligible';
        } catch (error) {
        }
      }

    */

    const dobForTM = dateOfBirth.replace(/-/g, '');

    $debug('calling threatmetrix endpoint: https://h-api.online-metrix.net/api/session-query');
    const res = yield $api.get(`https://h-api.online-metrix.net/api/session-query?
        org_id=1dgdm0rx
        &api_key=stivy9ugpvfpryx0
        &session_id=LApp${applicationNumber}
        &service_type=session-policy
        &event_type=ACCOUNT_CREATION
        &input_ip_address=${ip}
        &transaction_id=${mobileNumber}
        &account_login=${mobileNumber}
        &account_telephone=${mobileNumber}
        &account_name=${firstName} ${middleName || ' '} ${lastName}
        &account_date_of_birth=${dobForTM}
        &drivers_licence_number_hash=${SHA256(panNumber).toString()}
        &ssn_hash=${SHA256(aadhaarNumber).toString()}
        &account_email=${personalEmail}
        &local_attrib_1=${officialEmail}
        &local_attrib_2=${employerName}
        &account_address_street1=${currentAddressLine1}
        &account_address_street2=${currentAddressLine2}
        &account_address_city=${currentCity}
        &account_address_zip=${currentPinCode}
        &account_address_country=IN
        &shipping_address_street1=${permanentAddressLine1}
        &shipping_address_street2=${permanentAddressLine2}
        &shipping_address_city=${permanentCity}
        &shipping_address_country=IN
        &shipping_address_zip=${permanentPinCode}`);

    $debug('threat metrix response is ', res);
    $debug('completed calling threat matrix');

    return {
      applicantId: returnValue.application.applicantId,
      firstName,
      middleName,
      lastName,
      applicationNumber: returnValue.application.applicationNumber,
      dateOfBirth: dateOfBirth,
      personalEmail,
      mobileNumber: mobileNumber,
      employerName,
      type: returnValue.type
    };
  } catch (error) {
    throw {
      code: 424,
      error,
      payload
    };
  }

};

module.exports = [schema, set];