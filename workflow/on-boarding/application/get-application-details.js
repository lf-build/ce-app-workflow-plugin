const schema = {
    applicationNumber: 'string: min=2, max=100'
};

function* set({
    $api,
    $set,
    $get,
    $facts: {
        'sign-up': signUp
    },
    value,
    $debug,
    $configuration
}) {

    const {
        'application': applicationEndPoint
    } = yield $configuration('endpoints');
    
    const {
        application: {
            applicationNumber
        }
    } = value.applicationNumber ? {
        application: {
            applicationNumber: value.applicationNumber
        } 
    } : yield signUp.$get();

    try {
        $debug(`getting application details for ${applicationNumber}`)
        const response = yield $api.get(`${applicationEndPoint}/${applicationNumber}`);
        return response && response.body;
    } catch (error) {
        throw {
            code: 400,
            error
        };
    }
};

module.exports = [schema, set];