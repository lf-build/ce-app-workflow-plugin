const schema = {
    sourceReferenceId: 'string: min=1,max=250',
    sourceType: 'string: min=1,max=250',
    systemChannel: 'string: min=1,max=250',
    heighestEducation: 'string:min=2,max=100,required',
    educationalInstitution: 'string:min=2,max=100,required',
    designation: 'string:min=2,max=100,required',
    workExperience: 'number:min=0,max=100,required',
    employerAddress: {
        'addressLine1': 'string: min=0, max=100, required',
        'addressLine2': 'string: min=0, max=100',
        'locality': 'string: min=0, max=100, required',
        'pinCode': 'string: min=6, max=6, required',
        'city': 'string: min=0, max=100, required',
        'state': 'string: min=0, max=100, required'
    },
    employementYear: 'string:min=0 max=2',
    currentAddressYear: 'string:min=0 max=2',
};

function* set({
    value,
    $api,
    $facts: {
        opportunity,
        'sign-up': signUp,
        qualify
    },
    value: {
        sourceReferenceId,
        sourceType,
        systemChannel,
        heighestEducation,
        educationalInstitution,
        designation,
        workExperience,
        employerAddress,
        employementYear,
    },
    $configuration,
    $debug,
    $set
}) {

    const {
        amount,
        reason: {
            reason,
            reasonText
        },
        basicInformation: {
            title,
            maritalStatus,
            firstName,
            middleName,
            lastName,
            personalEmail
        }
    } = yield opportunity.$get();

    const {
        signupStatus: {
            username,
            exists
        },
        mobile: {
            mobileNumber,
            verification: {
                status: mobileVerificationStatus,
                date: mobileVerificationDate
            }
        },
        user,
        application: {
            applicationNumber,
            trackingCode,
            trackingCodeMedium,
            trackingCodeCampaign,
            trackingCodeTerm,
            trackingCodeContent,
            applicantId
        }
    } = yield signUp.$get();

    const {
        work: {
            employer: {
                name: employerName,
                cin
            },
            monthlyTakeHomeSalary,
            officialEmail
        },
        residenceExpenses: {
            residenceType,
            hasHomeLoan,
            homeLoanEmi,
            monthlyRent
        },
        details: {
            dateOfBirth,
            aadhaarNumber,
            panNumber,
            currentResidentialAddress: {
                locality: currentLocality,
                addressLine1: currentAddressLine1,
                addressLine2: currentAddressLine2,
                pinCode: currentPinCode,
                city: currentCity,
                state: currentState
            },
            permanentResidentialAddressSameAsCurrent,
            permanentResidentialAddress: {
                locality: permanentLocality,
                addressLine1: permanentAddressLine1,
                addressLine2: permanentAddressLine2,
                pinCode: permanentPinCode,
                city: permanentCity,
                state: permanentState
            },
            sodexoCode,
            currentAddressYear,
        }
    } = yield qualify.$get();

    const payload = {
        applicationNumber,
        purposeOfLoan: reason,
        OtherPurposeDescription: reasonText,
        requestedAmount: amount,
        requestedTermType: 'Monthly',
        requestedTermValue: '12.5',
        aadhaarNumber: aadhaarNumber,
        dateOfBirth: dateOfBirth,
        salutation: title,
        firstName: firstName,
        middleName: middleName,
        lastName: lastName,
        gender: title === 'Mr' ?
            'Male' : 'Female',
        maritalStatus: maritalStatus,
        userName: '', //username,
        password: '!@#123qwe',
        userId: user.id,
        permanentAccountNumber: panNumber,
        personalMobile: mobileNumber,
        isMobileVerified: mobileVerificationStatus,
        mobileVerificationTime: mobileVerificationDate,
        mobileVerificationNotes: 'Testing',
        personalEmail: personalEmail,
        residenceType: residenceType,
        educationalInstitution: educationalInstitution,
        levelOfEducation: heighestEducation,
        currentAddressLine1: currentAddressLine1,
        currentAddressLine2: currentAddressLine2,
        currentAddressLine3: null,
        currentAddressLine4: null,
        currentCity: currentCity,
        currentCountry: 'India',
        currentLandMark: null,
        currentLocation: currentLocality,
        currentPinCode: currentPinCode,
        currentState: currentState,
        permanentAddressLine1: permanentAddressLine1,
        permanentAddressLine2: permanentAddressLine2,
        permanentAddressLine3: null,
        permanentAddressLine4: null,
        permanentCity: permanentCity,
        permanentCountry: 'India',
        permanenttLandMark: null,
        permanentLocation: permanentLocality,
        permanentPinCode: permanentPinCode,
        permanentState: permanentState,
        employmentAsOfDate: null,
        cinNumber: cin,
        designation: designation,
        employmentStatus: 'Salaried',
        lengthOfEmploymentInMonths: workExperience * 12,
        employerName: employerName,
        workEmail: officialEmail,
        income: monthlyTakeHomeSalary,
        paymentFrequency: 'Monthly',
        workAddressLine1: employerAddress.addressLine1,
        workAddressLine2: employerAddress.addressLine2,
        workAddressLine3: null,
        workAddressLine4: null,
        workCity: employerAddress.city,
        workCountry: "India",
        workLandMark: null,
        workLocation: employerAddress.locality,
        workPinCode: employerAddress.pinCode,
        workState: employerAddress.state,
        debtPayments: (homeLoanEmi || 0),
        monthlyExpenses: 0, // monthlyExpenses
        monthlyRent: monthlyRent && monthlyRent !== null ? monthlyRent : 0, // in case when home loan is No or Company accomodation then setting monthly rent as 0
        sourceReferenceId: sourceReferenceId,
        sourceType: sourceType,
        systemChannel: systemChannel,
        trackingCode: trackingCode,
        trackingCodeMedium: trackingCodeMedium,
        trackingCodeCampaign: trackingCodeCampaign,
        trackingCodeTerm: trackingCodeTerm,
        trackingCodeContent: trackingCodeContent,
        sodexoCode,
        employementYear,
        currentAddressYear: (value.currentAddressYear || currentAddressYear),
    };

    const {
        'application-processor': applicationProcessorEndpoint
    } = yield $configuration('endpoints');

    try {

        $debug('monthly rent in update application payload ', payload.monthlyRent);
        $debug(`calling - ${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`);

        const appResponse = yield $api.put(`${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`, payload);

        const returnValue = {
            type: appResponse.status.code === 202 ?
                'lead' : 'eligible',
            application: appResponse.body
        };

        $debug(`completed calling - ${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`);

        $debug('setting /update-application response in workflow');
        yield $set(returnValue);
        $debug('completed setting /update-application response in workflow');

        return returnValue.application;

    } catch (error) {
        throw error;
    }

};

module.exports = [schema, set];