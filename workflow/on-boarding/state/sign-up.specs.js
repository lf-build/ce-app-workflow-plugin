const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('State', () => {
  describe('restore', () => {
    const moduleToTest = require('./restore');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
