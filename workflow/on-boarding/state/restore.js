const { dbAction } = require('@sigma-infosolutions/orbit-base/storage');
const buildPhaseOneState = (fromStore) => {

  const retval = {
    form: {
      amount: {
        values: {
          amount: fromStore['on-boarding'].opportunity.amount,
          amountField: fromStore['on-boarding'].opportunity.amount
        }
      },
      reason: {
        values: {
          reason: fromStore['on-boarding'].opportunity.reason.reason,
          reasonText: fromStore['on-boarding'].opportunity.reason.reasonText
        }
      },
      'basic-information': {
        values: fromStore['on-boarding'].opportunity.basicInformation
      },
      mobile: {
        values: {
          mobileNumber: fromStore['on-boarding']['sign-up'].mobile.mobileNumber
        }
      }
    },
    route: {
      locationBeforeTransitions: {
        pathname: '/',
        search: '',
        hash: '',
        action: 'REPLACE',
        key: 'touagv',
        query: {}
      }
    },
    reasonPage: {
      selected: fromStore['on-boarding'].opportunity.reason.reason
    },
    language: {
      locale: 'en-IN'
    },
    applicationProgress: {
      mlevel1: 0,
      mlevel2: 0,
      level1: 0,
      level2: 0,
      level3: 0
    }
  };

  if (!(fromStore['on-boarding'].qualify && fromStore['on-boarding'].qualify.work)) {
    // if (!(fromStore['on-boarding']['sign-up'] && fromStore['on-boarding']['sign-up'].user)) {
    //   retval.route.locationBeforeTransitions.pathname = '/application/create-account';
    //   retval.applicationProgress = {
    //     mlevel1: 1,
    //     mlevel2: 1,
    //     level1: 1,
    //     level2: 1,
    //     level3: 0
    //   };
    // } else {
      retval.route.locationBeforeTransitions.pathname = '/application/work-details';
      retval.applicationProgress = {
        mlevel1: 1,
        mlevel2: 2,
        level1: 1,
        level2: 2,
        level3: 0
      };
    // }
    return retval;
  }
  retval.form['work-details'] = {
    values: fromStore['on-boarding'].qualify.work
  };

  if (!(fromStore['on-boarding'].qualify.residenceExpenses)) {
    retval.route.locationBeforeTransitions.pathname = '/application/residence-expense-details';
    retval.applicationProgress = {
      mlevel1: 1,
      mlevel2: 3,
      level1: 1,
      level2: 3,
      level3: 0
    };
    return retval;
  }
  retval.form['residence-expense-details'] = {
    values: fromStore['on-boarding'].qualify.residenceExpenses
  };

  // if (!(fromStore['on-boarding'].qualify.creditCardExpenses)) {
  //   retval.route.locationBeforeTransitions.pathname = '/application/credit-card-expense-details';
  //   retval.applicationProgress = {
  //     mlevel1: 1,
  //     mlevel2: 3,
  //     level1: 1,
  //     level2: 3,
  //     level3: 1
  //   };
  //   return retval;
  // }
  // retval.form['credit-card-expense-details'] = {
  //   values: fromStore['on-boarding'].qualify.creditCardExpenses
  // };

  // if (!(fromStore['on-boarding'].qualify.otherExpenses)) {
  //   retval.route.locationBeforeTransitions.pathname = '/application/other-expense-details';
  //   retval.applicationProgress = {
  //     mlevel1: 1,
  //     mlevel2: 3,
  //     level1: 1,
  //     level2: 3,
  //     level3: 2
  //   };
  //   return retval;
  // }
  // retval.form['other-expense-details'] = {
  //   values: fromStore['on-boarding'].qualify.otherExpenses
  // };

  if (!(fromStore['on-boarding'].qualify.details)) {
    retval.route.locationBeforeTransitions.pathname = '/application/personal-details';
    retval.applicationProgress = {
      mlevel1: 1,
      mlevel2: 4,
      level1: 1,
      level2: 4,
      level3: 0
    };
    return retval;
  }

  const parsedDob = new Date(fromStore['on-boarding'].qualify.details.dateOfBirth);
  const date = (parsedDob.getDate() < 10
    ? '0'
    : '') + parsedDob.getDate();
  const month = ((parsedDob.getMonth() + 1) < 10
    ? '0'
    : '') + (parsedDob.getMonth() + 1);
  fromStore['on-boarding'].qualify.details.dateOfBirth = `${date}/${month}/${parsedDob.getFullYear()}`;
  fromStore['on-boarding'].qualify.details.aadhaarNumber = fromStore['on-boarding'].qualify.details.aadhaarNumber
    ? fromStore['on-boarding']
      .qualify
      .details
      .aadhaarNumber
      .match(/.{1,4}/g)
      .join(' ')
    : undefined;

    fromStore['on-boarding'].qualify.details.sodexoCode = fromStore['on-boarding'].qualify.details.sodexoCode
    ? fromStore['on-boarding']
      .qualify
      .details
      .sodexoCode
    : undefined;

  const perDetails = Object.assign({}, fromStore['on-boarding'].qualify.details, { date: date, month: month, year: parsedDob.getFullYear() });
  retval.form['personal-details'] = {
    values: perDetails
  };

  retval.route.locationBeforeTransitions.pathname = '/application/review-application';
  retval.applicationProgress = {
    mlevel1: 1,
    mlevel2: 5,
    level1: 1,
    level2: 5,
    level3: 0
  };

  retval.app = Object.assign(retval.app || {}, { reviewPage: 'personalDetailsPage' });

  if (fromStore['on-boarding'].verify) {
    if (fromStore['on-boarding'].verify.banking) {
      retval.form['banking'] = {
        values: fromStore['on-boarding'].verify.banking
      }
    }

    if (fromStore['on-boarding'].verify.education) {
      retval.form['education'] = {
        values: fromStore['on-boarding'].verify.education
      }
    }

    if (fromStore['on-boarding'].verify.employment) {
      retval.form['employment'] = {
        values: fromStore['on-boarding'].verify.employment
      }
    }

    if (fromStore['on-boarding'].verify.social) {
      retval.form['social'] = {
        values: fromStore['on-boarding'].verify.social
      }
    }
  }

  return retval;
};

const schema = {
  flow: 'string: min=2, max=20, required',
  mobileNumber: {
    '@type': 'string',
    'regex': /^[6-9][0-9]{9}$/,
    required: true
  }
};

function* set({
  $api,
  value: {
    flow,
    mobileNumber
  },
  $stages: {
    'on-boarding': {
      'sign-up': {
        'submit-lead': submitLead
      }
    }
  },
  $configuration,
  $debug
}) {

  const { 'application-filters': applicationFiltersEndpoint } = yield $configuration('endpoints');

  // There can be a chance when the workflow db's store will have more than 1 entry with same mobile number
  // so to handle such cases we have added .sort({ _id: -1 }) here -1 will sort in decending order i.e. latest 
  // entry will come first
  const readAllStates = (db) => db
    .collection('store')
    .find({ 'on-boarding.sign-up.mobile.mobileNumber': mobileNumber })
    .sort({ _id: -1 })
    .toArray();
  let states = yield dbAction(readAllStates);

  if (states.length === 0) {
    throw { code: 404, message: `mobileNumber: ${mobileNumber} to state mapping not found` }
  }

  let [persistedState] = states;
  console.log(persistedState._id);
  if (flow === 'RE-APPLY') {
    // all methods starting $$ are RAW versions, need to pass request ID
    // (workflowId) to load the workflow session.    
    yield submitLead
      .$$execute
      .bind(undefined, {
        requestId: persistedState._id,
        api: $api
      })({});
    states = yield dbAction(readAllStates);
    persistedState = states[0];
  }

  const state = buildPhaseOneState(persistedState);
  // If application exists meaning application was submitted and we are in phase
  // two now! $debug(persistedState['on-boarding']['sign-up'].application)
  if (persistedState['on-boarding'].application || persistedState['on-boarding']['sign-up'].application) {
    const {
      type,
      application: {
        applicationNumber,
        applicantId
      }
    } = persistedState['on-boarding']['sign-up'] || persistedState['on-boarding'].application;

    $debug(applicationNumber, applicantId);

    const { item2: backendVersion, item3: {
        tags
      } } = (yield $api.get(`${applicationFiltersEndpoint}/searchforattribute/ApplicationNumber/${applicationNumber}`)).body;
    const { statusName: applicationStatus } = backendVersion;
    const hasTag = (tag) => tags.filter((t) => t.tagName === tag).length > 0;
    // $debug(state.form); Fill application related state.
    if (persistedState['on-boarding'].application) {
      const [dd,
        mm,
        yyyy] = state
          .form['personal-details']
          .values
          .dateOfBirth
          .split('/');
      const dateOfBirth = new Date((new Date(yyyy, mm, dd)).getTime() + (330 * 60000));

      state.app = {
        reviewPage: 'personalDetailsPage',
        status: 'submited',
        application: {
          applicantId,
          firstName: state.form['basic-information'].values.firstName,
          middleName: state.form['basic-information'].values.middleName,
          lastName: state.form['basic-information'].values.lastName,
          applicationNumber,
          dateOfBirth: dateOfBirth
            .toISOString()
            .split('T')[0],
          personalEmail: state.form['basic-information'].values.personalEmail,
          mobileNumber,
          employerName: state.form['work-details'].values.employer.name,
          type
        }
      };

    }

    if (persistedState['on-boarding'] && persistedState['on-boarding'].offer && persistedState['on-boarding'].offer.initial && persistedState['on-boarding'].offer.initial.offers && persistedState['on-boarding'].offer.initial.offers.length) {
      if (!state.app) {
        state.app = {};
      }
      state.app.initialOffer = persistedState['on-boarding'].offer.initial.offers[0];
    }
    
    $debug('Application Stautus is : ', applicationStatus);
    
    switch (applicationStatus) {
      case 'Lead Created':
        if (state.app && state.app.reviewPage) {
          state.app = {
            "reviewPage": state.app.reviewPage,
            "status": "submited"
          };
        }
        break;
      case 'Rejected':
        state.route.locationBeforeTransitions.pathname = '/application/application-rejected';
        state.applicationProgress = {
          mlevel1: 1,
          mlevel2: 9,
          level1: 1,
          level2: 9,
          level3: 0
        };
        break;
      case 'Credit Review':
        state.route.locationBeforeTransitions.pathname = '/application/application-under-review';
        state.applicationProgress = {
          mlevel1: 1,
          mlevel2: 8,
          level1: 1,
          level2: 8,
          level3: 0
        };

        if (hasTag('Bank Pending')) {
          state.route.locationBeforeTransitions.pathname = '/application/banking';
          state.applicationProgress = {
            mlevel1: 2,
            mlevel2: 0,
            level1: 2,
            level2: 0,
            level3: 0
          };
        } 

        // Commenting because: In case of Manual Review (Application Status: Credit Review), there might be a possibility
        // that Bank Linking might reinitiate so borrower would be taken to banking page in such scenario.
        // else {
        //   if (!state.form.education) {
        //     state.route.locationBeforeTransitions.pathname = '/application/education';
        //     state.applicationProgress = {
        //       mlevel1: 2,
        //       mlevel2: 1,
        //       level1: 2,
        //       level2: 1,
        //       level3: 0
        //     };

        //   } else if (!state.form.employment) {
        //     state.route.locationBeforeTransitions.pathname = '/application/employment';
        //     state.applicationProgress = {
        //       mlevel1: 2,
        //       mlevel2: 2,
        //       level1: 2,
        //       level2: 2,
        //       level3: 0
        //     };

        //   }
        // }

        break;
      case 'Not Interested':
        state.route.locationBeforeTransitions.pathname = '/application/not-interested';
        state.applicationProgress = {
          mlevel1: 1,
          mlevel2: 8,
          level1: 1,
          level2: 8,
          level3: 0
        };

        break;
      case 'Application Submitted':
      case 'Initial Offer Given':
        state.route.locationBeforeTransitions.pathname = '/application/initial-offer';
        state.applicationProgress = {
          mlevel1: 1,
          mlevel2: 8,
          level1: 1,
          level2: 8,
          level3: 0
        };

        break;
      case 'Initial Offer Selected':
        if (hasTag('Bank Pending')) {
          state.route.locationBeforeTransitions.pathname = '/application/banking';
          state.applicationProgress = {
            mlevel1: 2,
            mlevel2: 0,
            level1: 2,
            level2: 0,
            level3: 0
          };
        } else {
          if (hasTag('Social Pending') || (!state.form.education && !state.form.employment)) {
            state.route.locationBeforeTransitions.pathname = '/application/social';
            state.applicationProgress = {
              mlevel1: 2,
              mlevel2: 1,
              level1: 2,
              level2: 1,
              level3: 0
            };            
          }
          else if (!state.form.education) {
            state.route.locationBeforeTransitions.pathname = '/application/education';
            state.applicationProgress = {
              mlevel1: 2,
              mlevel2: 2,
              level1: 2,
              level2: 2,
              level3: 0
            };  
          } else {
            state.route.locationBeforeTransitions.pathname = '/application/employment';
            state.applicationProgress = {
              mlevel1: 2,
              mlevel2: 3,
              level1: 2,
              level2: 3,
              level3: 0
            };
          }
          // else {
          //   state.route.locationBeforeTransitions.pathname = '/application/social';
          //   state.applicationProgress = {
          //     mlevel1: 2,
          //     mlevel2: 3,
          //     level1: 2,
          //     level2: 3,
          //     level3: 0
          //   };

          // }
        }
        break;
      case 'Final offer made':
        state.route.locationBeforeTransitions.pathname = '/application/final-offer';
        state.applicationProgress = {
          mlevel1: 2,
          mlevel2: 4,
          level1: 2,
          level2: 4,
          level3: 0
        };
        break;
      case 'Expired':
        state.route.locationBeforeTransitions.pathname = '/user/application-expired';
        state.applicationProgress = {
          level1: 2,
          level2: 4,
          level3: 0
        };
        break;
      case 'Final offer Accepted':
        if (hasTag('Bank Pending')) {
          state.route.locationBeforeTransitions.pathname = '/application/banking';
          state.applicationProgress = {
            mlevel1: 2,
            mlevel2: 0,
            level1: 2,
            level2: 0,
            level3: 0
          };
        } else
        // if(hasTag('Pending Documents')) {
        // state.route.locationBeforeTransitions.pathname = '/application/kyc';
        // state.applicationProgress = {     level1: 3,     level2: 0,     level3: 0 };
        // } else
        {
          state.route.locationBeforeTransitions.pathname = '/application/application-loan-agreement';
          state.applicationProgress = {
            mlevel1: 3,
            mlevel2: 1,
            level1: 3,
            level2: 1,
            level3: 0
          };
        }
        break;
      case 'CE Approved':        
      case 'Bank Fraud Review':
      case 'Bank Credit Review':
      case 'Approved':
          state.route.locationBeforeTransitions.pathname = '/application/application-loan-agreement';
          state.applicationProgress = {
            mlevel1: 3,
            mlevel2: 1,
            level1: 3,
            level2: 1,
            level3: 0
          };        
        break;
      case 'Funded':
        state.route.locationBeforeTransitions.pathname = '/application/funded';
        state.applicationProgress = {
          mlevel1: 3,
          mlevel2: 1,
          level1: 3,
          level2: 1,
          level3: 0
        };
      default:
        break;
    }

    $debug(applicationStatus, tags, hasTag('Pending Bank Linking'));
  }

  return { state: state, workflowId: states[0]._id };
};

module.exports = [schema, set];
