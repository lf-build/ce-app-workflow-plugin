const schema = {};

function * set({
  $api,
  $get,
  $set,
  $setStatus,
  $facts: {
    'opportunity': opportunity,
  },
  $configuration,
  $debug
}) {
  const { mobileNumber } = yield $get('mobile');
  const { basicInformation: { personalEmail } } = yield opportunity.$get();
  const {otp: otpEndpoint} = yield $configuration('endpoints');
  const {'otp-verification': refNo, attempts} = yield $get();
  
  if (process.env.OTP_MODE && process.env.OTP_MODE === 'LIVE') {    
    if (attempts >= 3) {      
      throw {
        code: 422,
        maxAttemptsReached: true,      
        message: 'Resend OTP is allowed only 3 times'
      };
    }
  }

  try {    
    if (process.env.OTP_MODE && process.env.OTP_MODE === 'LIVE') {
      yield $set('attempts', attempts + 1);
      var data = {};
      if (process.env.EMAIL_OTP && process.env.EMAIL_OTP === 'EMAIL'){
        data = {Email: `${personalEmail}`,"RetryType": "voice"};
      }
      const otpResponse = yield $api.post(`${otpEndpoint}/application/${mobileNumber}`, {Phone: `+91${mobileNumber}`, Data: data,"isResend": true});
      yield $set('otp-verification', otpResponse.body.referenceNumber);
    } else {
      $debug(`OTP SIMULATED. - ${mobileNumber}`);
    }
  } catch (error) {
      throw {
        code: 422,
        message: 'OTP expired or did not match'
      };
  }
};

module.exports = [schema, set];
