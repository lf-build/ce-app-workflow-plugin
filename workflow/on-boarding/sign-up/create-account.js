const schema = {
  password: 'string: min=6,max=25,required',
  confirmPassword: 'string: min=6,max=25,required',
  utmId: 'string: min=1,max=250'
};

function * set({
  $api,
  $set,
  $get,
  value: {
    password,
    confirmPassword,
    utmId
  },
  $facts: {
    opportunity
  },
  $stages: {
    authorization: {
      identity: {
        'create-account': createAccount,
        login
      }
    }
  },
  $debug,
  $configuration
}) {

  if (password !== confirmPassword) {
    throw {
      code: '422',
      details: [
        {
          "path": "password",
          "message": "\"password\" and \"confirmPasswords\" do not match"
        }
      ]
    };
  }
  const {mobileNumber} = yield $get('mobile');

  const {'application-processor': applicationProcessorEndpoint} = yield $configuration('endpoints');
  const {
    amount,
    reason: {
      reason,
      reasonText
    },
    basicInformation: {
      title,
      maritalStatus,
      firstName,
      middleName,
      lastName,
      personalEmail
    }
  } = yield opportunity.$get();

  const {
    mobile: {
      verification: {
        status: mobileVerificationStatus,
        date: mobileVerificationDate
      }
    }
  } = yield $get();
  // const {   firstName,   middleName,   lastName,   personalEmail,  } = yield
  // opportunity.$get('basicInformation');

  const newUser = yield createAccount.$execute({username: mobileNumber, password: password, name: `${firstName} ${lastName}`, email: personalEmail});
  yield $set('user', newUser);

  const loginDetails = yield login.$execute({username: mobileNumber, password});

  const applicationSubmitPayload = {
    purposeOfLoan: reason,
    OtherPurposeDescription: reasonText,
    requestedAmount: amount,
    personalMobile: mobileNumber,
    isMobileVerified: mobileVerificationStatus,
    mobileVerificationTime: mobileVerificationDate,
    firstName: firstName,
    middleName: middleName,
    lastName: lastName,
    personalEmail,
    userId: newUser.id,

    // CE-1131: BorrowerPortal: Need to set few parameters values at the time of lead creation.
    gender: title === 'Mr'
      ? 'Male'
      : 'Female',
    salutation: title,
    maritalStatus: maritalStatus,
    sourceReferenceId: 'Organic',
    sourceType: 'Organic',
    systemChannel: 'BorrowerPortal',
    trackingCode: utmId || 'BorrowerPortal',
    paymentFrequency: 'Monthly',
    requestedTermType: 'Monthly',
    requestedTermValue: '12.5',
    employmentStatus: 'Salaried'
  };
  const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/submit`, applicationSubmitPayload);
  const parsedApplicationObject = Object.assign(applicationSubmitPayload, {
    applicationNumber: appResponse.body.applicationNumber,
    applicantId: appResponse.body.applicantId
  });
  yield $set('application', parsedApplicationObject);

  return Object.assign(loginDetails, {application: parsedApplicationObject});
};

module.exports = [schema, set];
