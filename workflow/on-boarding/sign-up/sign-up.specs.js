const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('sign-up', () => {
  describe('create-account', () => {
    const moduleToTest = require('./create-account');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('send-otp', () => {
    const moduleToTest = require('./send-otp');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-mobile', () => {
    const moduleToTest = require('./set-mobile');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('verify-otp', () => {
    const moduleToTest = require('./verify-otp');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
