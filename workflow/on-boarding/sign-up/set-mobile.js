const schema = {
  mobileNumber: {
    '@type': 'string',
    'regex': /^[6-9][0-9]{9}$/,
    required: true
  }
};

function * set({
  $api,
  $set,
  value: {
    mobileNumber
  },
  $facts: {
    'opportunity': opportunity,
  },
  $stages: {
    authorization: {
      identity: {
        check
      }
    }
  },
  $configuration,
  $debug
}) {
  const signupStatus = yield check.$execute({username: mobileNumber});
  $debug(signupStatus);
  yield $set('signupStatus', signupStatus);
  if (signupStatus.exists) {
    $debug(`User already exists: ${mobileNumber}`);
    throw {
      code: 422,
      "details": [
        {
          "path": "mobileNumber",
          message: 'Mobile Number is already registered, please login to continue.'
        }
      ]
    };
  }

  const {
    simulation: simulationEndpoint, otp: otpEndpoint,
    // 'application-filters': applicationFiltersEndpoint
  } = yield $configuration('endpoints');


  const { basicInformation: { personalEmail } } = yield opportunity.$get();
  // const duplicateApplicationResponse = yield
  // $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Mobile/${mobileNum
  // ber}`); $debug(duplicateApplicationResponse.body); if
  // (duplicateApplicationResponse.body.applicationId) {   throw {     code: 422,
  //    "details": [       {         "path": "mobileNumber",         message:
  // 'Mobile Number is already registered'       }     ]   }; }

  if (process.env.OTP_MODE && process.env.OTP_MODE === 'LIVE') {
    var data = {};
    if (process.env.EMAIL_OTP && process.env.EMAIL_OTP === 'EMAIL'){
          data = {Email: `${personalEmail}`};
    }
    const otpResponse = yield $api.post(`${otpEndpoint}/application/${mobileNumber}`, {Phone: `+91${mobileNumber}`, Data: data});

    yield $set('otp-verification', otpResponse.body.referenceNumber);
    yield $set('attempts', 0);
  } else {
    $debug(`OTP SIMULATED. - ${mobileNumber}`);
  }
 

  try {
    const payload = {
      "SearchParameters": {
        "//DCRequest/Fields/Field[@TelePhoneNumber1]": "Value('" + mobileNumber + "')"

      },
      "MinimumMatchRequiredForSearch": 1,
      "IsXml": true,
      "Document": "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><Execu" +
                  "teXMLStringResponse xmlns=\"http://tempuri.org/\"><ExecuteXMLStringResult>" +
                  "&lt;DCResponse&gt;&lt;Status&gt;Success&lt;/Status&gt;&lt;Authentication&gt;&lt;" +
                  "Status&gt;Success&lt;/Status&gt;&lt;Token&gt;15c502b3-2897-4573-9da6-b4d84aa5f30" +
                  "8&lt;/Token&gt;&lt;/Authentication&gt;&lt;ResponseInfo&gt;&lt;ApplicationId&gt;" +
                  "11784848&lt;/ApplicationId&gt;&lt;SolutionSetInstanceId&gt;74292e54-7f74-4a03-af0c" +
                  "-baaacef67605&lt;/SolutionSetInstanceId&gt;&lt;CurrentQueue&gt;&lt;/CurrentQueue&gt;" +
                  "&lt;/ResponseInfo&gt;&lt;ContextData&gt;&lt;Field key=\"Applicants\"&gt;&amp;lt;" +
                  "Applicants&amp;gt;&#xD; &amp;lt;Applicant&amp;gt;&#xD; &amp;lt;Addresses&amp;gt;" +
                  "&#xD; &amp;lt;Address&amp;gt;&#xD; &amp;lt;StateCode&amp;gt;27&amp;lt;/StateCode" +
                  "&amp;gt;&#xD; &amp;lt;ResidenceType&amp;gt;01&amp;lt;/ResidenceType&amp;gt;&#xD; &amp;" +
                  "lt;PinCode&amp;gt;400050&amp;lt;/PinCode&amp;gt;&#xD; &amp;lt;City&amp;gt;MUMBAI&amp;" +
                  "lt;/City&amp;gt;&#xD; &amp;lt;AddressType&amp;gt;01&amp;lt;/AddressType&amp;gt;&#xD; &amp;" +
                  "lt;AddressLine5&amp;gt;MUMBAI&amp;lt;/AddressLine5&amp;gt;&#xD; &amp;lt;AddressLine4&amp;gt;" +
                  "1st kurukku   street ,&amp;lt;/AddressLine4&amp;gt;&#xD; &amp;lt;AddressLine3&amp;gt;" +
                  "N.177 mahalakshmi nagar &amp;lt;/AddressLine3&amp;gt;&#xD; &amp;lt;AddressLine2&amp;" +
                  "gt; PAM VILLE DMONTE PARK &amp;lt;/AddressLine2&amp;gt;&#xD; &amp;lt;AddressLine1&amp;gt;" +
                  "501 FIFTH FLOOR&amp;lt;/AddressLine1&amp;gt;&#xD; &amp;lt;/Address&amp;gt;&#xD; &amp;lt;" +
                  "/Addresses&amp;gt;&#xD; &amp;lt;Telephones&amp;gt;&#xD; &amp;lt;Telephone&amp;gt;&#xD; &amp;" +
                  "lt;TelephoneType&amp;gt;01&amp;lt;/TelephoneType&amp;gt;&#xD; &amp;lt;TelephoneNumber&amp;gt;" +
                  "9012783067&amp;lt;/TelephoneNumber&amp;gt;&#xD; &amp;lt;TelephoneExtension/&amp;gt;&#xD; &amp;" +
                  "lt;/Telephone&amp;gt;&#xD; &amp;lt;/Telephones&amp;gt;&#xD; &amp;lt;Identifiers&amp;gt;&#xD; &amp;lt;" +
                  "Identifier&amp;gt;&#xD; &amp;lt;IdType&amp;gt;01&amp;lt;/IdType&amp;gt;&#xD; &amp;lt;IdNumber&amp;" +
                  "gt;ADZPR4147H&amp;lt;/IdNumber&amp;gt;&#xD; &amp;lt;/Identifier&amp;gt;&#xD; &amp;lt;/Identifiers" +
                  "&amp;gt;&#xD; &amp;lt;Gender&amp;gt;MALE&amp;lt;/Gender&amp;gt;&#xD; &amp;lt;DateOfBirth&amp;" +
                  "gt;01051966&amp;lt;/DateOfBirth&amp;gt;&#xD; &amp;lt;ApplicantLastName&amp;gt;RAJAGOPALAN&amp;" +
                  "lt;/ApplicantLastName&amp;gt;&#xD; &amp;lt;ApplicantMiddleName&amp;gt;K&amp;lt;/ApplicantMiddleName&amp;" +
                  "gt;&#xD; &amp;lt;ApplicantFirstName&amp;gt;MABEL&amp;lt;/ApplicantFirstName&amp;gt;&#xD; &amp;lt;" +
                  "ApplicantType&amp;gt;Main&amp;lt;/ApplicantType&amp;gt;&#xD; &amp;lt;DsCibilBureau&amp;gt;&#xD; &amp;lt;" +
                  "Response&amp;gt;&#xD; &amp;lt;CibilBureauResponse&amp;gt;&#xD; &amp;lt;BureauResponseRaw&amp;gt;&amp;lt;" +
                  "/BureauResponseRaw&amp;gt;&#xD; &amp;lt;BureauResponseXml&amp;gt;&amp;amp;lt;CreditReport&amp;amp;" +
                  "gt; 	&amp;amp;lt;Header&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;TUEF&amp;amp;lt;/SegmentTag&amp;" +
                  "amp;gt;	&amp;amp;lt;Version&amp;amp;gt;12&amp;amp;lt;/Version&amp;amp;gt;	&amp;amp;lt;ReferenceNumber" +
                  "&amp;amp;gt;10064324&amp;amp;lt;/ReferenceNumber&amp;amp;gt;	&amp;amp;lt;MemberCode&amp;amp;gt;" +
                  "BP03291005_DCTST              &amp;amp;lt;/MemberCode&amp;amp;gt;	&amp;amp;lt;SubjectReturnCode&amp;amp;gt;" +
                  "1&amp;amp;lt;/SubjectReturnCode&amp;amp;gt;	&amp;amp;lt;EnquiryControlNumber&amp;amp;gt;002148411006" +
                  "&amp;amp;lt;/EnquiryControlNumber&amp;amp;gt;	&amp;amp;lt;DateProcessed&amp;amp;gt;10022018&amp;amp;" +
                  "lt;/DateProcessed&amp;amp;gt;	&amp;amp;lt;TimeProcessed&amp;amp;gt;150200&amp;amp;lt;/TimeProcessed" +
                  "&amp;amp;gt; 	&amp;amp;lt;/Header&amp;amp;gt; 	&amp;amp;lt;NameSegment&amp;amp;gt;	&amp;amp;lt;Length" +
                  "&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;N01&amp;amp;lt;/SegmentTag" +
                  "&amp;amp;gt;	&amp;amp;lt;ConsumerName1FieldLength&amp;amp;gt;05&amp;amp;lt;/ConsumerName1FieldLength&amp;" +
                  "amp;gt;	&amp;amp;lt;ConsumerName1&amp;amp;gt;MABEL&amp;amp;lt;/ConsumerName1&amp;amp;gt;	&amp;amp;" +
                  "lt;ConsumerName2FieldLength&amp;amp;gt;02&amp;amp;lt;/ConsumerName2FieldLength&amp;amp;gt;	&amp;amp;lt;" +
                  "ConsumerName2&amp;amp;gt;KR&amp;amp;lt;/ConsumerName2&amp;amp;gt;	&amp;amp;lt;ConsumerName3FieldLength" +
                  "&amp;amp;gt;11&amp;amp;lt;/ConsumerName3FieldLength&amp;amp;gt;	&amp;amp;lt;ConsumerName3&amp;amp;gt;" +
                  "RAJAGOPALAN&amp;amp;lt;/ConsumerName3&amp;amp;gt;	&amp;amp;lt;DateOfBirthFieldLength&amp;amp;gt;08&amp;" +
                  "amp;lt;/DateOfBirthFieldLength&amp;amp;gt;	&amp;amp;lt;DateOfBirth&amp;amp;gt;01051966&amp;amp;lt;" +
                  "/DateOfBirth&amp;amp;gt;	&amp;amp;lt;GenderFieldLength&amp;amp;gt;01&amp;amp;lt;/GenderFieldLength&amp;" +
                  "amp;gt;	&amp;amp;lt;Gender&amp;amp;gt;1&amp;amp;lt;/Gender&amp;amp;gt; 	&amp;amp;lt;/NameSegment&amp;" +
                  "amp;gt; 	&amp;amp;lt;IDSegment&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;" +
                  "gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;I01&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;IDType&amp;amp;" +
                  "gt;01&amp;amp;lt;/IDType&amp;amp;gt;	&amp;amp;lt;IDNumberFieldLength&amp;amp;gt;10&amp;amp;lt;" +
                  "/IDNumberFieldLength&amp;amp;gt;	&amp;amp;lt;IDNumber&amp;amp;gt;ADZPR4147H&amp;amp;lt;/IDNumber&amp;amp;" +
                  "gt; 	&amp;amp;lt;/IDSegment&amp;amp;gt; 	&amp;amp;lt;IDSegment&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;" +
                  "gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;I02&amp;amp;lt;/SegmentTag&amp;" +
                  "amp;gt;	&amp;amp;lt;IDType&amp;amp;gt;03&amp;amp;lt;/IDType&amp;amp;gt;	&amp;amp;lt;IDNumberFieldLength" +
                  "&amp;amp;gt;10&amp;amp;lt;/IDNumberFieldLength&amp;amp;gt;	&amp;amp;lt;IDNumber&amp;amp;gt;URH0817686&amp;" +
                  "amp;lt;/IDNumber&amp;amp;gt; 	&amp;amp;lt;/IDSegment&amp;amp;gt; 	&amp;amp;lt;TelephoneSegment&amp;amp;" +
                  "gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;" +
                  "T01&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;TelephoneNumberFieldLength&amp;amp;gt;10&amp;amp;lt;" +
                  "/TelephoneNumberFieldLength&amp;amp;gt;	&amp;amp;lt;TelephoneNumber&amp;amp;gt;9863454554&amp;amp;lt;" +
                  "/TelephoneNumber&amp;amp;gt;	&amp;amp;lt;TelephoneExtensionFieldLength&amp;amp;gt;06&amp;amp;lt;" +
                  "/TelephoneExtensionFieldLength&amp;amp;gt;	&amp;amp;lt;TelephoneExtension&amp;amp;gt;234243&amp;amp;lt;" +
                  "/TelephoneExtension&amp;amp;gt;	&amp;amp;lt;TelephoneType&amp;amp;gt;01&amp;amp;lt;/TelephoneType&amp;amp;" +
                  "gt; 	&amp;amp;lt;/TelephoneSegment&amp;amp;gt; 	&amp;amp;lt;TelephoneSegment&amp;amp;gt;	&amp;amp;lt;Length" +
                  "&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;T02&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;	&amp;amp;lt;TelephoneNumberFieldLength&amp;amp;gt;10&amp;amp;lt;" +
                  "/TelephoneNumberFieldLength&amp;amp;gt;	&amp;amp;lt;TelephoneNumber&amp;amp;gt;9840714555&amp;amp;lt;" +
                  "/TelephoneNumber&amp;amp;gt;	&amp;amp;lt;TelephoneType&amp;amp;gt;01&amp;amp;lt;/TelephoneType&amp;amp;" +
                  "gt; 	&amp;amp;lt;/TelephoneSegment&amp;amp;gt; 	&amp;amp;lt;TelephoneSegment&amp;amp;gt;	&amp;amp;lt;" +
                  "Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;T03&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;	&amp;amp;lt;TelephoneNumberFieldLength&amp;amp;gt;10&amp;amp;lt;/TelephoneNumberFieldLength" +
                  "&amp;amp;gt;	&amp;amp;lt;TelephoneNumber&amp;amp;gt;9870115963&amp;amp;lt;/TelephoneNumber&amp;amp;gt;	&amp;amp;" +
                  "lt;TelephoneType&amp;amp;gt;01&amp;amp;lt;/TelephoneType&amp;amp;gt; 	&amp;amp;lt;/TelephoneSegment&amp;" +
                  "amp;gt; 	&amp;amp;lt;TelephoneSegment&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length" +
                  "&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;T04&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;" +
                  "TelephoneNumberFieldLength&amp;amp;gt;10&amp;amp;lt;/TelephoneNumberFieldLength&amp;amp;gt;	&amp;amp;lt;" +
                  "TelephoneNumber&amp;amp;gt;9012783067&amp;amp;lt;/TelephoneNumber&amp;amp;gt;	&amp;amp;lt;" +
                  "TelephoneExtensionFieldLength&amp;amp;gt;06&amp;amp;lt;/TelephoneExtensionFieldLength&amp;amp;gt;	&amp;amp;" +
                  "lt;TelephoneExtension&amp;amp;gt;234243&amp;amp;lt;/TelephoneExtension&amp;amp;gt;	&amp;amp;lt;" +
                  "TelephoneType&amp;amp;gt;01&amp;amp;lt;/TelephoneType&amp;amp;gt; 	&amp;amp;lt;/TelephoneSegment" +
                  "&amp;amp;gt; 	&amp;amp;lt;EmailContactSegment&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;" +
                  "lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;C01&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;" +
                  "lt;EmailIDFieldLength&amp;amp;gt;35&amp;amp;lt;/EmailIDFieldLength&amp;amp;gt;	&amp;amp;lt;EmailID&amp;amp;" +
                  "gt;vibhavari.sathe@webaccessglobal.com&amp;amp;lt;/EmailID&amp;amp;gt; 	&amp;amp;lt;/EmailContactSegment&amp;" +
                  "amp;gt; 	&amp;amp;lt;EmailContactSegment&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;" +
                  "amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;C02&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;" +
                  "EmailIDFieldLength&amp;amp;gt;35&amp;amp;lt;/EmailIDFieldLength&amp;amp;gt;	&amp;amp;lt;EmailID&amp;amp;" +
                  "gt;vibhavari.sathe@webaccessglobal.com&amp;amp;lt;/EmailID&amp;amp;gt; 	&amp;amp;lt;/EmailContactSegment" +
                  "&amp;amp;gt; 	&amp;amp;lt;ScoreSegment&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;10&amp;amp;lt;/Length" +
                  "&amp;amp;gt;	&amp;amp;lt;ScoreName&amp;amp;gt;CIBILTUSC2&amp;amp;lt;/ScoreName&amp;amp;gt;	&amp;amp;lt;" +
                  "ScoreCardName&amp;amp;gt;04&amp;amp;lt;/ScoreCardName&amp;amp;gt;	&amp;amp;lt;ScoreCardVersion&amp;amp;gt;" +
                  "10&amp;amp;lt;/ScoreCardVersion&amp;amp;gt;	&amp;amp;lt;ScoreDate&amp;amp;gt;19022018&amp;amp;lt;/ScoreDate" +
                  "&amp;amp;gt;	&amp;amp;lt;Score&amp;amp;gt;00750&amp;amp;lt;/Score&amp;amp;gt; 	&amp;amp;lt;/ScoreSegment" +
                  "&amp;amp;gt; 	&amp;amp;lt;Address&amp;amp;gt;	&amp;amp;lt;AddressSegmentTag&amp;amp;gt;PA&amp;amp;lt;" +
                  "/AddressSegmentTag&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;" +
                  "lt;SegmentTag&amp;amp;gt;A01&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;AddressLine1FieldLength&amp;" +
                  "amp;gt;15&amp;amp;lt;/AddressLine1FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine1&amp;amp;gt;501 FIFTH FLOOR" +
                  "&amp;amp;lt;/AddressLine1&amp;amp;gt;	&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;21&amp;amp;lt;" +
                  "/AddressLine2FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine2&amp;amp;gt;PAM VILLE DMONTE PARK&amp;amp;lt;" +
                  "/AddressLine2&amp;amp;gt;	&amp;amp;lt;StateCode&amp;amp;gt;29&amp;amp;lt;/StateCode&amp;amp;gt;	&amp;amp;" +
                  "lt;PinCodeFieldLength&amp;amp;gt;06&amp;amp;lt;/PinCodeFieldLength&amp;amp;gt;	&amp;amp;lt;PinCode&amp;amp;" +
                  "gt;560008&amp;amp;lt;/PinCode&amp;amp;gt;	&amp;amp;lt;AddressCategory&amp;amp;gt;01&amp;amp;lt;/AddressCategory" +
                  "&amp;amp;gt;	&amp;amp;lt;ResidenceCode&amp;amp;gt;01&amp;amp;lt;/ResidenceCode&amp;amp;gt;	&amp;amp;lt;" +
                  "DateReported&amp;amp;gt;30082017&amp;amp;lt;/DateReported&amp;amp;gt; 	&amp;amp;lt;/Address&amp;amp;gt; 	&amp;" +
                  "amp;lt;Address&amp;amp;gt;	&amp;amp;lt;AddressSegmentTag&amp;amp;gt;PA&amp;amp;lt;/AddressSegmentTag&amp;amp;" +
                  "gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;" +
                  "A02&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;AddressLine1FieldLength&amp;amp;gt;17&amp;amp;lt;" +
                  "/AddressLine1FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine1&amp;amp;gt;ETREWREWRDSFDSFSF&amp;amp;lt;" +
                  "/AddressLine1&amp;amp;gt;	&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;13&amp;amp;lt;" +
                  "/AddressLine2FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine2&amp;amp;gt;FDSFDSFDSFDSF&amp;amp;lt;" +
                  "/AddressLine2&amp;amp;gt;	&amp;amp;lt;AddressLine3FieldLength&amp;amp;gt;21&amp;amp;lt;/AddressLine3FieldLength" +
                  "&amp;amp;gt;	&amp;amp;lt;AddressLine3&amp;amp;gt;DSFDSFDSFDSFDSFDSFDSF&amp;amp;lt;/AddressLine3&amp;amp;gt;	&amp;" +
                  "amp;lt;StateCode&amp;amp;gt;33&amp;amp;lt;/StateCode&amp;amp;gt;	&amp;amp;lt;PinCodeFieldLength&amp;amp;gt;06&amp;" +
                  "amp;lt;/PinCodeFieldLength&amp;amp;gt;	&amp;amp;lt;PinCode&amp;amp;gt;600100&amp;amp;lt;/PinCode&amp;amp;gt;	&amp;" +
                  "amp;lt;AddressCategory&amp;amp;gt;02&amp;amp;lt;/AddressCategory&amp;amp;gt;	&amp;amp;lt;ResidenceCode&amp;amp;gt;" +
                  "01&amp;amp;lt;/ResidenceCode&amp;amp;gt;	&amp;amp;lt;DateReported&amp;amp;gt;24072017&amp;amp;lt;/DateReported" +
                  "&amp;amp;gt; 	&amp;amp;lt;/Address&amp;amp;gt; 	&amp;amp;lt;Address&amp;amp;gt;	&amp;amp;lt;AddressSegmentTag" +
                  "&amp;amp;gt;PA&amp;amp;lt;/AddressSegmentTag&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;" +
                  "/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;A03&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;" +
                  "lt;AddressLine1FieldLength&amp;amp;gt;14&amp;amp;lt;/AddressLine1FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine1" +
                  "&amp;amp;gt;11 KAMLA NAGAR&amp;amp;lt;/AddressLine1&amp;amp;gt;	&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;" +
                  "20&amp;amp;lt;/AddressLine2FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine2&amp;amp;gt;2ND FLOOR  9TH SCTOR" +
                  "&amp;amp;lt;/AddressLine2&amp;amp;gt;	&amp;amp;lt;AddressLine3FieldLength&amp;amp;gt;08&amp;amp;lt;" +
                  "/AddressLine3FieldLength&amp;amp;gt;	&amp;amp;lt;AddressLine3&amp;amp;gt;KK NAGAR&amp;amp;lt;/AddressLine3" +
                  "&amp;amp;gt;	&amp;amp;lt;StateCode&amp;amp;gt;27&amp;amp;lt;/StateCode&amp;amp;gt;	&amp;amp;lt;PinCodeFieldLength" +
                  "&amp;amp;gt;06&amp;amp;lt;/PinCodeFieldLength&amp;amp;gt;	&amp;amp;lt;PinCode&amp;amp;gt;400001&amp;amp;lt;" +
                  "/PinCode&amp;amp;gt;	&amp;amp;lt;AddressCategory&amp;amp;gt;02&amp;amp;lt;/AddressCategory&amp;amp;gt;	&amp;amp;" +
                  "lt;DateReported&amp;amp;gt;19052017&amp;amp;lt;/DateReported&amp;amp;gt; 	&amp;amp;lt;/Address&amp;amp;gt; 	&amp;" +
                  "amp;lt;Address&amp;amp;gt;	&amp;amp;lt;AddressSegmentTag&amp;amp;gt;PA&amp;amp;lt;/AddressSegmentTag&amp;amp;" +
                  "gt;	&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;A04&amp;" +
                  "amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;AddressLine1FieldLength&amp;amp;gt;22&amp;amp;lt;/AddressLine1FieldLength" +
                  "&amp;amp;gt;	&amp;amp;lt;AddressLine1&amp;amp;gt;PAM  VILLE DMONTE PARK&amp;amp;lt;/AddressLine1&amp;amp;gt;	&amp;" +
                  "amp;lt;AddressLine2FieldLength&amp;amp;gt;22&amp;amp;lt;/AddressLine2FieldLength&amp;amp;gt;	&amp;amp;lt;" +
                  "AddressLine2&amp;amp;gt;PAM  VILLE DMONTE PARK&amp;amp;lt;/AddressLine2&amp;amp;gt;	&amp;amp;lt;" +
                  "AddressLine4FieldLength&amp;amp;gt;05&amp;amp;lt;/AddressLine4FieldLength&amp;amp;gt;	&amp;amp;lt;" +
                  "AddressLine4&amp;amp;gt;DELHI&amp;amp;lt;/AddressLine4&amp;amp;gt;	&amp;amp;lt;StateCode&amp;amp;gt;07&amp;" +
                  "amp;lt;/StateCode&amp;amp;gt;	&amp;amp;lt;PinCodeFieldLength&amp;amp;gt;06&amp;amp;lt;/PinCodeFieldLength&amp;amp;" +
                  "gt;	&amp;amp;lt;PinCode&amp;amp;gt;110002&amp;amp;lt;/PinCode&amp;amp;gt;	&amp;amp;lt;AddressCategory&amp;amp;" +
                  "gt;02&amp;amp;lt;/AddressCategory&amp;amp;gt;	&amp;amp;lt;DateReported&amp;amp;gt;09012017&amp;amp;lt;" +
                  "/DateReported&amp;amp;gt; 	&amp;amp;lt;/Address&amp;amp;gt; 	&amp;amp;lt;Account&amp;amp;gt;	&amp;amp;lt;" +
                  "Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;T001&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;	&amp;amp;lt;Account_Summary_Segment_Fields&amp;amp;gt;	&amp;amp;lt;" +
                  "ReportingMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;lt;/ReportingMemberShortNameFieldLength&amp;amp;" +
                  "gt;	&amp;amp;lt;/Account_Summary_Segment_Fields&amp;amp;gt;	&amp;amp;lt;Account_NonSummary_Segment_Fields&amp;" +
                  "amp;gt;	&amp;amp;lt;ReportingMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;lt;/ReportingMemberShortNameFieldLength" +
                  "&amp;amp;gt;	&amp;amp;lt;ReportingMemberShortName&amp;amp;gt;NOT DISCLOSED&amp;amp;lt;/ReportingMemberShortName&amp;" +
                  "amp;gt;	&amp;amp;lt;AccountType&amp;amp;gt;10&amp;amp;lt;/AccountType&amp;amp;gt;	&amp;amp;lt;" +
                  "OwenershipIndicator&amp;amp;gt;1&amp;amp;lt;/OwenershipIndicator&amp;amp;gt;	&amp;amp;lt;DateOpenedOrDisbursed" +
                  "&amp;amp;gt;01102017&amp;amp;lt;/DateOpenedOrDisbursed&amp;amp;gt;	&amp;amp;lt;" +
                  "HighCreditOrSanctionedAmountFieldLength&amp;amp;gt;05&amp;amp;lt;/HighCreditOrSanctionedAmountFieldLength&amp;amp;" +
                  "gt;	&amp;amp;lt;HighCreditOrSanctionedAmount&amp;amp;gt;100000&amp;amp;lt;/HighCreditOrSanctionedAmount&amp;amp;" +
                  "gt;	&amp;amp;lt;DateReportedAndCertified&amp;amp;gt;30062014&amp;amp;lt;/DateReportedAndCertified&amp;amp;" +
                  "gt;	&amp;amp;lt;CurrentBalanceFieldLength&amp;amp;gt;01&amp;amp;lt;/CurrentBalanceFieldLength&amp;amp;gt;	&amp;" +
                  "amp;lt;CurrentBalance&amp;amp;gt;100000&amp;amp;lt;/CurrentBalance&amp;amp;gt;	&amp;amp;lt;CreditLimit&amp;" +
                  "amp;gt;20000&amp;amp;lt;/CreditLimit&amp;amp;gt;	&amp;amp;lt;PaymentHistory1FieldLength&amp;amp;gt;09&amp;" +
                  "amp;lt;/PaymentHistory1FieldLength&amp;amp;gt;	&amp;amp;lt;PaymentHistory1&amp;amp;gt;" +
                  "000000000000000000000000000000000000000000000000000000&amp;amp;lt;/PaymentHistory1&amp;amp;gt;	&amp;amp;lt;" +
                  "PaymentHistoryStartDate&amp;amp;gt;30012018&amp;amp;lt;/PaymentHistoryStartDate&amp;amp;gt;	&amp;amp;lt;" +
                  "PaymentHistoryEndDate&amp;amp;gt;01042014&amp;amp;lt;/PaymentHistoryEndDate&amp;amp;gt;	&amp;amp;lt;" +
                  "CreditLimitFieldLength&amp;amp;gt;05&amp;amp;lt;/CreditLimitFieldLength&amp;amp;gt;	&amp;amp;lt;" +
                  "AmountOverdue&amp;amp;gt;5000&amp;amp;lt;/AmountOverdue&amp;amp;gt;	&amp;amp;lt;PaymentFrequency&amp;" +
                  "amp;gt;03&amp;amp;lt;/PaymentFrequency&amp;amp;gt;	&amp;amp;lt;/Account_NonSummary_Segment_Fields&amp;" +
                  "amp;gt; 	&amp;amp;lt;/Account&amp;amp;gt; 	&amp;amp;lt;Account&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;" +
                  "gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;T002&amp;amp;lt;/SegmentTag&amp;" +
                  "amp;gt;	&amp;amp;lt;Account_Summary_Segment_Fields&amp;amp;gt;	&amp;amp;lt;ReportingMemberShortNameFieldLength" +
                  "&amp;amp;gt;13&amp;amp;lt;/ReportingMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;" +
                  "/Account_Summary_Segment_Fields&amp;amp;gt;	&amp;amp;lt;Account_NonSummary_Segment_Fields&amp;amp;gt;	&amp;" +
                  "amp;lt;ReportingMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;lt;/ReportingMemberShortNameFieldLength&amp;" +
                  "amp;gt;	&amp;amp;lt;ReportingMemberShortName&amp;amp;gt;NOT DISCLOSED&amp;amp;lt;/ReportingMemberShortName&amp;" +
                  "amp;gt;	&amp;amp;lt;AccountType&amp;amp;gt;02&amp;amp;lt;/AccountType&amp;amp;gt;	&amp;amp;lt;" +
                  "OwenershipIndicator&amp;amp;gt;1&amp;amp;lt;/OwenershipIndicator&amp;amp;gt;	&amp;amp;lt;DateOpenedOrDisbursed" +
                  "&amp;amp;gt;01052017&amp;amp;lt;/DateOpenedOrDisbursed&amp;amp;gt;	&amp;amp;lt;DateReportedAndCertified&amp;" +
                  "amp;gt;30062014&amp;amp;lt;/DateReportedAndCertified&amp;amp;gt;	&amp;amp;lt;CurrentBalanceFieldLength&amp;" +
                  "amp;gt;01&amp;amp;lt;/CurrentBalanceFieldLength&amp;amp;gt;	&amp;amp;lt;CurrentBalance&amp;amp;gt;50000" +
                  "&amp;amp;lt;/CurrentBalance&amp;amp;gt;	&amp;amp;lt;PaymentHistory1FieldLength&amp;amp;gt;09&amp;amp;" +
                  "lt;/PaymentHistory1FieldLength&amp;amp;gt;	&amp;amp;lt;PaymentHistory1&amp;amp;gt;000000000&amp;amp;lt;" +
                  "/PaymentHistory1&amp;amp;gt;	&amp;amp;lt;PaymentHistoryStartDate&amp;amp;gt;01062014&amp;amp;lt;" +
                  "/PaymentHistoryStartDate&amp;amp;gt;	&amp;amp;lt;PaymentHistoryEndDate&amp;amp;gt;01042014&amp;amp;lt;" +
                  "/PaymentHistoryEndDate&amp;amp;gt;	&amp;amp;lt;CreditLimitFieldLength&amp;amp;gt;05&amp;amp;lt;" +
                  "/CreditLimitFieldLength&amp;amp;gt;	&amp;amp;lt;CreditLimit&amp;amp;gt;25000&amp;amp;lt;/CreditLimit&amp;" +
                  "amp;gt;	&amp;amp;lt;PaymentFrequency&amp;amp;gt;03&amp;amp;lt;/PaymentFrequency&amp;amp;gt;	&amp;amp;lt;" +
                  "/Account_NonSummary_Segment_Fields&amp;amp;gt; 	&amp;amp;lt;/Account&amp;amp;gt; 	&amp;amp;lt;Account&amp;" +
                  "amp;gt;	&amp;amp;lt;Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;" +
                  "T003&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;Account_Summary_Segment_Fields&amp;amp;gt;	&amp;amp;" +
                  "lt;ReportingMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;lt;/ReportingMemberShortNameFieldLength&amp;" +
                  "amp;gt;	&amp;amp;lt;/Account_Summary_Segment_Fields&amp;amp;gt;	&amp;amp;lt;Account_NonSummary_Segment_Fields" +
                  "&amp;amp;gt;	&amp;amp;lt;ReportingMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;lt;" +
                  "/ReportingMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;ReportingMemberShortName&amp;amp;gt;" +
                  "NOT DISCLOSED&amp;amp;lt;/ReportingMemberShortName&amp;amp;gt;	&amp;amp;lt;AccountType&amp;amp;gt;05" +
                  "&amp;amp;lt;/AccountType&amp;amp;gt;	&amp;amp;lt;OwenershipIndicator&amp;amp;gt;1&amp;amp;lt;" +
                  "/OwenershipIndicator&amp;amp;gt;	&amp;amp;lt;DateOpenedOrDisbursed&amp;amp;gt;01022016&amp;amp;lt;" +
                  "/DateOpenedOrDisbursed&amp;amp;gt;	&amp;amp;lt;DateOfLastPayment&amp;amp;gt;01082018&amp;amp;lt;" +
                  "/DateOfLastPayment&amp;amp;gt;	&amp;amp;lt;DateReportedAndCertified&amp;amp;gt;30062014&amp;amp;lt;" +
                  "/DateReportedAndCertified&amp;amp;gt;	&amp;amp;lt;HighCreditOrSanctionedAmountFieldLength&amp;amp;gt;06&amp;" +
                  "amp;lt;/HighCreditOrSanctionedAmountFieldLength&amp;amp;gt;	&amp;amp;lt;HighCreditOrSanctionedAmount&amp;" +
                  "amp;gt;500000&amp;amp;lt;/HighCreditOrSanctionedAmount&amp;amp;gt;	&amp;amp;lt;CurrentBalanceFieldLength" +
                  "&amp;amp;gt;06&amp;amp;lt;/CurrentBalanceFieldLength&amp;amp;gt;	&amp;amp;lt;CurrentBalance&amp;amp;gt;" +
                  "461634&amp;amp;lt;/CurrentBalance&amp;amp;gt;	&amp;amp;lt;EmiAmountFieldLength&amp;amp;gt;04&amp;amp;" +
                  "lt;/EmiAmountFieldLength&amp;amp;gt;	&amp;amp;lt;EmiAmount&amp;amp;gt;5000&amp;amp;lt;/EmiAmount&amp;" +
                  "amp;gt;	&amp;amp;lt;PaymentHistory1FieldLength&amp;amp;gt;12&amp;amp;lt;/PaymentHistory1FieldLength&amp;" +
                  "amp;gt;	&amp;amp;lt;PaymentHistory1&amp;amp;gt;000000000000&amp;amp;lt;/PaymentHistory1&amp;amp;gt;	&amp;" +
                  "amp;lt;PaymentHistoryStartDate&amp;amp;gt;01062014&amp;amp;lt;/PaymentHistoryStartDate&amp;amp;gt;	&amp;" +
                  "amp;lt;PaymentHistoryEndDate&amp;amp;gt;01032014&amp;amp;lt;/PaymentHistoryEndDate&amp;amp;gt;	&amp;" +
                  "amp;lt;/Account_NonSummary_Segment_Fields&amp;amp;gt; 	&amp;amp;lt;/Account&amp;amp;gt; 	&amp;amp;lt;" +
                  "Enquiry&amp;amp;gt;	&amp;amp;lt;Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;" +
                  "SegmentTag&amp;amp;gt;I001&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;DateOfEnquiryFields&amp;" +
                  "amp;gt;01012018&amp;amp;lt;/DateOfEnquiryFields&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortNameFieldLength" +
                  "&amp;amp;gt;09&amp;amp;lt;/EnquiringMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortName" +
                  "&amp;amp;gt;AXIS BANK&amp;amp;lt;/EnquiringMemberShortName&amp;amp;gt;	&amp;amp;lt;EnquiryPurpose&amp;amp;" +
                  "gt;05&amp;amp;lt;/EnquiryPurpose&amp;amp;gt;	&amp;amp;lt;EnquiryAmountFieldLength&amp;amp;gt;08&amp;amp;" +
                  "lt;/EnquiryAmountFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiryAmount&amp;amp;gt;10000000&amp;amp;lt;" +
                  "/EnquiryAmount&amp;amp;gt; 	&amp;amp;lt;/Enquiry&amp;amp;gt; 	&amp;amp;lt;Enquiry&amp;amp;gt;	&amp;amp;" +
                  "lt;Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;I002&amp;amp;" +
                  "lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;DateOfEnquiryFields&amp;amp;gt;01122017&amp;amp;lt;" +
                  "/DateOfEnquiryFields&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;" +
                  "lt;/EnquiringMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortName&amp;amp;gt;" +
                  "NOT DISCLOSED&amp;amp;lt;/EnquiringMemberShortName&amp;amp;gt;	&amp;amp;lt;EnquiryPurpose&amp;amp;gt;" +
                  "10&amp;amp;lt;/EnquiryPurpose&amp;amp;gt;	&amp;amp;lt;EnquiryAmountFieldLength&amp;amp;gt;06&amp;amp;" +
                  "lt;/EnquiryAmountFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiryAmount&amp;amp;gt;100000&amp;amp;lt;" +
                  "/EnquiryAmount&amp;amp;gt; 	&amp;amp;lt;/Enquiry&amp;amp;gt; 	&amp;amp;lt;Enquiry&amp;amp;gt;	&amp;" +
                  "amp;lt;Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;I003" +
                  "&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;DateOfEnquiryFields&amp;amp;gt;01112017&amp;amp;lt;" +
                  "/DateOfEnquiryFields&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;" +
                  "lt;/EnquiringMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortName&amp;amp;gt;" +
                  "NOT DISCLOSED&amp;amp;lt;/EnquiringMemberShortName&amp;amp;gt;	&amp;amp;lt;EnquiryPurpose&amp;amp;" +
                  "gt;05&amp;amp;lt;/EnquiryPurpose&amp;amp;gt;	&amp;amp;lt;EnquiryAmountFieldLength&amp;amp;gt;06&amp;" +
                  "amp;lt;/EnquiryAmountFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiryAmount&amp;amp;gt;100000&amp;amp;" +
                  "lt;/EnquiryAmount&amp;amp;gt; 	&amp;amp;lt;/Enquiry&amp;amp;gt; 	&amp;amp;lt;Enquiry&amp;amp;gt;	&amp;" +
                  "amp;lt;Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;I004&amp;" +
                  "amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;DateOfEnquiryFields&amp;amp;gt;01102017&amp;amp;" +
                  "lt;/DateOfEnquiryFields&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortNameFieldLength&amp;amp;gt;13&amp;" +
                  "amp;lt;/EnquiringMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortName&amp;" +
                  "amp;gt;NOT DISCLOSED&amp;amp;lt;/EnquiringMemberShortName&amp;amp;gt;	&amp;amp;lt;EnquiryPurpose&amp;" +
                  "amp;gt;05&amp;amp;lt;/EnquiryPurpose&amp;amp;gt;	&amp;amp;lt;EnquiryAmountFieldLength&amp;amp;gt;06&amp;" +
                  "amp;lt;/EnquiryAmountFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiryAmount&amp;amp;gt;100000&amp;amp;lt;" +
                  "/EnquiryAmount&amp;amp;gt; 	&amp;amp;lt;/Enquiry&amp;amp;gt; 	&amp;amp;lt;Enquiry&amp;amp;gt;	&amp;amp;lt;" +
                  "Length&amp;amp;gt;04&amp;amp;lt;/Length&amp;amp;gt;	&amp;amp;lt;SegmentTag&amp;amp;gt;I005&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;	&amp;amp;lt;DateOfEnquiryFields&amp;amp;gt;01092017&amp;amp;lt;/DateOfEnquiryFields&amp;" +
                  "amp;gt;	&amp;amp;lt;EnquiringMemberShortNameFieldLength&amp;amp;gt;13&amp;amp;lt;" +
                  "/EnquiringMemberShortNameFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiringMemberShortName&amp;amp;" +
                  "gt;NOT DISCLOSED&amp;amp;lt;/EnquiringMemberShortName&amp;amp;gt;	&amp;amp;lt;EnquiryPurpose&amp;amp;gt;" +
                  "10&amp;amp;lt;/EnquiryPurpose&amp;amp;gt;	&amp;amp;lt;EnquiryAmountFieldLength&amp;amp;gt;06&amp;amp;lt;" +
                  "/EnquiryAmountFieldLength&amp;amp;gt;	&amp;amp;lt;EnquiryAmount&amp;amp;gt;100000&amp;amp;lt;" +
                  "/EnquiryAmount&amp;amp;gt; 	&amp;amp;lt;/Enquiry&amp;amp;gt; 	&amp;amp;lt;End&amp;amp;gt;	&amp;amp;lt;" +
                  "SegmentTag&amp;amp;gt;ES07&amp;amp;lt;/SegmentTag&amp;amp;gt;	&amp;amp;lt;TotalLength&amp;amp;gt;0044141&amp;amp;" +
                  "lt;/TotalLength&amp;amp;gt; 	&amp;amp;lt;/End&amp;amp;gt; &amp;amp;lt;/CreditReport&amp;amp;gt;&amp;lt;" +
                  "/BureauResponseXml&amp;gt;&#xD; &amp;lt;SecondaryReportXml&amp;gt;&amp;amp;lt;Root&amp;amp;gt;&amp;amp;lt;" +
                  "CreditReport&amp;amp;gt;&amp;amp;lt;Header&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;TUEF&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;&amp;amp;lt;Version&amp;amp;gt;12&amp;amp;lt;/Version&amp;amp;gt;&amp;amp;lt;" +
                  "ReferenceNumber&amp;amp;gt;10064324&amp;amp;lt;/ReferenceNumber&amp;amp;gt;&amp;amp;lt;MemberCode&amp;" +
                  "amp;gt;BP03291005_DCTST              &amp;amp;lt;/MemberCode&amp;amp;gt;&amp;amp;lt;SubjectReturnCode" +
                  "&amp;amp;gt;1&amp;amp;lt;/SubjectReturnCode&amp;amp;gt;&amp;amp;lt;EnquiryControlNumber&amp;amp;gt;" +
                  "000000000000&amp;amp;lt;/EnquiryControlNumber&amp;amp;gt;&amp;amp;lt;DateProcessed&amp;amp;gt;" +
                  "19022018&amp;amp;lt;/DateProcessed&amp;amp;gt;&amp;amp;lt;TimeProcessed&amp;amp;gt;150200&amp;amp;" +
                  "lt;/TimeProcessed&amp;amp;gt;&amp;amp;lt;/Header&amp;amp;gt;&amp;amp;lt;NameSegment&amp;amp;gt;&amp;" +
                  "amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;N01&amp;" +
                  "amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;ConsumerName1FieldLength&amp;amp;gt;05&amp;amp;lt;" +
                  "/ConsumerName1FieldLength&amp;amp;gt;&amp;amp;lt;ConsumerName1&amp;amp;gt;MABEL&amp;amp;lt;" +
                  "/ConsumerName1&amp;amp;gt;&amp;amp;lt;ConsumerName2FieldLength&amp;amp;gt;02&amp;amp;lt;/ConsumerName2FieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;ConsumerName2&amp;amp;gt;KS&amp;amp;lt;/ConsumerName2&amp;amp;gt;&amp;amp;lt;" +
                  "ConsumerName3FieldLength&amp;amp;gt;11&amp;amp;lt;/ConsumerName3FieldLength&amp;amp;gt;&amp;amp;lt;ConsumerName3" +
                  "&amp;amp;gt;RAJAGOPALAN&amp;amp;lt;/ConsumerName3&amp;amp;gt;&amp;amp;lt;DateOfBirthFieldLength&amp;amp;gt;" +
                  "08&amp;amp;lt;/DateOfBirthFieldLength&amp;amp;gt;&amp;amp;lt;DateOfBirth&amp;amp;gt;01051966&amp;amp;lt;" +
                  "/DateOfBirth&amp;amp;gt;&amp;amp;lt;GenderFieldLength&amp;amp;gt;01&amp;amp;lt;/GenderFieldLength&amp;amp;" +
                  "gt;&amp;amp;lt;Gender&amp;amp;gt;2&amp;amp;lt;/Gender&amp;amp;gt;&amp;amp;lt;/NameSegment&amp;amp;gt;&amp;" +
                  "amp;lt;IDSegment&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;" +
                  "SegmentTag&amp;amp;gt;I01&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;IDType&amp;amp;gt;01&amp;amp;lt;" +
                  "/IDType&amp;amp;gt;&amp;amp;lt;IDNumberFieldLength&amp;amp;gt;10&amp;amp;lt;/IDNumberFieldLength&amp;amp;" +
                  "gt;&amp;amp;lt;IDNumber&amp;amp;gt;DBFPS5377L&amp;amp;lt;/IDNumber&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry" +
                  "&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;/IDSegment&amp;amp;gt;&amp;amp;lt;" +
                  "IDSegment&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag" +
                  "&amp;amp;gt;I02&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;IDType&amp;amp;gt;03&amp;amp;lt;/IDType" +
                  "&amp;amp;gt;&amp;amp;lt;IDNumberFieldLength&amp;amp;gt;14&amp;amp;lt;/IDNumberFieldLength&amp;amp;gt;&amp;" +
                  "amp;lt;IDNumber&amp;amp;gt;AP090570102476&amp;amp;lt;/IDNumber&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry" +
                  "&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;/IDSegment&amp;amp;gt;&amp;amp;lt;" +
                  "TelephoneSegment&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;" +
                  "SegmentTag&amp;amp;gt;T01&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;TelephoneNumberFieldLength&amp;" +
                  "amp;gt;09&amp;amp;lt;/TelephoneNumberFieldLength&amp;amp;gt;&amp;amp;lt;TelephoneNumber&amp;amp;gt;" +
                  "989040883&amp;amp;lt;/TelephoneNumber&amp;amp;gt;&amp;amp;lt;TelephoneType&amp;amp;gt;00&amp;amp;lt;" +
                  "/TelephoneType&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry" +
                  "&amp;amp;gt;&amp;amp;lt;/TelephoneSegment&amp;amp;gt;&amp;amp;lt;TelephoneSegment&amp;amp;gt;&amp;amp;lt;" +
                  "Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;T02&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;&amp;amp;lt;TelephoneNumberFieldLength&amp;amp;gt;10&amp;amp;lt;/TelephoneNumberFieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;TelephoneNumber&amp;amp;gt;9632587410&amp;amp;lt;/TelephoneNumber&amp;amp;gt;&amp;amp;" +
                  "lt;TelephoneType&amp;amp;gt;01&amp;amp;lt;/TelephoneType&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;" +
                  "gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;/TelephoneSegment&amp;amp;gt;&amp;amp;lt;" +
                  "TelephoneSegment&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;" +
                  "SegmentTag&amp;amp;gt;T03&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;TelephoneNumberFieldLength&amp;" +
                  "amp;gt;08&amp;amp;lt;/TelephoneNumberFieldLength&amp;amp;gt;&amp;amp;lt;TelephoneNumber&amp;amp;gt;" +
                  "33456244&amp;amp;lt;/TelephoneNumber&amp;amp;gt;&amp;amp;lt;TelephoneType&amp;amp;gt;00&amp;amp;lt;" +
                  "/TelephoneType&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry" +
                  "&amp;amp;gt;&amp;amp;lt;/TelephoneSegment&amp;amp;gt;&amp;amp;lt;TelephoneSegment&amp;amp;gt;&amp;amp;lt;" +
                  "Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;T04&amp;amp;lt;" +
                  "/SegmentTag&amp;amp;gt;&amp;amp;lt;TelephoneNumberFieldLength&amp;amp;gt;05&amp;amp;lt;/TelephoneNumberFieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;TelephoneNumber&amp;amp;gt;23456&amp;amp;lt;/TelephoneNumber&amp;amp;gt;&amp;amp;lt;" +
                  "TelephoneType&amp;amp;gt;00&amp;amp;lt;/TelephoneType&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;gt;" +
                  "Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;/TelephoneSegment&amp;amp;gt;&amp;amp;lt;Address" +
                  "&amp;amp;gt;&amp;amp;lt;AddressSegmentTag&amp;amp;gt;PA&amp;amp;lt;/AddressSegmentTag&amp;amp;gt;&amp;amp;" +
                  "lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;A01&amp;amp;" +
                  "lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;AddressLine1FieldLength&amp;amp;gt;30&amp;amp;lt;/AddressLine1FieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;AddressLine1&amp;amp;gt;NILAM ROADWAYS NR AROHI MOTORS&amp;amp;lt;/AddressLine1" +
                  "&amp;amp;gt;&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;19&amp;amp;lt;/AddressLine2FieldLength&amp;" +
                  "amp;gt;&amp;amp;lt;AddressLine2&amp;amp;gt;NAROL JETALPUR ROAD&amp;amp;lt;/AddressLine2&amp;amp;gt;&amp;" +
                  "amp;lt;AddressLine4FieldLength&amp;amp;gt;09&amp;amp;lt;/AddressLine4FieldLength&amp;amp;gt;&amp;amp;lt;" +
                  "AddressLine4&amp;amp;gt;AHMEDABAD&amp;amp;lt;/AddressLine4&amp;amp;gt;&amp;amp;lt;StateCode&amp;amp;gt;24" +
                  "&amp;amp;lt;/StateCode&amp;amp;gt;&amp;amp;lt;PinCodeFieldLength&amp;amp;gt;06&amp;amp;lt;/PinCodeFieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;PinCode&amp;amp;gt;380008&amp;amp;lt;/PinCode&amp;amp;gt;&amp;amp;lt;AddressCategory" +
                  "&amp;amp;gt;03&amp;amp;lt;/AddressCategory&amp;amp;gt;&amp;amp;lt;ResidenceCode&amp;amp;gt;02&amp;amp;lt;" +
                  "/ResidenceCode&amp;amp;gt;&amp;amp;lt;DateReported&amp;amp;gt;18102016&amp;amp;lt;/DateReported&amp;amp;gt;" +
                  "&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;" +
                  "/Address&amp;amp;gt;&amp;amp;lt;Address&amp;amp;gt;&amp;amp;lt;AddressSegmentTag&amp;amp;gt;PA&amp;amp;lt;" +
                  "/AddressSegmentTag&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;" +
                  "SegmentTag&amp;amp;gt;A02&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;AddressLine1FieldLength&amp;amp;" +
                  "gt;28&amp;amp;lt;/AddressLine1FieldLength&amp;amp;gt;&amp;amp;lt;AddressLine1&amp;amp;gt;501, FIFTH FLOOR, PAM VILLE," +
                  "&amp;amp;lt;/AddressLine1&amp;amp;gt;&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;23&amp;amp;lt;" +
                  "/AddressLine2FieldLength&amp;amp;gt;&amp;amp;lt;AddressLine2&amp;amp;gt;DMONTE PARK ROAD BANDRA&amp;amp;lt;" +
                  "/AddressLine2&amp;amp;gt;&amp;amp;lt;AddressLine3FieldLength&amp;amp;gt;11&amp;amp;lt;/AddressLine3FieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;AddressLine3&amp;amp;gt;WEST MUMBAI&amp;amp;lt;/AddressLine3&amp;amp;gt;&amp;amp;lt;" +
                  "AddressLine4FieldLength&amp;amp;gt;06&amp;amp;lt;/AddressLine4FieldLength&amp;amp;gt;&amp;amp;lt;AddressLine4" +
                  "&amp;amp;gt;MUMBAI&amp;amp;lt;/AddressLine4&amp;amp;gt;&amp;amp;lt;StateCode&amp;amp;gt;24&amp;amp;lt;" +
                  "/StateCode&amp;amp;gt;&amp;amp;lt;PinCodeFieldLength&amp;amp;gt;06&amp;amp;lt;/PinCodeFieldLength&amp;amp;gt;" +
                  "&amp;amp;lt;PinCode&amp;amp;gt;380008&amp;amp;lt;/PinCode&amp;amp;gt;&amp;amp;lt;AddressCategory&amp;amp;gt;" +
                  "02&amp;amp;lt;/AddressCategory&amp;amp;gt;&amp;amp;lt;ResidenceCode&amp;amp;gt;02&amp;amp;lt;" +
                  "/ResidenceCode&amp;amp;gt;&amp;amp;lt;DateReported&amp;amp;gt;18102016&amp;amp;lt;/DateReported&amp;amp;" +
                  "gt;&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;" +
                  "lt;/Address&amp;amp;gt;&amp;amp;lt;Address&amp;amp;gt;&amp;amp;lt;AddressSegmentTag&amp;amp;gt;PA&amp;" +
                  "amp;lt;/AddressSegmentTag&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;" +
                  "amp;lt;SegmentTag&amp;amp;gt;A03&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;AddressLine1FieldLength&amp;" +
                  "amp;gt;15&amp;amp;lt;/AddressLine1FieldLength&amp;amp;gt;&amp;amp;lt;AddressLine1&amp;amp;gt;501 FIFTH FLOOR" +
                  "&amp;amp;lt;/AddressLine1&amp;amp;gt;&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;21&amp;amp;lt;" +
                  "/AddressLine2FieldLength&amp;amp;gt;&amp;amp;lt;AddressLine2&amp;amp;gt;PAM VILLE DMONTE PARK&amp;amp;lt;" +
                  "/AddressLine2&amp;amp;gt;&amp;amp;lt;AddressLine3FieldLength&amp;amp;gt;23&amp;amp;lt;/AddressLine3FieldLength" +
                  "&amp;amp;gt;&amp;amp;lt;AddressLine3&amp;amp;gt;N.177 MAHALAKSHMI NAGAR&amp;amp;lt;/AddressLine3&amp;amp;gt;" +
                  "&amp;amp;lt;AddressLine4FieldLength&amp;amp;gt;22&amp;amp;lt;/AddressLine4FieldLength&amp;amp;gt;&amp;amp;" +
                  "lt;AddressLine4&amp;amp;gt;1ST KURUKKU   STREET ,&amp;amp;lt;/AddressLine4&amp;amp;gt;&amp;amp;lt;" +
                  "AddressLine5FieldLength&amp;amp;gt;06&amp;amp;lt;/AddressLine5FieldLength&amp;amp;gt;&amp;amp;lt;AddressLine5" +
                  "&amp;amp;gt;MUMBAI&amp;amp;lt;/AddressLine5&amp;amp;gt;&amp;amp;lt;StateCode&amp;amp;gt;27&amp;amp;lt;" +
                  "/StateCode&amp;amp;gt;&amp;amp;lt;PinCodeFieldLength&amp;amp;gt;06&amp;amp;lt;/PinCodeFieldLength&amp;amp;gt;" +
                  "&amp;amp;lt;PinCode&amp;amp;gt;400050&amp;amp;lt;/PinCode&amp;amp;gt;&amp;amp;lt;AddressCategory&amp;amp;gt;" +
                  "02&amp;amp;lt;/AddressCategory&amp;amp;gt;&amp;amp;lt;ResidenceCode&amp;amp;gt;01&amp;amp;lt;/ResidenceCode" +
                  "&amp;amp;gt;&amp;amp;lt;DateReported&amp;amp;gt;25082015&amp;amp;lt;/DateReported&amp;amp;gt;&amp;amp;lt;" +
                  "EnrichedThroughEnquiry&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;/Address&amp;" +
                  "amp;gt;&amp;amp;lt;End&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;ES07&amp;amp;lt;/SegmentTag&amp;amp;gt;" +
                  "&amp;amp;lt;TotalLength&amp;amp;gt;0000770&amp;amp;lt;/TotalLength&amp;amp;gt;&amp;amp;lt;/End&amp;amp;gt;" +
                  "&amp;amp;lt;/CreditReport&amp;amp;gt;&amp;amp;lt;CreditReport&amp;amp;gt;&amp;amp;lt;Header&amp;amp;gt;" +
                  "&amp;amp;lt;SegmentTag&amp;amp;gt;TUEF&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;Version&amp;amp;gt;" +
                  "12&amp;amp;lt;/Version&amp;amp;gt;&amp;amp;lt;ReferenceNumber&amp;amp;gt;10064324&amp;amp;lt;/ReferenceNumber" +
                  "&amp;amp;gt;&amp;amp;lt;MemberCode&amp;amp;gt;BP03291005_DCTST              &amp;amp;lt;/MemberCode&amp;amp;" +
                  "gt;&amp;amp;lt;SubjectReturnCode&amp;amp;gt;1&amp;amp;lt;/SubjectReturnCode&amp;amp;gt;&amp;amp;lt;" +
                  "EnquiryControlNumber&amp;amp;gt;000000000000&amp;amp;lt;/EnquiryControlNumber&amp;amp;gt;&amp;amp;lt;" +
                  "DateProcessed&amp;amp;gt;19022018&amp;amp;lt;/DateProcessed&amp;amp;gt;&amp;amp;lt;TimeProcessed&amp;amp;" +
                  "gt;150200&amp;amp;lt;/TimeProcessed&amp;amp;gt;&amp;amp;lt;/Header&amp;amp;gt;&amp;amp;lt;NameSegment&amp;" +
                  "amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;N01" +
                  "&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;ConsumerName1FieldLength&amp;amp;gt;05&amp;amp;lt;" +
                  "/ConsumerName1FieldLength&amp;amp;gt;&amp;amp;lt;ConsumerName1&amp;amp;gt;MABEL&amp;amp;lt;/ConsumerName1" +
                  "&amp;amp;gt;&amp;amp;lt;ConsumerName2FieldLength&amp;amp;gt;11&amp;amp;lt;/ConsumerName2FieldLength&amp;" +
                  "amp;gt;&amp;amp;lt;ConsumerName2&amp;amp;gt;RAJAGOPALAN&amp;amp;lt;/ConsumerName2&amp;amp;gt;&amp;amp;lt;" +
                  "DateOfBirthFieldLength&amp;amp;gt;08&amp;amp;lt;/DateOfBirthFieldLength&amp;amp;gt;&amp;amp;lt;" +
                  "DateOfBirth&amp;amp;gt;01051966&amp;amp;lt;/DateOfBirth&amp;amp;gt;&amp;amp;lt;GenderFieldLength&amp;amp;" +
                  "gt;01&amp;amp;lt;/GenderFieldLength&amp;amp;gt;&amp;amp;lt;Gender&amp;amp;gt;2&amp;amp;lt;/Gender&amp;amp;" +
                  "gt;&amp;amp;lt;/NameSegment&amp;amp;gt;&amp;amp;lt;IDSegment&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03" +
                  "&amp;amp;lt;/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;I01&amp;amp;lt;/SegmentTag&amp;amp;gt;" +
                  "&amp;amp;lt;IDType&amp;amp;gt;01&amp;amp;lt;/IDType&amp;amp;gt;&amp;amp;lt;IDNumberFieldLength&amp;amp;" +
                  "gt;10&amp;amp;lt;/IDNumberFieldLength&amp;amp;gt;&amp;amp;lt;IDNumber&amp;amp;gt;ACHPL0696C&amp;amp;lt;" +
                  "/IDNumber&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry" +
                  "&amp;amp;gt;&amp;amp;lt;/IDSegment&amp;amp;gt;&amp;amp;lt;Address&amp;amp;gt;&amp;amp;lt;AddressSegmentTag" +
                  "&amp;amp;gt;PA&amp;amp;lt;/AddressSegmentTag&amp;amp;gt;&amp;amp;lt;Length&amp;amp;gt;03&amp;amp;lt;" +
                  "/Length&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;A01&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;" +
                  "AddressLine1FieldLength&amp;amp;gt;38&amp;amp;lt;/AddressLine1FieldLength&amp;amp;gt;&amp;amp;lt;" +
                  "AddressLine1&amp;amp;gt;501 FIFTH FLOOR  PAM VILLE DMONTE PARK&amp;amp;lt;/AddressLine1&amp;amp;gt;" +
                  "&amp;amp;lt;AddressLine2FieldLength&amp;amp;gt;11&amp;amp;lt;/AddressLine2FieldLength&amp;amp;gt;&amp;" +
                  "amp;lt;AddressLine2&amp;amp;gt;MAHARASHTRA&amp;amp;lt;/AddressLine2&amp;amp;gt;&amp;amp;lt;StateCode" +
                  "&amp;amp;gt;27&amp;amp;lt;/StateCode&amp;amp;gt;&amp;amp;lt;PinCodeFieldLength&amp;amp;gt;06&amp;amp;" +
                  "lt;/PinCodeFieldLength&amp;amp;gt;&amp;amp;lt;PinCode&amp;amp;gt;400050&amp;amp;lt;/PinCode&amp;amp;" +
                  "gt;&amp;amp;lt;AddressCategory&amp;amp;gt;01&amp;amp;lt;/AddressCategory&amp;amp;gt;&amp;amp;lt;" +
                  "DateReported&amp;amp;gt;17042017&amp;amp;lt;/DateReported&amp;amp;gt;&amp;amp;lt;EnrichedThroughEnquiry" +
                  "&amp;amp;gt;Y&amp;amp;lt;/EnrichedThroughEnquiry&amp;amp;gt;&amp;amp;lt;/Address&amp;amp;gt;&amp;amp;" +
                  "lt;End&amp;amp;gt;&amp;amp;lt;SegmentTag&amp;amp;gt;ES07&amp;amp;lt;/SegmentTag&amp;amp;gt;&amp;amp;lt;" +
                  "TotalLength&amp;amp;gt;0000294&amp;amp;lt;/TotalLength&amp;amp;gt;&amp;amp;lt;/End&amp;amp;gt;&amp;amp;" +
                  "lt;/CreditReport&amp;amp;gt;&amp;amp;lt;/Root&amp;amp;gt;&amp;lt;/SecondaryReportXml&amp;gt;&#xD; &amp;" +
                  "lt;IsSucess&amp;gt;True&amp;lt;/IsSucess&amp;gt;&#xD; &amp;lt;/CibilBureauResponse&amp;gt;&#xD; &amp;" +
                  "lt;/Response&amp;gt;&#xD; &amp;lt;/DsCibilBureau&amp;gt;&#xD; &amp;lt;DsIDVision&amp;gt;&#xD; &amp;lt;" +
                  "CPVAttributes&amp;gt;&#xD; &amp;lt;Match&amp;gt;&#xD; &amp;lt;ContactabilityAadhaarTelephone1Status" +
                  "&amp;gt;-&amp;lt;/ContactabilityAadhaarTelephone1Status&amp;gt;&#xD; &amp;lt;" +
                  "ContactabilityAadhaarTelephone1Match&amp;gt;-&amp;lt;/ContactabilityAadhaarTelephone1Match&amp;gt;" +
                  "&#xD; &amp;lt;AddressAadhaarPermanentStatus&amp;gt;-&amp;lt;/AddressAadhaarPermanentStatus&amp;gt;" +
                  "&#xD; &amp;lt;AddressAadhaarPermanentMatch&amp;gt;-&amp;lt;/AddressAadhaarPermanentMatch&amp;gt;" +
                  "&#xD; &amp;lt;AddressAadhaarResidenceStatus&amp;gt;-&amp;lt;/AddressAadhaarResidenceStatus&amp;gt;" +
                  "&#xD; &amp;lt;AddressAadhaarResidenceMatch&amp;gt;-&amp;lt;/AddressAadhaarResidenceMatch&amp;gt;" +
                  "&#xD; &amp;lt;IDAadhaarIdentifierStatus&amp;gt;-&amp;lt;/IDAadhaarIdentifierStatus&amp;gt;&#xD; &amp;" +
                  "lt;IDAadhaarIdentifierMatch&amp;gt;-&amp;lt;/IDAadhaarIdentifierMatch&amp;gt;&#xD; &amp;lt;" +
                  "IDAadharGenderStatus&amp;gt;-&amp;lt;/IDAadharGenderStatus&amp;gt;&#xD; &amp;lt;IDAadharGenderMatch" +
                  "&amp;gt;-&amp;lt;/IDAadharGenderMatch&amp;gt;&#xD; &amp;lt;IDAadharDOBYearStatus&amp;gt;-&amp;lt;" +
                  "/IDAadharDOBYearStatus&amp;gt;&#xD; &amp;lt;IDAadharDOBYearMatch&amp;gt;-&amp;lt;/IDAadharDOBYearMatch" +
                  "&amp;gt;&#xD; &amp;lt;IDAadharNameStatus&amp;gt;-&amp;lt;/IDAadharNameStatus&amp;gt;&#xD; &amp;lt;" +
                  "IDAadharNameMatch&amp;gt;-&amp;lt;/IDAadharNameMatch&amp;gt;&#xD; &amp;lt;AddressVoterPermanentStatus" +
                  "&amp;gt;-&amp;lt;/AddressVoterPermanentStatus&amp;gt;&#xD; &amp;lt;AddressVoterPermanentMatch&amp;gt;" +
                  "-&amp;lt;/AddressVoterPermanentMatch&amp;gt;&#xD; &amp;lt;AddressVoterResidenceStatus&amp;gt;-&amp;lt;" +
                  "/AddressVoterResidenceStatus&amp;gt;&#xD; &amp;lt;AddressVoterResidenceMatch&amp;gt;-&amp;lt;" +
                  "/AddressVoterResidenceMatch&amp;gt;&#xD; &amp;lt;IDVoterIdentifierStatus&amp;gt;-&amp;lt;" +
                  "/IDVoterIdentifierStatus&amp;gt;&#xD; &amp;lt;IDVoterIdentifierMatch&amp;gt;-&amp;lt;/IDVoterIdentifierMatch" +
                  "&amp;gt;&#xD; &amp;lt;IDVoterGenderStatus&amp;gt;-&amp;lt;/IDVoterGenderStatus&amp;gt;&#xD; &amp;lt;" +
                  "IDVoterGenderMatch&amp;gt;-&amp;lt;/IDVoterGenderMatch&amp;gt;&#xD; &amp;lt;IDVoterNameStatus&amp;gt;" +
                  "-&amp;lt;/IDVoterNameStatus&amp;gt;&#xD; &amp;lt;IDVoterNameMatch&amp;gt;-&amp;lt;/IDVoterNameMatch" +
                  "&amp;gt;&#xD; &amp;lt;IDNSDLIdentifierStatus&amp;gt;NA&amp;lt;/IDNSDLIdentifierStatus&amp;gt;&#xD; &amp;" +
                  "lt;IDNSDLIdentifierMatch&amp;gt;100&amp;lt;/IDNSDLIdentifierMatch&amp;gt;&#xD; &amp;lt;IDNSDLNameStatus&amp;" +
                  "gt;NA&amp;lt;/IDNSDLNameStatus&amp;gt;&#xD; &amp;lt;IDNSDLNameMatch&amp;gt;89&amp;lt;/IDNSDLNameMatch&amp;gt;" +
                  "&#xD; &amp;lt;ContactabilityOfficeNumberStatus&amp;gt;-&amp;lt;/ContactabilityOfficeNumberStatus&amp;gt;" +
                  "&#xD; &amp;lt;ContactabilityOfficeNumberMatch&amp;gt;-&amp;lt;/ContactabilityOfficeNumberMatch&amp;gt;" +
                  "&#xD; &amp;lt;ContactabilityResidenceNumberStatus&amp;gt;-&amp;lt;/ContactabilityResidenceNumberStatus" +
                  "&amp;gt;&#xD; &amp;lt;ContactabilityResidenceNumberMatch&amp;gt;-&amp;lt;/ContactabilityResidenceNumberMatch" +
                  "&amp;gt;&#xD; &amp;lt;ContactabilityMobileNumberStatus&amp;gt;Pass&amp;lt;/ContactabilityMobileNumberStatus" +
                  "&amp;gt;&#xD; &amp;lt;ContactabilityMobileNumberMatch&amp;gt;100&amp;lt;/ContactabilityMobileNumberMatch" +
                  "&amp;gt;&#xD; &amp;lt;AddressCIBILOfficeStatus&amp;gt;-&amp;lt;/AddressCIBILOfficeStatus&amp;gt;" +
                  "&#xD; &amp;lt;AddressCIBILOfficeMatch&amp;gt;-&amp;lt;/AddressCIBILOfficeMatch&amp;gt;&#xD; &amp;" +
                  "lt;AddressCIBILPermanentStatus&amp;gt;Unclear&amp;lt;/AddressCIBILPermanentStatus&amp;gt;&#xD; &amp;" +
                  "lt;AddressCIBILPermanentMatch&amp;gt;62.0&amp;lt;/AddressCIBILPermanentMatch&amp;gt;&#xD; &amp;lt;" +
                  "AddressCIBILResidenceStatus&amp;gt;-&amp;lt;/AddressCIBILResidenceStatus&amp;gt;&#xD; &amp;lt;" +
                  "AddressCIBILResidenceMatch&amp;gt;-&amp;lt;/AddressCIBILResidenceMatch&amp;gt;&#xD; &amp;lt;" +
                  "IDCIBILDrivingLicenseIDStatus&amp;gt;-&amp;lt;/IDCIBILDrivingLicenseIDStatus&amp;gt;&#xD; &amp;lt;" +
                  "IDCIBILDrivingLicenseIDMatch&amp;gt;-&amp;lt;/IDCIBILDrivingLicenseIDMatch&amp;gt;&#xD; &amp;lt;" +
                  "IDCIBILRationCardIDStatus&amp;gt;-&amp;lt;/IDCIBILRationCardIDStatus&amp;gt;&#xD; &amp;lt;" +
                  "IDCIBILRationCardIDMatch&amp;gt;-&amp;lt;/IDCIBILRationCardIDMatch&amp;gt;&#xD; &amp;lt;IDCIBILPassportIDStatus" +
                  "&amp;gt;-&amp;lt;/IDCIBILPassportIDStatus&amp;gt;&#xD; &amp;lt;IDCIBILPassportIDMatch&amp;gt;" +
                  "-&amp;lt;/IDCIBILPassportIDMatch&amp;gt;&#xD; &amp;lt;IDCIBILAadhaarIDStatus&amp;gt;-&amp;lt;" +
                  "/IDCIBILAadhaarIDStatus&amp;gt;&#xD; &amp;lt;IDCIBILAadhaarIDMacth&amp;gt;-&amp;lt;/IDCIBILAadhaarIDMacth" +
                  "&amp;gt;&#xD; &amp;lt;IDCIBILVoterIDStatus&amp;gt;-&amp;lt;/IDCIBILVoterIDStatus&amp;gt;&#xD; &amp;lt;" +
                  "IDCIBILVoterIDMatch&amp;gt;-&amp;lt;/IDCIBILVoterIDMatch&amp;gt;&#xD; &amp;lt;IDCIBILPANStatus&amp;gt;" +
                  "Pass&amp;lt;/IDCIBILPANStatus&amp;gt;&#xD; &amp;lt;IDCIBILPANMatch&amp;gt;100&amp;lt;/IDCIBILPANMatch" +
                  "&amp;gt;&#xD; &amp;lt;IDCIBILGenderStatus&amp;gt;Fail&amp;lt;/IDCIBILGenderStatus&amp;gt;&#xD; &amp;" +
                  "lt;IDCIBILGenderMatch&amp;gt;0&amp;lt;/IDCIBILGenderMatch&amp;gt;&#xD; &amp;lt;IDCIBILDOBStatus&amp;" +
                  "gt;Pass&amp;lt;/IDCIBILDOBStatus&amp;gt;&#xD; &amp;lt;IDCIBILDOBMatch&amp;gt;100&amp;lt;/IDCIBILDOBMatch" +
                  "&amp;gt;&#xD; &amp;lt;IDCIBILNameStatus&amp;gt;Pass&amp;lt;/IDCIBILNameStatus&amp;gt;&#xD; &amp;lt;" +
                  "IDCIBILNameMatch&amp;gt;100&amp;lt;/IDCIBILNameMatch&amp;gt;&#xD; &amp;lt;ExceptionMessage&amp;gt;" +
                  "&#xD; &amp;lt;PANMessage&amp;gt;Unable to retrieve PAN Information&amp;lt;/PANMessage&amp;gt;" +
                  "&#xD; &amp;lt;/ExceptionMessage&amp;gt;&#xD; &amp;lt;/Match&amp;gt;&#xD; &amp;lt;EnquiryInfo&amp;" +
                  "gt;&#xD; &amp;lt;MobileNumber&amp;gt;9012783067&amp;lt;/MobileNumber&amp;gt;&#xD; &amp;lt;" +
                  "PermanentAddressCity&amp;gt;MUMBAI&amp;lt;/PermanentAddressCity&amp;gt;&#xD; &amp;lt;" +
                  "PermanentAddressState&amp;gt;MAHARASHTRA&amp;lt;/PermanentAddressState&amp;gt;&#xD; &amp;lt;" +
                  "PermanentAddressStateCode&amp;gt;27&amp;lt;/PermanentAddressStateCode&amp;gt;&#xD; &amp;lt;" +
                  "PermanentAddressPinCode&amp;gt;400050&amp;lt;/PermanentAddressPinCode&amp;gt;&#xD; &amp;lt;" +
                  "PermanentAddressLine&amp;gt;501 FIFTH FLOOR  PAM VILLE DMONTE PARK N.177 mahalakshmi nagar &amp;lt;" +
                  "/PermanentAddressLine&amp;gt;&#xD; &amp;lt;PAN&amp;gt;ADZPR4147H&amp;lt;/PAN&amp;gt;&#xD; &amp;" +
                  "lt;EmailID&amp;gt;&#xD; &amp;lt;/EmailID&amp;gt;&#xD; &amp;lt;DateofBirth&amp;gt;01/05/1966&amp;" +
                  "lt;/DateofBirth&amp;gt;&#xD; &amp;lt;Gender&amp;gt;MALE&amp;lt;/Gender&amp;gt;&#xD; &amp;lt;LastName" +
                  "&amp;gt;RAJAGOPALAN&amp;lt;/LastName&amp;gt;&#xD; &amp;lt;MiddleName&amp;gt;K&amp;lt;/MiddleName&amp;" +
                  "gt;&#xD; &amp;lt;FirstName&amp;gt;MABEL&amp;lt;/FirstName&amp;gt;&#xD; &amp;lt;/EnquiryInfo&amp;gt;" +
                  "&#xD; &amp;lt;CIBILReport&amp;gt;&#xD; &amp;lt;Telephones&amp;gt;&#xD; &amp;lt;Telephone&amp;gt;" +
                  "&#xD; &amp;lt;DateReported&amp;gt;&#xD; &amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry" +
                  "&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;Match&amp;gt;NO_MATCH&amp;lt;" +
                  "/Match&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;T01&amp;lt;/SegmentTag&amp;gt;&#xD; &amp;lt;FID&amp;" +
                  "gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;SNo&amp;gt;0812330159&amp;lt;/SNo&amp;gt;&#xD; &amp;" +
                  "lt;TelephoneType&amp;gt;01&amp;lt;/TelephoneType&amp;gt;&#xD; &amp;lt;TelephoneNumber&amp;gt;" +
                  "9863454554&amp;lt;/TelephoneNumber&amp;gt;&#xD; &amp;lt;/Telephone&amp;gt;&#xD; &amp;lt;Telephone" +
                  "&amp;gt;&#xD; &amp;lt;DateReported&amp;gt;&#xD; &amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;" +
                  "EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;Match&amp;" +
                  "gt;NO_MATCH&amp;lt;/Match&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;T02&amp;lt;/SegmentTag&amp;gt;" +
                  "&#xD; &amp;lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;SNo&amp;gt;0783351461&amp;" +
                  "lt;/SNo&amp;gt;&#xD; &amp;lt;TelephoneType&amp;gt;01&amp;lt;/TelephoneType&amp;gt;&#xD; &amp;lt;" +
                  "TelephoneNumber&amp;gt;9840714555&amp;lt;/TelephoneNumber&amp;gt;&#xD; &amp;lt;/Telephone&amp;gt;" +
                  "&#xD; &amp;lt;Telephone&amp;gt;&#xD; &amp;lt;DateReported&amp;gt;&#xD; &amp;lt;/DateReported&amp;" +
                  "gt;&#xD; &amp;lt;EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;gt;" +
                  "&#xD; &amp;lt;Match&amp;gt;NO_MATCH&amp;lt;/Match&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;T03" +
                  "&amp;lt;/SegmentTag&amp;gt;&#xD; &amp;lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;" +
                  "lt;SNo&amp;gt;0779760461&amp;lt;/SNo&amp;gt;&#xD; &amp;lt;TelephoneType&amp;gt;01&amp;lt;" +
                  "/TelephoneType&amp;gt;&#xD; &amp;lt;TelephoneNumber&amp;gt;9870115963&amp;lt;/TelephoneNumber" +
                  "&amp;gt;&#xD; &amp;lt;/Telephone&amp;gt;&#xD; &amp;lt;Telephone&amp;gt;&#xD; &amp;lt;" +
                  "DateReported&amp;gt;&#xD; &amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry" +
                  "&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;Match&amp;gt;NSN_MATCH&amp;" +
                  "lt;/Match&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;T04&amp;lt;/SegmentTag&amp;gt;&#xD; &amp;lt;" +
                  "FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;SNo&amp;gt;0725147809&amp;lt;/SNo&amp;" +
                  "gt;&#xD; &amp;lt;TelephoneType&amp;gt;01&amp;lt;/TelephoneType&amp;gt;&#xD; &amp;lt;" +
                  "TelephoneNumber&amp;gt;9012783067&amp;lt;/TelephoneNumber&amp;gt;&#xD; &amp;lt;/Telephone" +
                  "&amp;gt;&#xD; &amp;lt;/Telephones&amp;gt;&#xD; &amp;lt;Addresses&amp;gt;&#xD; &amp;lt;Address" +
                  "&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;" +
                  "gt;&#xD; &amp;lt;Match&amp;gt;46.0&amp;lt;/Match&amp;gt;&#xD; &amp;lt;DateReported&amp;gt;" +
                  "09/01/2017&amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;A04&amp;lt;/SegmentTag" +
                  "&amp;gt;&#xD; &amp;lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;SNo&amp;gt;" +
                  "0811649987&amp;lt;/SNo&amp;gt;&#xD; &amp;lt;PinCode&amp;gt;110002&amp;lt;/PinCode&amp;gt;&#xD; &amp;" +
                  "lt;StateCode&amp;gt;07&amp;lt;/StateCode&amp;gt;&#xD; &amp;lt;Address&amp;gt;" +
                  "PAM  VILLE DMONTE PARK PAM  VILLE DMONTE PARK DELHI DELHI&amp;lt;/Address&amp;gt;&#xD; &amp;" +
                  "lt;AddressCategory&amp;gt;02&amp;lt;/AddressCategory&amp;gt;&#xD; &amp;lt;/Address&amp;gt;" +
                  "&#xD; &amp;lt;Address&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;" +
                  "/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;Match&amp;gt;0.0&amp;lt;/Match&amp;gt;&#xD; &amp;lt;" +
                  "DateReported&amp;gt;19/05/2017&amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;A03" +
                  "&amp;lt;/SegmentTag&amp;gt;&#xD; &amp;lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;" +
                  "lt;SNo&amp;gt;0851393310&amp;lt;/SNo&amp;gt;&#xD; &amp;lt;PinCode&amp;gt;400001&amp;lt;/PinCode" +
                  "&amp;gt;&#xD; &amp;lt;StateCode&amp;gt;27&amp;lt;/StateCode&amp;gt;&#xD; &amp;lt;Address&amp;gt;" +
                  "11 KAMLA NAGAR 2ND FLOOR  9TH SCTOR KK NAGAR MAHARASHTRA&amp;lt;/Address&amp;gt;&#xD; &amp;lt;" +
                  "AddressCategory&amp;gt;02&amp;lt;/AddressCategory&amp;gt;&#xD; &amp;lt;/Address&amp;gt;" +
                  "&#xD; &amp;lt;Address&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;" +
                  "/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;Match&amp;gt;0.0&amp;lt;/Match&amp;gt;&#xD; &amp;" +
                  "lt;DateReported&amp;gt;24/07/2017&amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;" +
                  "A02&amp;lt;/SegmentTag&amp;gt;&#xD; &amp;lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;" +
                  "lt;SNo&amp;gt;0869261376&amp;lt;/SNo&amp;gt;&#xD; &amp;lt;PinCode&amp;gt;600100&amp;lt;/PinCode" +
                  "&amp;gt;&#xD; &amp;lt;StateCode&amp;gt;33&amp;lt;/StateCode&amp;gt;&#xD; &amp;lt;Address&amp;" +
                  "gt;ETREWREWRDSFDSFSF FDSFDSFDSFDSF DSFDSFDSFDSFDSFDSFDSF TAMIL NADU&amp;lt;/Address&amp;gt;" +
                  "&#xD; &amp;lt;AddressCategory&amp;gt;02&amp;lt;/AddressCategory&amp;gt;&#xD; &amp;lt;/Address&amp;gt;" +
                  "&#xD; &amp;lt;Address&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry" +
                  "&amp;gt;&#xD; &amp;lt;Match&amp;gt;62.0&amp;lt;/Match&amp;gt;&#xD; &amp;lt;DateReported&amp;gt;30/08/2017&amp;" +
                  "lt;/DateReported&amp;gt;&#xD; &amp;lt;SegmentTag&amp;gt;A01&amp;lt;/SegmentTag&amp;gt;&#xD; &amp;lt;FID&amp;" +
                  "gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;SNo&amp;gt;0873442945&amp;lt;/SNo&amp;gt;&#xD; &amp;lt;" +
                  "PinCode&amp;gt;560008&amp;lt;/PinCode&amp;gt;&#xD; &amp;lt;StateCode&amp;gt;29&amp;lt;/StateCode&amp;" +
                  "gt;&#xD; &amp;lt;Address&amp;gt;501 FIFTH FLOOR PAM VILLE DMONTE PARK KARNATAKA&amp;lt;/Address&amp;" +
                  "gt;&#xD; &amp;lt;AddressCategory&amp;gt;01&amp;lt;/AddressCategory&amp;gt;&#xD; &amp;lt;/Address&amp;" +
                  "gt;&#xD; &amp;lt;/Addresses&amp;gt;&#xD; &amp;lt;Identifiers&amp;gt;&#xD; &amp;lt;Identifier&amp;gt;" +
                  "&#xD; &amp;lt;DateReported&amp;gt;&#xD; &amp;lt;/DateReported&amp;gt;&#xD; &amp;lt;EnrichedThroughEnquiry" +
                  "&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;" +
                  "gt;&#xD; &amp;lt;SNo&amp;gt;0181288190&amp;lt;/SNo&amp;gt;&#xD; &amp;lt;IdNumber&amp;gt;ADZPR4147H&amp;" +
                  "lt;/IdNumber&amp;gt;&#xD; &amp;lt;IdType&amp;gt;01&amp;lt;/IdType&amp;gt;&#xD; &amp;lt;/Identifier&amp;" +
                  "gt;&#xD; &amp;lt;Identifier&amp;gt;&#xD; &amp;lt;DateReported&amp;gt;&#xD; &amp;lt;/DateReported&amp;" +
                  "gt;&#xD; &amp;lt;EnrichedThroughEnquiry&amp;gt;&#xD; &amp;lt;/EnrichedThroughEnquiry&amp;gt;&#xD; &amp;" +
                  "lt;FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;SNo&amp;gt;0179171887&amp;lt;/SNo&amp;gt;" +
                  "&#xD; &amp;lt;IdNumber&amp;gt;URH0817686&amp;lt;/IdNumber&amp;gt;&#xD; &amp;lt;IdType&amp;gt;03&amp;" +
                  "lt;/IdType&amp;gt;&#xD; &amp;lt;/Identifier&amp;gt;&#xD; &amp;lt;/Identifiers&amp;gt;&#xD; &amp;lt;" +
                  "FID&amp;gt;0090133866&amp;lt;/FID&amp;gt;&#xD; &amp;lt;Gender&amp;gt;1&amp;lt;/Gender&amp;gt;&#xD; &amp;" +
                  "lt;DateOfBirth&amp;gt;01/05/1966&amp;lt;/DateOfBirth&amp;gt;&#xD; &amp;lt;EnquiryControlNumber&amp;gt;" +
                  "002148411007&amp;lt;/EnquiryControlNumber&amp;gt;&#xD; &amp;lt;ConsumerName&amp;gt;MABEL KR RAJAGOPALAN" +
                  "&amp;lt;/ConsumerName&amp;gt;&#xD; &amp;lt;/CIBILReport&amp;gt;&#xD; &amp;lt;VerificationScore&amp;gt;" +
                  "&#xD; &amp;lt;IDNameScore&amp;gt;100&amp;lt;/IDNameScore&amp;gt;&#xD; &amp;lt;IDNameStatus/&amp;gt;" +
                  "&#xD; &amp;lt;IDAltNameScore&amp;gt;0&amp;lt;/IDAltNameScore&amp;gt;&#xD; &amp;lt;IDAltDOBScore&amp;" +
                  "gt;0&amp;lt;/IDAltDOBScore&amp;gt;&#xD; &amp;lt;IDDOBScore&amp;gt;100&amp;lt;/IDDOBScore&amp;gt;&#xD; &amp;" +
                  "lt;IDDOBStatus/&amp;gt;&#xD; &amp;lt;IDAltGenderScore&amp;gt;0&amp;lt;/IDAltGenderScore&amp;gt;&#xD; &amp;" +
                  "lt;IDGenderScore&amp;gt;0&amp;lt;/IDGenderScore&amp;gt;&#xD; &amp;lt;IDGenderStatus/&amp;gt;&#xD; &amp;lt;" +
                  "IDAltIdentifierScore&amp;gt;0&amp;lt;/IDAltIdentifierScore&amp;gt;&#xD; &amp;lt;IDIdentifierScore&amp;" +
                  "gt;100&amp;lt;/IDIdentifierScore&amp;gt;&#xD; &amp;lt;IDIdentifierStatus/&amp;gt;&#xD; &amp;lt;" +
                  "AddAltResScore&amp;gt;0&amp;lt;/AddAltResScore&amp;gt;&#xD; &amp;lt;AddressResidenceScore&amp;gt;" +
                  "0&amp;lt;/AddressResidenceScore&amp;gt;&#xD; &amp;lt;AddressResidenceStatus/&amp;gt;&#xD; &amp;lt;" +
                  "AddAltPerScore&amp;gt;0&amp;lt;/AddAltPerScore&amp;gt;&#xD; &amp;lt;AddressPermanentScore&amp;gt;" +
                  "62&amp;lt;/AddressPermanentScore&amp;gt;&#xD; &amp;lt;AddressPermanentStatus/&amp;gt;&#xD; &amp;lt;" +
                  "AddressOfficeScore&amp;gt;0&amp;lt;/AddressOfficeScore&amp;gt;&#xD; &amp;lt;AddressOfficeStatus/" +
                  "&amp;gt;&#xD; &amp;lt;ConAltPhoneScore&amp;gt;0&amp;lt;/ConAltPhoneScore&amp;gt;&#xD; &amp;lt;" +
                  "ContactabilityTelephone1Score&amp;gt;100&amp;lt;/ContactabilityTelephone1Score&amp;gt;&#xD; &amp;" +
                  "lt;ContactabilityTelephone1Status/&amp;gt;&#xD; &amp;lt;ContactabilityTelephone2Score&amp;gt;" +
                  "0&amp;lt;/ContactabilityTelephone2Score&amp;gt;&#xD; &amp;lt;ContactabilityTelephone2Status/&amp;" +
                  "gt;&#xD; &amp;lt;ContactabilityTelephone3Score&amp;gt;0&amp;lt;/ContactabilityTelephone3Score&amp;" +
                  "gt;&#xD; &amp;lt;ContactabilityTelephone3Status/&amp;gt;&#xD; &amp;lt;FinalIdentityScore&amp;gt;" +
                  "85&amp;lt;/FinalIdentityScore&amp;gt;&#xD; &amp;lt;FinalIdentityStatus/&amp;gt;&#xD; &amp;lt;" +
                  "FinalAddressScore&amp;gt;62&amp;lt;/FinalAddressScore&amp;gt;&#xD; &amp;lt;FinalAddressStatus/" +
                  "&amp;gt;&#xD; &amp;lt;FinalContactabilityScore&amp;gt;100&amp;lt;/FinalContactabilityScore&amp;" +
                  "gt;&#xD; &amp;lt;FinalContactabilityStatus/&amp;gt;&#xD; &amp;lt;FinalVerificationScore&amp;gt;" +
                  "79&amp;lt;/FinalVerificationScore&amp;gt;&#xD; &amp;lt;FinalVerificationStatus/&amp;gt;&#xD; &amp;" +
                  "lt;/VerificationScore&amp;gt;&#xD; &amp;lt;CIBILDetect&amp;gt;&#xD; &amp;lt;Responseheader&amp;" +
                  "gt;&#xD; &amp;lt;ProcessDate&amp;gt;20/02/2018&amp;lt;/ProcessDate&amp;gt;&#xD; &amp;lt;ErrorCount" +
                  "&amp;gt;0&amp;lt;/ErrorCount&amp;gt;&#xD; &amp;lt;NoHitCount&amp;gt;1&amp;lt;/NoHitCount&amp;gt;" +
                  "&#xD; &amp;lt;HitCount&amp;gt;0&amp;lt;/HitCount&amp;gt;&#xD; &amp;lt;/Responseheader&amp;gt;&#xD; &amp;" +
                  "lt;/CIBILDetect&amp;gt;&#xD; &amp;lt;Velocity&amp;gt;&#xD; &amp;lt;ProductInfo1month&amp;gt;&#xD; &amp;" +
                  "lt;NoOfEnquiries&amp;gt;9&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;40&amp;lt;" +
                  "/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo1month&amp;gt;&#xD; &amp;lt;ProductInfo1month&amp;" +
                  "gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;34&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType" +
                  "&amp;gt;05&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo1month&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo1month&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;13&amp;lt;/NoOfEnquiries&amp;gt;" +
                  "&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo1month" +
                  "&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;9&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;40&amp;lt;/ProductType&amp;gt;&#xD; &amp;" +
                  "lt;/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;lt;NoOfEnquiries" +
                  "&amp;gt;34&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType" +
                  "&amp;gt;&#xD; &amp;lt;/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;" +
                  "lt;NoOfEnquiries&amp;gt;18&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;" +
                  "/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;" +
                  "&#xD; &amp;lt;NoOfEnquiries&amp;gt;2&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;" +
                  "00&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo3months&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo6months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;9&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;" +
                  "lt;ProductType&amp;gt;40&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo6months&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo6months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;34&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;" +
                  "lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo6months&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo6months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;8&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;" +
                  "ProductType&amp;gt;00&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo6months&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo6months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;1&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;" +
                  "ProductType&amp;gt;01&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo6months&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo6months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;2&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;" +
                  "ProductType&amp;gt;51&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo6months&amp;gt;&#xD; &amp;lt;" +
                  "ProductInfo6months&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;39&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;" +
                  "ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/ProductInfo6months&amp;gt;&#xD; &amp;lt;" +
                  "/Velocity&amp;gt;&#xD; &amp;lt;ECR&amp;gt;&#xD; &amp;lt;ProductInfo1month&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;" +
                  "gt;&#xD; &amp;lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;" +
                  "&#xD; &amp;lt;NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;" +
                  "gt;9&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;40&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo1month&amp;gt;&#xD; &amp;lt;ProductInfo1month&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;lt;" +
                  "/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;34&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo1month&amp;gt;&#xD; &amp;lt;ProductInfo1month&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;13&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo1month&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;" +
                  "&#xD; &amp;lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;" +
                  "&#xD; &amp;lt;NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;" +
                  "gt;9&amp;lt;/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;40&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;34&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;18&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo3months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;2&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;00&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo3months&amp;gt;&#xD; &amp;lt;ProductInfo6months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt" +
                  ";NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;9&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;40&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo6months&amp;gt;&#xD; &amp;lt;ProductInfo6months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;34&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo6months&amp;gt;&#xD; &amp;lt;ProductInfo6months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;8&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;00&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo6months&amp;gt;&#xD; &amp;lt;ProductInfo6months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;lt;" +
                  "/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;1&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;01&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo6months&amp;gt;&#xD; &amp;lt;ProductInfo6months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;2&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;51&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo6months&amp;gt;&#xD; &amp;lt;ProductInfo6months&amp;gt;&#xD; &amp;lt;AvgECRValue&amp;gt;&#xD; &amp;" +
                  "lt;/AvgECRValue&amp;gt;&#xD; &amp;lt;ECRValue&amp;gt;No Tradeline Opened&amp;lt;/ECRValue&amp;gt;&#xD; &amp;lt;" +
                  "NoOfTradeLinesOpened&amp;gt;0&amp;lt;/NoOfTradeLinesOpened&amp;gt;&#xD; &amp;lt;NoOfEnquiries&amp;gt;39&amp;lt;" +
                  "/NoOfEnquiries&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;" +
                  "/ProductInfo6months&amp;gt;&#xD; &amp;lt;/ECR&amp;gt;&#xD; &amp;lt;AverageECR&amp;gt;&#xD; &amp;lt;AverageECR1month&amp;" +
                  "gt;&#xD; &amp;lt;AVGECR&amp;gt;19.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;40&amp;lt;" +
                  "/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR1month&amp;gt;&#xD; &amp;lt;AverageECR1month&amp;gt;&#xD; &amp;lt;" +
                  "AVGECR&amp;gt;18.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;gt;&#xD; &amp;" +
                  "lt;/AverageECR1month&amp;gt;&#xD; &amp;lt;AverageECR1month&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;18.1&amp;lt;/AVGECR&amp;" +
                  "gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR1month&amp;gt;&#xD; &amp;lt;" +
                  "AverageECR3months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;19.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;" +
                  "40&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR3months&amp;gt;&#xD; &amp;lt;AverageECR3months&amp;gt;" +
                  "&#xD; &amp;lt;AVGECR&amp;gt;18.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;gt;" +
                  "&#xD; &amp;lt;/AverageECR3months&amp;gt;&#xD; &amp;lt;AverageECR3months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;18.1&amp;" +
                  "lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR3months&amp;" +
                  "gt;&#xD; &amp;lt;AverageECR3months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;19.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;" +
                  "ProductType&amp;gt;00&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR3months&amp;gt;&#xD; &amp;lt;" +
                  "AverageECR6months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;19.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;" +
                  "40&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR6months&amp;gt;&#xD; &amp;lt;AverageECR6months&amp;gt;" +
                  "&#xD; &amp;lt;AVGECR&amp;gt;18.2&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;05&amp;lt;/ProductType&amp;" +
                  "gt;&#xD; &amp;lt;/AverageECR6months&amp;gt;&#xD; &amp;lt;AverageECR6months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;19.2&amp;" +
                  "lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;00&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR6months&amp;" +
                  "gt;&#xD; &amp;lt;AverageECR6months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;18.6&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;" +
                  "ProductType&amp;gt;01&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR6months&amp;gt;&#xD; &amp;lt;AverageECR6months&amp;" +
                  "gt;&#xD; &amp;lt;AVGECR&amp;gt;19&amp;lt;/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;51&amp;lt;/ProductType&amp;gt;" +
                  "&#xD; &amp;lt;/AverageECR6months&amp;gt;&#xD; &amp;lt;AverageECR6months&amp;gt;&#xD; &amp;lt;AVGECR&amp;gt;18.1&amp;lt;" +
                  "/AVGECR&amp;gt;&#xD; &amp;lt;ProductType&amp;gt;10&amp;lt;/ProductType&amp;gt;&#xD; &amp;lt;/AverageECR6months&amp;gt;" +
                  "&#xD; &amp;lt;/AverageECR&amp;gt;&#xD; &amp;lt;WilfulDefaultDetails&amp;gt;&#xD; &amp;lt;SuitfiledandWilfuldefault&amp;gt;" +
                  "&#xD; &amp;lt;Status&amp;gt;No&amp;lt;/Status&amp;gt;&#xD; &amp;lt;/SuitfiledandWilfuldefault&amp;gt;&#xD; &amp;lt;" +
                  "WilfulDefault&amp;gt;&#xD; &amp;lt;Status&amp;gt;No&amp;lt;/Status&amp;gt;&#xD; &amp;lt;/WilfulDefault&amp;gt;" +
                  "&#xD; &amp;lt;SuitFiled&amp;gt;&#xD; &amp;lt;Status&amp;gt;No&amp;lt;/Status&amp;gt;&#xD; &amp;lt;/SuitFiled&amp;gt;" +
                  "&#xD; &amp;lt;/WilfulDefaultDetails&amp;gt;&#xD; &amp;lt;/CPVAttributes&amp;gt;&#xD; &amp;lt;ReturnMessage&amp;gt;;" +
                  "Please configure VOTER;Please configure UIDAI&amp;lt;/ReturnMessage&amp;gt;&#xD; &amp;lt;/DsIDVision&amp;gt;&#xD; &amp;" +
                  "lt;/Applicant&amp;gt;&#xD; &amp;lt;/Applicants&amp;gt;&#xD; &lt;/Field&gt;&lt;Field key=\"ApplicationData\"&gt;&amp;lt;" +
                  "ApplicationData&amp;gt;&#xD; &amp;lt;ReferenceNumber&amp;gt;10064324&amp;lt;/ReferenceNumber&amp;gt;&#xD; &amp;lt;" +
                  "RepaymentPeriodInMonths&amp;gt;34&amp;lt;/RepaymentPeriodInMonths&amp;gt;&#xD; &amp;lt;Product&amp;gt;&#xD; &amp;lt;" +
                  "/Product&amp;gt;&#xD; &amp;lt;Amount&amp;gt;10000000&amp;lt;/Amount&amp;gt;&#xD; &amp;lt;Purpose&amp;gt;05&amp;lt;" +
                  "/Purpose&amp;gt;&#xD; &amp;lt;GSTStateCode&amp;gt;27&amp;lt;/GSTStateCode&amp;gt;&#xD; &amp;lt;" +
                  "ConsumerConsentForUIDAIAuthentication&amp;gt;Y&amp;lt;/ConsumerConsentForUIDAIAuthentication&amp;gt;&#xD; &amp;" +
                  "lt;ExternalApplicationID&amp;gt;01&amp;lt;/ExternalApplicationID&amp;gt;&#xD; &amp;lt;EnquiryPurpose&amp;gt;80&amp;" +
                  "lt;/EnquiryPurpose&amp;gt;&#xD; &amp;lt;EnquiryAmount&amp;gt;30000&amp;lt;/EnquiryAmount&amp;gt;&#xD; &amp;lt;" +
                  "IDVMemberCode&amp;gt;NB6605&amp;lt;/IDVMemberCode&amp;gt;&#xD; &amp;lt;SkipDSTuIDVisionFlag&amp;gt;False&amp;lt;" +
                  "/SkipDSTuIDVisionFlag&amp;gt;&#xD; &amp;lt;SkipDSTuNtcFlag&amp;gt;False&amp;lt;/SkipDSTuNtcFlag&amp;gt;&#xD; &amp;" +
                  "lt;SkipCibilBureauFlag&amp;gt;False&amp;lt;/SkipCibilBureauFlag&amp;gt;&#xD; &amp;lt;Milestone&amp;gt;&#xD; &amp;" +
                  "lt;Step&amp;gt;CIBIL Bureau call&amp;lt;/Step&amp;gt;&#xD; &amp;lt;Step&amp;gt;IDVision Score&amp;lt;/Step&amp;gt;" +
                  "&#xD; &amp;lt;Step&amp;gt;IDVision check&amp;lt;/Step&amp;gt;&#xD; &amp;lt;Step&amp;gt;CPV Verification Call&amp;lt;" +
                  "/Step&amp;gt;&#xD; &amp;lt;Step&amp;gt;New To Credit Score Check&amp;lt;/Step&amp;gt;&#xD; &amp;lt;/Milestone&amp;gt;" +
                  "&#xD; &amp;lt;Start&amp;gt;2018-02-20T06:34:02.1569764Z&amp;lt;/Start&amp;gt;&#xD; &amp;lt;SkipMFICIRPuller&amp;gt;" +
                  "True&amp;lt;/SkipMFICIRPuller&amp;gt;&#xD; &amp;lt;IsMFTUEF&amp;gt;False&amp;lt;/IsMFTUEF&amp;gt;&#xD; &amp;lt;DTTrail&amp;" +
                  "gt;&#xD; &amp;lt;Step&amp;gt;&#xD; &amp;lt;Name&amp;gt;CIBIL Bureau call&amp;lt;/Name&amp;gt;&#xD; &amp;lt;Duration&amp;" +
                  "gt;00:00:08.1276143&amp;lt;/Duration&amp;gt;&#xD; &amp;lt;/Step&amp;gt;&#xD; &amp;lt;Step&amp;gt;&#xD; &amp;lt;Name&amp;" +
                  "gt;IDVision check&amp;lt;/Name&amp;gt;&#xD; &amp;lt;Duration&amp;gt;00:00:38.0806713&amp;lt;/Duration&amp;gt;&#xD; &amp;" +
                  "lt;/Step&amp;gt;&#xD; &amp;lt;Step&amp;gt;&#xD; &amp;lt;Name&amp;gt;IDVision Score&amp;lt;/Name&amp;gt;&#xD; &amp;lt;" +
                  "Duration&amp;gt;00:00:03.4164060&amp;lt;/Duration&amp;gt;&#xD; &amp;lt;/Step&amp;gt;&#xD; &amp;lt;Step&amp;gt;&#xD; &amp;" +
                  "lt;Name&amp;gt;New To Credit Score Check&amp;lt;/Name&amp;gt;&#xD; &amp;lt;Duration&amp;gt;00:00:09.1260160&amp;lt;" +
                  "/Duration&amp;gt;&#xD; &amp;lt;/Step&amp;gt;&#xD; &amp;lt;/DTTrail&amp;gt;&#xD; &amp;lt;InputValReasonCodes&amp;gt;" +
                  "&#xD; &amp;lt;/InputValReasonCodes&amp;gt;&#xD; &amp;lt;User&amp;gt;CreditExchange_Admin&amp;lt;/User&amp;gt;&#xD; &amp;" +
                  "lt;BusinessUnitId&amp;gt;1&amp;lt;/BusinessUnitId&amp;gt;&#xD; &amp;lt;ApplicationId&amp;gt;11784848&amp;lt;/ApplicationId&amp;" +
                  "gt;&#xD; &amp;lt;SolutionSetId&amp;gt;1776&amp;lt;/SolutionSetId&amp;gt;&#xD; &amp;lt;EnvironmentTypeId&amp;gt;1&amp;" +
                  "lt;/EnvironmentTypeId&amp;gt;&#xD; &amp;lt;EnvironmentType&amp;gt;UAT&amp;lt;/EnvironmentType&amp;gt;&#xD; &amp;lt;" +
                  "SkipTuVerificationFlag&amp;gt;False&amp;lt;/SkipTuVerificationFlag&amp;gt;&#xD; &amp;lt;ReturnMessage&amp;gt;" +
                  "NTC Component Execution was Successfull&amp;lt;/ReturnMessage&amp;gt;&#xD; &amp;lt;Address1Category&amp;gt;01&amp;" +
                  "lt;/Address1Category&amp;gt;&#xD; &amp;lt;Address2Category&amp;gt;0&amp;lt;/Address2Category&amp;gt;&#xD; &amp;lt;" +
                  "TUXF_Default_HDR_Amount&amp;gt;000000000&amp;lt;/TUXF_Default_HDR_Amount&amp;gt;&#xD; &amp;lt;TUXF_Default_HDR_Purpose&amp;" +
                  "gt;05&amp;lt;/TUXF_Default_HDR_Purpose&amp;gt;&#xD; &amp;lt;SkipDetectFlag&amp;gt;False&amp;lt;/SkipDetectFlag&amp;gt;&#xD; &amp;" +
                  "lt;SkipRecursiveMatchFlag&amp;gt;True&amp;lt;/SkipRecursiveMatchFlag&amp;gt;&#xD; &amp;lt;SkipWilfulDefaultFlag&amp;gt;" +
                  "False&amp;lt;/SkipWilfulDefaultFlag&amp;gt;&#xD; &amp;lt;SkipVelocityECRFlag&amp;gt;False&amp;lt;/SkipVelocityECRFlag&amp;" +
                  "gt;&#xD; &amp;lt;SkipDateReportedFlag&amp;gt;False&amp;lt;/SkipDateReportedFlag&amp;gt;&#xD; &amp;lt;" +
                  "SkipInformationTaggingFlag&amp;gt;False&amp;lt;/SkipInformationTaggingFlag&amp;gt;&#xD; &amp;lt;SkipDETECTREPORT&amp;gt;" +
                  "False&amp;lt;/SkipDETECTREPORT&amp;gt;&#xD; &amp;lt;SkipAlertsFlag&amp;gt;False&amp;lt;/SkipAlertsFlag&amp;gt;&#xD; &amp;" +
                  "lt;SkipNSDLRequestFlag&amp;gt;False&amp;lt;/SkipNSDLRequestFlag&amp;gt;&#xD; &amp;lt;SkipUIDAIRequestFlag&amp;" +
                  "gt;True&amp;lt;/SkipUIDAIRequestFlag&amp;gt;&#xD; &amp;lt;SkipCIBILDetectRequestFlag&amp;gt;False&amp;lt;" +
                  "/SkipCIBILDetectRequestFlag&amp;gt;&#xD; &amp;lt;SkipVoterIDRequestFlag&amp;gt;True&amp;lt;" +
                  "/SkipVoterIDRequestFlag&amp;gt;&#xD; &amp;lt;SkipBureauRequestFlag&amp;gt;False&amp;lt;/SkipBureauRequestFlag&amp;gt;" +
                  "&#xD; &amp;lt;SkipTUXFCibilFlag&amp;gt;False&amp;lt;/SkipTUXFCibilFlag&amp;gt;&#xD; &amp;lt;RequestType&amp;gt;" +
                  "WS&amp;lt;/RequestType&amp;gt;&#xD; &amp;lt;MemberID&amp;gt;IBN005&amp;lt;/MemberID&amp;gt;&#xD; &amp;lt;" +
                  "MemberName&amp;gt;CHOLAMANDALAM INVESMENT &amp;amp;amp; FINANCE COMPANY&amp;lt;/MemberName&amp;gt;&#xD; &amp;lt;" +
                  "KAMContact&amp;gt;123456789&amp;lt;/KAMContact&amp;gt;&#xD; &amp;lt;KAMName&amp;gt;Sangeetha&amp;lt;/KAMName&amp;" +
                  "gt;&#xD; &amp;lt;KOB&amp;gt;PSU&amp;lt;/KOB&amp;gt;&#xD; &amp;lt;ShortName&amp;gt;CHOLA INVST FIN&amp;lt;" +
                  "/ShortName&amp;gt;&#xD; &amp;lt;NewToCredit&amp;gt;False&amp;lt;/NewToCredit&amp;gt;&#xD; &amp;lt;AddrPermanentInfo" +
                  "&amp;gt;Yes&amp;lt;/AddrPermanentInfo&amp;gt;&#xD; &amp;lt;AddrResidantInfo&amp;gt;No&amp;lt;/AddrResidantInfo&amp;" +
                  "gt;&#xD; &amp;lt;AddrOfficeInfo&amp;gt;No&amp;lt;/AddrOfficeInfo&amp;gt;&#xD; &amp;lt;TelMobileInfo&amp;gt;Yes&amp;lt;" +
                  "/TelMobileInfo&amp;gt;&#xD; &amp;lt;TelResidantInfo&amp;gt;No&amp;lt;/TelResidantInfo&amp;gt;&#xD; &amp;lt;" +
                  "TelOfficeInfo&amp;gt;No&amp;lt;/TelOfficeInfo&amp;gt;&#xD; &amp;lt;/ApplicationData&amp;gt;&#xD; &lt;/Field&gt;&lt;" +
                  "/ContextData&gt;&lt;/DCResponse&gt;</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Env" +
                  "elope>"
    };
    yield $api.put(`${simulationEndpoint}/cibil/add`, payload);
  } catch (err) {}
  return yield $set('mobile', {mobileNumber});
};

module.exports = [schema, set];
