const schema = {
    username: 'string:min=2,max=100,required'
};

function* set({
    $api,
    $get,
    $set,
    $setStatus,
    $configuration,
    $debug,
    value: {
        username
    }
}) {
    const { identity: identityEndpoint } = yield $configuration('endpoints');

    try {
        const otpResponse = yield $api.post(`${identityEndpoint}/otp`, { username });
        yield $set('login-otp-verification', otpResponse.body.referenceNumber);
    } catch (error) {
        throw {
            code: 422,
            message: error
        };
    }
};

module.exports = [schema, set];
