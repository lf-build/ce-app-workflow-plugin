const schema = {
  attribute: 'string:min=2,max=500,required',
  value: 'string:min=2,max=500,required',
};

function* set({
  $api,
  $set,
  $configuration,
  value: {
    attribute,
    value,
  },
  $debug
}) {
  const {
    'application-processor': applicationProcessorEndpoint,
    'application-filters': applicationFilterEndpoint,
  } = yield $configuration('endpoints');

  $debug(`${applicationProcessorEndpoint}/application/EligibilityCheckForReApply/${attribute}/${value}`);

  // checking if eligible for re apply.
  const eligibilityResponse = yield $api.get(`${applicationProcessorEndpoint}/application/EligibilityCheckForReApply/${attribute}/${value}`)

  // checking if the application terminated with milestone 'Not Interested' or 'Expired'
  const response = yield $api.get(`${applicationFilterEndpoint}/searchforattribute/ApplicantPhone/${value}`);
  let isFreshApplication = false;

  if (response && response.body) {
    const { item2: { statusCode, statusName } } = response.body;
    $debug(`searchforattribute response: statusCode: ${statusCode} statusName: ${statusName}`);

    // if Not Interested or Expired, then borrower will be able to reapply on his own    
    if (statusName === 'Not Interested' || statusName === 'Expired') {
      isFreshApplication = true;
    }

    // if Rejected then should not be considered as a fresh application
    if (statusName === 'Rejected') {
      isFreshApplication = false;
    }
  }

  return {
    eligible: JSON.parse(eligibilityResponse.body),
    freshApplication: isFreshApplication,
  };
};

module.exports = [schema, set];
