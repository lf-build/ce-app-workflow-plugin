const schema = {
    utmSource: 'string: min=1,max=250',
    utmMedium: 'string: min=1,max=250',
    utmCampaign: 'string: min=1,max=250',
    utmTerm: 'string: min=1,max=250',
    utmContent: 'string: min=1,max=250',
    sourceReferenceId: 'string: min=1,max=250',
    sourceType: 'string: min=1,max=250',
    systemChannel: 'string: min=1,max=250',
};

function* set({
    $api,
    $get,
    $set,
    $setStatus,
    $configuration,
    value: {
        utmSource,
        utmMedium,
        utmCampaign,
        utmTerm,
        utmContent,
        sourceReferenceId,
        sourceType,
        systemChannel
    },
    $debug,
    $facts: {
        opportunity
    },
    $stages: {
        authorization: {
            identity: {
                'create-account': createAccount
            }
        }
    },
}) {

    // creating account and login
    $debug('now executing the logic to create-account, login and submit application');

    // as create account page is skipped need to create account after OTP is entered
    const randomPassword = Math.floor(100000 + Math.random() * 900000);
    $debug(`random number is ${randomPassword}`);

    const {
        mobileNumber
    } = yield $get('mobile');

    const {
        'application-processor': applicationProcessorEndpoint
    } = yield $configuration('endpoints');

    const {
        amount,
        reason: {
            reason,
            reasonText
        },
        basicInformation: {
            title,
            maritalStatus,
            firstName,
            middleName,
            lastName,
            personalEmail
        }
    } = yield opportunity.$get();

    const {
        mobile: {
            verification: {
                status: mobileVerificationStatus,
                date: mobileVerificationDate
            }
        }
    } = yield $get();

    try {
        $debug('creating new user');
        const newUser = yield createAccount.$execute({
            username: mobileNumber,
            password: randomPassword.toString(),
            name: `${firstName} ${lastName}`,
            email: personalEmail
        });

        yield $set('user', Object.assign({}, newUser, {
            randomNumber: randomPassword
        }));
        $debug('user created successfully');

        const applicationSubmitPayload = {
            purposeOfLoan: reason,
            OtherPurposeDescription: reasonText,
            requestedAmount: amount,
            personalMobile: mobileNumber,
            isMobileVerified: mobileVerificationStatus,
            mobileVerificationTime: mobileVerificationDate,
            firstName: firstName,
            middleName: middleName,
            lastName: lastName,
            personalEmail,
            userId: newUser.id,
            gender: title === 'Mr' ?
                'Male' : 'Female',
            salutation: title,
            maritalStatus: maritalStatus,
            sourceReferenceId: sourceReferenceId,
            sourceType: sourceType,
            systemChannel: systemChannel,
            trackingCode: utmSource || 'CRM',
            trackingCodeMedium: utmMedium || 'CRM',
            trackingCodeCampaign: utmCampaign || 'CRM',
            trackingCodeTerm: utmTerm || 'CRM',
            trackingCodeContent: utmContent || 'CRM',
            paymentFrequency: 'Monthly',
            requestedTermType: 'Monthly',
            requestedTermValue: '12.5',
            employmentStatus: 'Salaried'
        };

        $debug('now creating applicaiton');
        const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/submit`, applicationSubmitPayload);
        const parsedApplicationObject = Object.assign(applicationSubmitPayload, {
            applicationNumber: appResponse.body.applicationNumber,
            applicantId: appResponse.body.applicantId
        });
        $debug('successfully created applicaiton');

        yield $set('application', parsedApplicationObject);

        return parsedApplicationObject;

    } catch (e) {
        throw e;
    }
};

module.exports = [schema, set];