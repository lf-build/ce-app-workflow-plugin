const { dbAction } = require('@sigma-infosolutions/orbit-base/storage');

const schema = {
    utmSource: 'string: min=1,max=250',
    utmMedium: 'string: min=1,max=250',
    utmCampaign: 'string: min=1,max=250',
    utmTerm: 'string: min=1,max=250',
    utmContent: 'string: min=1,max=250',
    sourceReferenceId: 'string: min=1,max=250',
    sourceType: 'string: min=1,max=250',
    systemChannel: 'string: min=1,max=250',
    mobileNumber: {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    },
};

function* set({
    $api,
    $get,
    $set,
    $setStatus,
    $configuration,
    value: {
        utmSource,
        utmMedium,
        utmCampaign,
        utmTerm,
        utmContent,
        sourceReferenceId,
        sourceType,
        systemChannel,
        mobileNumber
    },
    $debug,
    $facts: {
        opportunity
    },
}) {
    const {
        'application-processor': applicationProcessorEndpoint
    } = yield $configuration('endpoints');

    // creating account and login
    $debug('now executing the logic reapply  submit application');

    // as create account page is skipped need to create account after OTP is entered
    // const randomPassword = Math.floor(100000 + Math.random() * 900000);
    // $debug(`random number is ${randomPassword}`);
    try {
        //fetach existing application user

        const readAllStates = (db) => db
            .collection('store')
            .find({ 'on-boarding.sign-up.mobile.mobileNumber': mobileNumber })
            .sort({ _id: -1 })
            .toArray();
        let states = yield dbAction(readAllStates);

        const {
            'on-boarding': {
                'sign-up': {
                    user
                }
            }
        } = states[((states.length) - 1)];

        yield $set('user', user);
        $debug('existing user added successfully', user);
        const {
            amount,
            reason: {
                reason,
                reasonText
            },
            basicInformation: {
                title,
                maritalStatus,
                firstName,
                middleName,
                lastName,
                personalEmail
            }
        } = yield opportunity.$get();
        const {
            mobile: {
                verification: {
                    status: mobileVerificationStatus,
                    date: mobileVerificationDate
                }
            }
        } = yield $get();
        const applicationSubmitPayload = {
            purposeOfLoan: reason,
            OtherPurposeDescription: reasonText,
            requestedAmount: amount,
            personalMobile: mobileNumber,
            isMobileVerified: mobileVerificationStatus,
            mobileVerificationTime: mobileVerificationDate,
            firstName: firstName,
            middleName: middleName,
            lastName: lastName,
            personalEmail,
            userId: user.id,      //need to set old user
            gender: title === 'Mr' ?
                'Male' : 'Female',
            salutation: title,
            maritalStatus: maritalStatus,
            sourceReferenceId: sourceReferenceId,
            sourceType: sourceType,
            systemChannel: systemChannel,
            trackingCode: utmSource || 'CRM',      //need to confirm for LSQ reapply
            trackingCodeMedium: utmMedium || 'CRM',  //need to confirm for LSQ reapply           
            trackingCodeCampaign: utmCampaign || 'CRM',
            trackingCodeTerm: utmTerm || 'CRM',
            trackingCodeContent: utmContent || 'CRM',
            paymentFrequency: 'Monthly',
            requestedTermType: 'Monthly',
            requestedTermValue: '12.5',
            employmentStatus: 'Salaried'
        };

        $debug('now creating applicaiton');
        const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/submit`, applicationSubmitPayload);
        const parsedApplicationObject = Object.assign(applicationSubmitPayload, {
            applicationNumber: appResponse.body.applicationNumber,
            applicantId: appResponse.body.applicantId
        });
        $debug('successfully created applicaiton');

        yield $set('application', parsedApplicationObject);

        return parsedApplicationObject;

    } catch (e) {
        throw e;
    }
};

module.exports = [schema, set];