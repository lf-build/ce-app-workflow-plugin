const schema = {};

function* set({
  $api,
  $set,
  $get,
  $facts: {
    // application,
    'sign-up': signUp
  },
  $debug,
  $configuration
}) {
  const {
    'application-processor': applicationProcessorEndPoint,
  } = yield $configuration('endpoints');

  const {
    initial: {
      offers: [{
        offerId
      }]
    }
  } = yield $get();
  const {
    application: {
      applicationNumber
    }
  } = yield signUp.$get();

  yield $api.put(`${applicationProcessorEndPoint}/offer/${applicationNumber}/acceptinitialoffer/${offerId}`);
  $debug(`${offerId} select as initial offer for ${applicationNumber}`);
  return offerId;
};

module.exports = [schema, set];