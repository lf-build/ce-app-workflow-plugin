const schema = {};

function * set({
  $api,
  $get,
  $facts: {
    // application
    'sign-up': signUp
  },
  $debug,
  $configuration
}) {

    const {'data-attribute': dataAttributeEndPoint} = yield $configuration('endpoints');

    const { application: {
        applicationNumber
    }} = yield signUp.$get();

    let fundedData = undefined;

    try {
        fundedData = yield $api.get(`${dataAttributeEndPoint}/application/${applicationNumber}/loan-fundeddata`);
        return fundedData.body;
    } catch(e) {
        $debug('in catch, some error occurred while calling /loan-fundeddata ', e);
        throw {
            code: 424,
            message: e,
        }
    }  
};

module.exports = [schema, set];
