const schema = {};

function* set({
    $api,
    $set,
    $get,
    $facts: {
        'sign-up': signUp
    },
    $debug,
    $configuration
}) {

    const {
        'application-processor': applicationProcessorEndPoint
    } = yield $configuration('endpoints');

    const {
        application: {
            applicationNumber
        }
    } = yield signUp.$get();

    try {
        $debug(`getting initial offer details for ${applicationNumber}`)
        const offerResponse = yield $api.get(`${applicationProcessorEndPoint}/offer/${applicationNumber}/initialoffer`);
        const [offer] = offerResponse.body.offers;
        return offer;
    } catch (error) {
        throw {
            code: 400,
            error
        };
    }
};

module.exports = [schema, set];