const schema = {};

function* set({
  $api,
  $set,
  $get,
  $facts: {
    // application
    'sign-up': signUp
  },
  $debug,
  $configuration
}) {

  const {
    'application-processor': applicationProcessorEndPoint
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = yield signUp.$get();

  yield $set('tmp', true);
  
  const {
    initial: generatedOffer
  } = yield $get();
  
  // if (generatedOffer && generatedOffer.offers) {   return
  // generatedOffer.offers[0]; } else if (generatedOffer && generatedOffer.status
  // === 'Rejected') {   return generatedOffer; } else {
  
  try {
    const offerResponse = yield $api.post(`${applicationProcessorEndPoint}/offer/${applicationNumber}/initialoffer`);
    if (!offerResponse.body.offers) {
      $debug('re-generated offer (retry from catch)', offerResponse.body);
      yield $set('initial', {
        status: offerResponse.body.status || 'Rejected'
      });

      return {
        status: offerResponse.body.status || 'Rejected'
      };
    } else {
      $debug('returning from generateoffer true');
      $set('initial', offerResponse.body)
      const [offer] = offerResponse.body.offers;
      // yield
      // $api.put(`${applicationProcessorEndPoint}/offer/${applicationNumber}/acceptin
      // i tialoffer/${offer.offerId}`);

      return offer;
    }
  } catch (e) {
    try {
      const offerResponse = yield $api.get(`${applicationProcessorEndPoint}/offer/${applicationNumber}/initialoffer`);
      $debug('re-generated offer', offerResponse.body);
      if (offerResponse.body.offers) {
        $set('initial', offerResponse.body)
        const [offer] = offerResponse.body.offers;
        return offer;
      } else {
        throw {
          status: 'Rejected'
        }
      }
    } catch (error) {
      throw {
        code: 424,
        error
      };
    }
  }
};
// };

module.exports = [schema, set];