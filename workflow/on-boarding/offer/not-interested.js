const schema = {
  reason: 'string:min=2,max=200,required'
};

function *set({
  $api,
  $set,
  $get,
  $facts: {
    application,
  },
  $configuration,
  value: {
    reason,
  },
  $debug})  {
  const offers = yield $get();
  const {
    'status-management': statusManagementEndpoint,
  } = yield $configuration('endpoints');

  const { application: { applicationNumber } } = yield application.$get();
  const notInterestedResponse = yield $api.post(`${statusManagementEndpoint}/application/${applicationNumber}/200.14/${reason}`);  
  return notInterestedResponse.body;
};

module.exports = [schema, set];
