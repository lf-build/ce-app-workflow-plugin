const schema = {
    selectedOfferId: 'string:min=2,max=200,required'
};

function* set({
    $api,
    $set,
    $facts: {
        //   application,
        'sign-up': signUp
    },
    value: {
        selectedOfferId
    },
    $debug,
    $configuration
}) {
    const {
        'application-processor': applicationProcessorEndpoint,
    } = yield $configuration('endpoints');

    const {
        application: {
            applicationNumber
        }
    } = yield signUp.$get();

    const offerResponse = yield $api.put(`${applicationProcessorEndpoint}/offer/${applicationNumber}/acceptfinaloffer/${selectedOfferId}`);
    
    yield $set('selectedOfferId', {
        selectedOfferId
    });
    
    return {
        selectedOfferId
    };
};

module.exports = [schema, set];