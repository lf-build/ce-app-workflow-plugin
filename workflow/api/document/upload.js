var FormData = require('form-data');

const schema = {
    "applicationNumber": "string:min=2,max=100,required",
    "uploadFileType": {
        '@type': 'string',
        valid: ['BankVerification', 'KYC-PAN', 'KYC-Address', 'SignedDocumentVerification', 'other'],
        required: true
    },
    "fileList": {
        "@items": {
            "file": "string:min=2,max=100,required",
            "dataUrl": "string:min=2,max=100000000,required"
        },
        "max": 1,
        "min": 1
    }
};

function* set({
    value: {
        applicationNumber,
        fileList,
        uploadFileType,
    },
    $api,
    $debug,
    $configuration,
}) {

    const {
        'verification-engine': verificationEngineEndpoint,
        'application-document': applicationDocumentEndpoint
    } = yield $configuration('endpoints');

    try {

        let factMethodName = '';
        switch (uploadFileType) {
            case 'BankVerification':
                factMethodName = 'BankVerification/BankManual';
                break;
            case 'KYC-PAN':
                factMethodName = 'KYC-PAN/KYCPan';
                break;
            case 'KYC-Address':
                factMethodName = 'KYC-Address/KYCAddress';
                break;
            case 'SignedDocumentVerification':
                factMethodName = 'SignedDocumentVerification/SignedDocuments';
                break;
            default:
                break;
        }

        let finalResponse = [];

        for (index in fileList) {
            let fileObject = fileList[index];

            const formData = new FormData();
            formData.append('file', new Buffer(fileObject.dataUrl.substring(fileObject.dataUrl.indexOf("base64,")).replace('base64,', ''), 'base64'), fileObject.file);

            if (factMethodName === '') {
                const response = yield $api.post(`${applicationDocumentEndpoint}/${applicationNumber}/other`, formData, null);
                finalResponse.push({
                    category: response.body.category,
                    fileId: response.body.id
                });
            } else {
                const response = yield $api.post(`${verificationEngineEndpoint}/application/${applicationNumber}/${factMethodName}/upload-document`, formData, null);
                finalResponse.push({
                    category: response.body.category,
                    fileId: response.body.id
                });
            }
        }

        return finalResponse;

    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, set];