const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};

/*
    TODO:
    1. need to check what all documents are required.
 */

function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $configuration,
    $debug
}) {

    const {
        'verification-engine': verificationEngineEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${verificationEngineEndpoint}/application/${applicationNumber}/documents`);
        return response && response.body;
    } catch (e) {
        $debug(e);
        throw {
            code: 400,
            message: 'Something went wrong'
        }
    }    
};

module.exports = [schema, set];