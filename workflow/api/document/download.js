const schema = {
    applicationNumber: 'string:min=2,max=100,required',
    documentId: 'string:min=2,max=100,required',
};

/*
    TODO:
    1. adding the pipe logic here to get pdf.
 */

function* set({
    $set,
    value: {
        applicationNumber,
        documentId
    },
    $api,
    $configuration,
    $debug
}) {

    const {
        'application-document': applicationDocumentEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${applicationDocumentEndpoint}/application/${applicationNumber}/${documentId}/downloaddocument`);
        return response && response.body && response.body.downloadString;
    } catch (e) {
        $debug(e);
        throw {
            code: 400,
            message: 'Something went wrong'
        }
    }    
};

module.exports = [schema, set];