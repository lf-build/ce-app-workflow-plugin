const schema = {
    applicationNumber: 'string:min=2,max=100,required',
    category: 'string:min=2,max=100,required',
};

function* set({
    $set,
    value: {
        applicationNumber,
        category
    },
    $api,
    $configuration,
    $debug
}) {

    const {
        'application-document': applicationDocumentEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${applicationDocumentEndpoint}/${applicationNumber}/${category}`);
        return response && response.body;
    } catch (e) {
        $debug(e);
        throw {
            code: 400,
            message: 'Something went wrong'
        }
    }
};

module.exports = [schema, set];