const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};


function* set({
    value: {
        applicationNumber
    },
    $api,
    $debug,
    $configuration,
}) {

    const {
        'application-document': applicationDocumentEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${applicationDocumentEndpoint}/${applicationNumber}`);
        return response && response.body;

    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, set];