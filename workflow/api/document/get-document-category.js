const schema = {};


function* set({
    value,
    $api,
    $debug,
    $configuration,
}) {

    const {
        configuration: configurationEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${configurationEndpoint}/application-documents`);
        return response && response.body;
    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, set];