const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};


function* set({
    value: {
        applicationNumber
    },
    $api,
    $debug,
    $configuration,
}) {

    const {
        'activity-list': activityListEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${activityListEndpoint}/application/${applicationNumber}/all/clean`);

        if (response && response.body && response.body.length === 0) {
            throw {
                code: 400,
                message: `No activites found for application number ${applicationNumber}`
            }
        }

        if (response && response.body) {
            const notes = response.body.filter((activity, i) => activity.title === 'Notes');

            if (notes.length === 0) {
                throw {
                    code: 400,
                    message: `No notes found for application number ${applicationNumber}`
                }
            }

            return notes;
        }

        return response && response.body;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];