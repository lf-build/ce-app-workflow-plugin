const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};


function* set({
    value: {
        applicationNumber
    },
    $api,
    $debug,
    $configuration,
}) {

    const {
        'status-management': statusManagementEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${statusManagementEndpoint}/application/${applicationNumber}`);
        return response && response.body && response.body.activities;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];