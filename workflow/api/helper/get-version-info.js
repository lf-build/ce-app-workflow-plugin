const packageInfo = require('../../../package.json');

const schema = {};

function* set({ }) {
    const { version } = packageInfo;

    return {
        version
    };
};

module.exports = [schema, set];