const schema = {
    applicationNumber: 'string: min=7, max=7, required',
    fundedDate: 'string: min=0, max=100, required',
    fundedAmount: { '@type': 'number', 'required': true },
    firstEmiDate: 'string: min=0, max=100 ,required',
    loanAccountNumber: { '@type': 'string', 'required': true },
    emiAmount: { '@type': 'number', 'required': true },
    paymentStatus: {
        '@type': 'string',
        'required': true,
        valid: [
            'PaymentDone', 'PaymentPending'
        ]
    },
    UTRNumber: 'string: min=0, max=50',
}


function* set({
    value,
    $api,
    $debug,
    $set,
    $configuration,
}) {
    try {

        const {
            'application-processor': applicationProcessorEndPoint,
        } = yield $configuration('endpoints');

        const formatDate = (dt) => {
            const [dd, mm, yyyy] = dt.split('/');
            return `${yyyy}-${mm}-${dd}`;
        }

        const {
            fundedDate,
            firstEmiDate,
            applicationNumber,
            ...rest
        } = value;

        const payload = {
            fundedDate: fundedDate && formatDate(fundedDate),
            firstEmiDate: firstEmiDate && formatDate(firstEmiDate),
            ...rest
        }

        const updatedData = yield $api.post(`${applicationProcessorEndPoint}/application/${applicationNumber}/loan-fundeddata`, payload);
        return updatedData && updatedData.body;

    }
    catch (e) {
        $debug(e);
        const message = e.message || e.body.message || e.body.Message || 'Something went wrong';
        const code = e.code || e.body.code || e.body.Code || 400;

        throw {
            message: message,
            code,
        }
    }
};
module.exports = [schema, set];