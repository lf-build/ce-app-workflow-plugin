const {
    dbAction
} = require('@sigma-infosolutions/orbit-base/storage');

const schema = {
    'applicationNumber': 'string: min=7, max=7, required',
    'requestedAmount': 'number:min=100000,max=1500000',
    'purposeOfLoan': {
        '@type': 'string',
        valid: ['travel', 'vehiclepurchase', 'medical', 'loanrefinancing', 'wedding', 'homeimprovement', 'business', 'education', 'assetacquisition', 'agriculture', 'other'],
        // required: true
    },
    'otherPurposeDescription': 'string: min=2, max=100',
    'salutation': {
        '@type': 'string',
        valid: [
            'Mr', 'Mrs', 'Ms'
        ],
        // required: true
    },
    'gender': {
        '@type': 'string',
        valid: [
            'Male', 'Female'
        ]
    },
    'maritalStatus': {
        '@type': 'string',
        valid: [
            'single', 'married'
        ],
        // required: true
    },
    'firstName': 'string: min=1, max=100',
    'middleName': 'string: min=1, max=100',
    'lastName': 'string: min=1, max=100',
    'personalEmail': 'email: min=2, max=100',
    'personalMobile': {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        // required: true
    },
    'employerName': 'string: min=2, max=100,',
    'cinNumber': 'string: min=2, max=100',
    'workEmail': 'email: max=100,',
    'income': 'number: min=0, max=1000000,',
    'residenceType': {
        '@type': 'string',
        valid: ['OwnedSelf', 'Owned-Family', 'Rented-Family', 'Rented-Alone', 'Rented-Friends', 'PG-Hostel', 'Company-Acco'],
        // required: true
    },
    'monthlyRent': 'number: min=0, max=1000000',
    'homeLoanEmi': 'number: min=0, max=1000000',
    'dateOfBirth': 'string: min=0, max=100,',
    'aadhaarNumber': 'string: min=12, max=12,',
    'permanentAccountNumber': 'string: min=10, max=10,',
    'currentAddressLine1': 'string: min=3, max=40,',
    'currentAddressLine2': 'string: min=0, max=40',
    'currentCity': 'string: min=3, max=40,',
    'currentLocation': 'string: min=3, max=40,',
    'currentPinCode': 'string: min=6, max=6,',
    'currentState': 'string: min=3, max=40,',
    'permanentAddressLine1': 'string: min=3, max=40,',
    'permanentAddressLine2': 'string: min=0, max=40',
    'permanentLocation': 'string: min=3, max=40,',
    'permanentCity': 'string: min=3, max=40,',
    'permanentPinCode': 'string: min=6, max=6,',
    'permanentState': 'string: min=3, max=40,',

    'heighestEducation': {
        '@type': 'string',
        valid: ['highschool', 'bachelordegree', 'masterdegree', 'doctratedegree'],
        //required: true
    },
    'educationalInstitution': 'string:min=2,max=100',
    'designation': 'string:min=2,max=100',
    'workExperience': 'number:min=0,max=10',
    'workAddressLine1': 'string: min=3, max=40,',
    'workAddressLine2': 'string: min=0, max=40',
    'workLocation': 'string: min=3, max=40,',
    'workCity': 'string: min=3, max=40,',
    'workPinCode': 'string: min=6, max=6,',
    'workState': 'string: min=3, max=40,',
    'trackingCode': 'string: min=1,max=250',
    'trackingCodeMedium': 'string: min=1,max=250',
    'trackingCodeCampaign': 'string: min=1,max=250',
    'trackingCodeTerm': 'string: min=1,max=250',
    'trackingCodeContent': 'string: min=1,max=250',
    'sodexoCode': 'string: min=1,max=250',
    'currentAddressYear': 'string:min=0 max=2',
    'employementYear': 'string:min=0 max=2',
    'sourceReferenceId': 'string: min=0, max=100,',
    'sourceType': 'string: min=0, max=100,',
    'systemChannel': 'string: min=0, max=100,',
    'fromNavigatore': 'boolean',
    'levelOfEducation': 'string: min=0, max=100,',
    'lengthOfEmploymentInMonths': 'number:min=0,max=10',

};

function* set({
    value,
    $api,
    $debug,
    $set,
    $configuration
}) {
    try {

        const {
            'application-filters': applicationFiltersEndpoint,
            'application-processor': applicationProcessorEndpoint
        } = yield $configuration('endpoints');

        const formatDate = (dt) => {
            const [dd, mm, yyyy] = dt.split('/');
            return `${yyyy}-${mm}-${dd}`;
        }

        //setting up the special cases
        value.dateOfBirth = value.dateOfBirth && formatDate(value.dateOfBirth);
        value.gender = value.salutation && (value.salutation === 'Mr' ? 'Male' : 'Female');
        value.levelOfEducation = value.heighestEducation && value.heighestEducation;
        value.lengthOfEmploymentInMonths = value.workExperience && value.workExperience * 12;

        // personal email.
        if (value.personalEmail !== null && value.personalEmail !== '') {
            const dupEmail = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Email/${value.personalEmail}`);
            if (dupEmail && dupEmail.body && dupEmail.body.length > 0) {
                const appNos = dupEmail.body.reduce((pre, curr) => {
                    pre.push(curr.applicationNumber);
                    return pre;
                }, []);

                if (appNos && appNos.indexOf(value.applicationNumber) === -1) {
                    throw {
                        code: 400,
                        message: 'Application with same email id already exists'
                    }
                }
            }
        }

        // pan number
        if (value.permanentAccountNumber !== null && value.permanentAccountNumber !== '') {
            const dupPan = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Pan/${value.permanentAccountNumber}`);
            if (dupPan && dupPan.body && dupPan.body.length > 0) {
                const appNos = dupPan.body.reduce((pre, curr) => {
                    pre.push(curr.applicationNumber);
                    return pre;
                }, []);

                if (appNos && appNos.indexOf(value.applicationNumber) === -1) {
                    throw {
                        code: 400,
                        message: 'Application with same permanent account number already exists',
                        appNos,
                    }
                }
            }
        }
        if (value.personalMobile !== null && value.personalMobile !== '') {
            const dupPhoneApp = (yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Mobile/${value.personalMobile}`)).body;
            if (dupPhoneApp && dupPhoneApp.body && dupPhoneApp.body.length > 0) {
                const appNos = dupPhoneApp.body.reduce((pre, curr) => {
                    pre.push(curr.applicationNumber);
                    return pre;
                }, []);

                if (appNos && appNos.indexOf(value.applicationNumber) === -1) {
                    throw {
                        code: 400,
                        message: 'Application with same phone number already exists',
                        appNos,
                    }
                }
            }
        }
        //Update application details.     
        const response = yield $api.put(`${applicationProcessorEndpoint}/application/${value.applicationNumber}/update-lead-application`, value);
        return response.body;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];