const schema = {
    applicationNumber: 'string:min=2,max=10,required'
};


function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $debug,
    $configuration,

}) {
    try {

        const {
            'application-processor': applicationProcessorEndPoint
        } = yield $configuration('endpoints');

        const submitApplication = yield $api.post(`${applicationProcessorEndPoint}/application/${applicationNumber}/changeStatus-submit`);

        if (submitApplication && submitApplication.body) {
            const initialOfferResponse = yield $api.post(`${applicationProcessorEndPoint}/offer/${applicationNumber}/initialoffer`);

            const {
                    entityType,
                    entityId,
                    status,
                    reasons
                } = initialOfferResponse.body;

            if (initialOfferResponse && initialOfferResponse.body) {
                if(initialOfferResponse.body.offers){
                    const offerId = initialOfferResponse.body.offers[0].offerId;
                    yield $api.put(`${applicationProcessorEndPoint}/offer/${applicationNumber}/acceptinitialoffer/${offerId}`);
                }
                return {
                            entityId,
                            entityType,
                            status,
                            reasons,
                }
            }
        }
    }
    catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};
module.exports = [schema, set];