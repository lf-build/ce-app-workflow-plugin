const schema = {
    personalMobile: {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    }
}

function* set({
    value,
    $api,
    $debug,
    $set,
    $configuration,
}) {
    try {

        const {
            'application-processor': applicationProcessorEndPoint,
            'application-filters' : applicationFilterEndPoint
        } = yield $configuration('endpoints');

        let applicationNumbersList=[];
    
        const filterApplication = yield $api.get(`${applicationFilterEndPoint}/duplicateexistsdata/Mobile/${value.personalMobile}`);
        const reApplyResult = yield $api.get(`${applicationProcessorEndPoint}/application/EligibilityCheckForReApply/Mobile/${value.personalMobile}`);
        filterApplication.body.forEach(element => {
            applicationNumbersList.push({
                    ApplicationNumber : element.applicationNumber
            });
        });
        
        return {
            ApplicationList: applicationNumbersList,
            EligibleforReapply : reApplyResult.body
        }

    }
    catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};
module.exports = [schema, set];