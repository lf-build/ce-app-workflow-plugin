const schema = {
    applicationNumber: 'string: min=7, max=7, required',
}


function* set({
    value  : { applicationNumber},
    $api,
    $debug,
    $set,
    $configuration,
}) {
    try {

        const {
            'application-processor': applicationProcessorEndPoint,
        } = yield $configuration('endpoints');

       
       const statusChanged = yield $api.post(`${applicationProcessorEndPoint}/status-management/${applicationNumber}/200.12`);
       return;

    }
    catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};
module.exports = [schema, set];