const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};

function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $configuration,
    $debug,
}) {
    try {
        const {
            application: applicationEndpoint
        } = yield $configuration('endpoints');
    
        const appDetails = yield $api.get(`${applicationEndpoint}/${applicationNumber}`);
    
        return appDetails && appDetails.body;

    } catch(e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];