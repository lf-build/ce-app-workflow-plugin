const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};

function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $configuration,
    $debug,
}) {
    try {
        const {
            'application-processor': applicationProcessorEndPoint
        } = yield $configuration('endpoints');

        let offerResponse = undefined;

        offerResponse = yield $api.get(`${applicationProcessorEndPoint}/offer/${applicationNumber}/finaloffer/presented`);

        if (!offerResponse.body.finalOffers && offerResponse.body.status === 'Failed') {
            return ({
                status: 'failed'
            });
        } else if (!offerResponse.body.finalOffers) {
            return ({
                status: 'rejected'
            });
        } else {            
            return offerResponse.body;
        }        

        return offerResponse;
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];