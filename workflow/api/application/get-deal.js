const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};

function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $configuration,
    $debug,
}) {
    const {
        'application-processor': applicationProcessorEndPoint
    } = yield $configuration('endpoints');

    const initialOffers = yield yield $api.get(`${applicationProcessorEndPoint}/offer/${applicationNumber}/initialoffer`);

    return initialOffers;
};

module.exports = [schema, set];