var schema = {};

/**
 * 
 * Get application lookup
 * 
 * @category Application
 * @section API
 * @name get lookup
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value,
    $debug,
    $configuration
}) {
    try {
        const {
            configuration,
            lookup
        } = yield $configuration('endpoints');

        let {
            body: {
                LookupNames
            }
        } = yield $api.get(`${configuration}/backoffice-portal`);

        if (!LookupNames) {
            LookupNames = [
                "cibil-account-type",
                "documentReasons",
                "application-purposeOfLoan",
                "application-levelOfEducation",
                "application-bankSource",
                "applicationDocument-category"
            ];
        }
        
        const joinedLookupNames = LookupNames.join('/');
        const response = yield $api.get(`${lookup}/entities/${joinedLookupNames}`);

        return response && response.body;

    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
}

module.exports = [schema, set];