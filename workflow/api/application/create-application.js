const schema = {
    'requestedAmount': 'number:min=50000,max=1500000,required',
    'purposeOfLoan': {
        '@type': 'string',
        valid: ['travel', 'vehiclepurchase', 'medical', 'loanrefinancing', 'wedding', 'homeimprovement', 'business', 'education', 'assetacquisition', 'agriculture', 'other'],
        required: true
    },
    'otherPurposeDescription': 'string: min=2, max=100',
    'salutation': {
        '@type': 'string',
        valid: [
            'Mr', 'Mrs', 'Ms'
        ],
        required: true
    },
    'maritalStatus': {
        '@type': 'string',
        valid: [
            'single', 'married'
        ],
        required: true
    },
    'firstName': 'string: min=1, max=100, required',
    'middleName': 'string: min=1, max=100',
    'lastName': 'string: min=1, max=100, required',
    'personalEmail': 'email: min=2, max=100, required',
    'personalMobile': {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    },
    'isMobileVerified': 'boolean: required',
    'mobileVerificationTime': 'string: min=2, max=100',
    'mobileVerificationNotes': 'string: min=2, max=100',
    'employerName': 'string: min=2, max=100, required',
    'cinNumber': 'string: min=2, max=100',
    'workEmail': 'email: max=100, required',
    'income': 'number: min=0, max=1000000, required',
    'residenceType': {
        '@type': 'string',
        valid: ['OwnedSelf', 'Owned-Family', 'Rented-Family', 'Rented-Alone', 'Rented-Friends', 'PG-Hostel', 'Company-Acco'],
        required: true
    },
    'monthlyRent': 'number: min=0, max=1000000',
    'homeLoanEmi': 'number: min=0, max=1000000',

    'dateOfBirth': 'string: min=0, max=100, required',
    'aadhaarNumber': 'string: min=12, max=12, required',
    'permanentAccountNumber': 'string: min=10, max=10, required',

    'currentAddressLine1': 'string: min=3, max=40, required',
    'currentAddressLine2': 'string: min=0, max=40',
    'currentCity': 'string: min=3, max=40, required',
    'currentLocation': 'string: min=3, max=40, required',
    'currentPinCode': 'string: min=6, max=6, required',
    'currentState': 'string: min=3, max=40, required',

    'permanentAddressLine1': 'string: min=3, max=40, required',
    'permanentAddressLine2': 'string: min=0, max=40',
    'permanentLocation': 'string: min=3, max=40, required',
    'permanentCity': 'string: min=3, max=40, required',
    'permanentPinCode': 'string: min=6, max=6, required',
    'permanentState': 'string: min=3, max=40, required',

    'heighestEducation': {
        '@type': 'string',
        valid: ['highschool', 'bachelordegree', 'masterdegree', 'doctratedegree'],
        required: true
    },
    'educationalInstitution': 'string:min=2,max=100,required',

    'designation': 'string:min=2,max=100,required',
    'workExperience': 'number:min=0,max=10,required',

    'workAddressLine1': 'string: min=3, max=40, required',
    'workAddressLine2': 'string: min=0, max=40',
    'workLocation': 'string: min=3, max=40, required',
    'workCity': 'string: min=3, max=40, required',
    'workPinCode': 'string: min=6, max=6, required',
    'workState': 'string: min=3, max=40, required',

    'trackingCode': 'string: min=1,max=250',
    'trackingCodeMedium': 'string: min=1,max=250',
    'trackingCodeCampaign': 'string: min=1,max=250',
    'trackingCodeTerm': 'string: min=1,max=250',
    'trackingCodeContent': 'string: min=1,max=250',

    'sodexoCode': 'string: min=1,max=250',

    'sourceReferenceId': 'string: min=0, max=100, required',
    'sourceType': 'string: min=0, max=100, required',
    'systemChannel': 'string: min=0, max=100, required',
    'currentAddressYear': 'string:min=0 max=2',
    'employementYear': 'string:min=0 max=2',
};

function* set({
    value,
    $api,
    $debug,
    $set,
    $configuration,
    $stages: {
        authorization: {
            identity: {
                check
            }
        },
        'on-boarding': {
            opportunity: {
                'set-amount': setAmount,
                'set-reason': setReason,
                'set-basic-information': setBasicInfo,
                'set-basic-information-reapply': setBasicInfoIsReapply
            },
            'sign-up': {
                'set-mobile-for-crm': setMobile,
                'verify-otp-for-crm': verifyOtp,
                'verify-otp-for-reapply': verifyOtpIsReapply
            },
            qualify: {
                'set-work-details': setWorkDetails,
                'set-residence-expenses': setResidenceExpenses,
                'set-details': setDetails,
                'set-details-reapply': setDetailsIsReapply,
            },
            verify: {
                'set-education-for-crm': setEducation,
                'set-employment-for-crm': setEmployment
            },
            application: {
                'update-application-for-crm': updateApplication
            },
            offer: {
                'generate-initial': generateInitialOffer,
                'select-initial-offer': selectInitialOffer
            }
        }
    }
}) {

    try {

        const {
            'application-filters': applicationFiltersEndpoint,
            'application-processor': applicationProcessorEndPoint,
            'status-management': statusManagementEndpoint,
        } = yield $configuration('endpoints');

        // checking if home loan EMI and monthly rent both are passed
        if (value.monthlyRent !== 0 && value.homeLoanEmi !== 0) {
            throw {
                code: 400,
                message: 'Please enter value for either homeLoanEmi or monthlyRent'
            }
        }
        //IsReapply
        let isReapply;
        // checking for duplicacy
        // mobile number

        const signupStatus = yield check.$execute({
            username: value.personalMobile
        });
        $debug('signupStatus:', signupStatus);
        if (signupStatus.exists) {
            const dupPhoneApp = (yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Mobile/${value.personalMobile}`)).body;
            const isReapplyResponse = yield $api.get(`${applicationProcessorEndPoint}/application/EligibilityCheckForReApply/Mobile/${value.personalMobile}`);
            isReapply = isReapplyResponse.body;

            const statuManagemetnResponse = yield $api.get(`${statusManagementEndpoint}/application/${dupPhoneApp[0].applicationNumber}`);
            $debug('statuManagemetnResponse.body.statusCode : ', (statuManagemetnResponse.body.code == "200.14"));

            if (statuManagemetnResponse.body.code == "200.14" ||
                statuManagemetnResponse.body.code == "200.15") {
                yield $api.post(`${applicationProcessorEndPoint}/application/${dupPhoneApp[0].applicationNumber}/${dupPhoneApp[0].applicantId}/EnableReApplyInCoolOff`);
                isReapply = true;
                $debug('Enabled ReApply  ', dupPhoneApp[0].applicationNumber);
            }
        }
        if (!isReapply) {
            if (signupStatus.exists) {
                throw {
                    code: 400,
                    message: 'Application with same mobile number already exists'
                };
            }
            const dupEmail = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Email/${value.personalEmail}`);
            console.log(dupEmail, dupEmail && dupEmail.length > 0);
            if (dupEmail && dupEmail.body && dupEmail.body.length > 0) {
                throw {
                    code: 400,
                    message: 'Application with same email id already exists'
                }
            }
            // pan number
            const dupPan = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Pan/${value.permanentAccountNumber}`);
            if (dupPan && dupPan.body && dupPan.body.length > 0) {
                throw {
                    code: 400,
                    message: 'Application with same permanent account number already exists'
                }
            }
        }
        $debug('setting amount');
        yield setAmount.$execute({
            amount: value.requestedAmount
        });
        $debug('Amount :', value.requestedAmount);

        $debug('done setting amount');

        $debug('setting reason');
        yield setReason.$execute({
            reason: value.purposeOfLoan,
            reasonText: value.otherPurposeDescription
        });
        $debug('done setting reason');

        /// create new frok from basic-info for duplicate check remove in case of reapply
        $debug('setting basic information');
        const paylodBasicDetail = {
            title: value.salutation,
            maritalStatus: value.maritalStatus,
            firstName: value.firstName,
            middleName: value.middleName,
            lastName: value.lastName,
            personalEmail: value.personalEmail,
        }
        if (isReapply) {
            yield setBasicInfoIsReapply.$execute(paylodBasicDetail);

        }
        else {
            yield setBasicInfo.$execute(paylodBasicDetail);
        }
        $debug('done setting basic information');
        // do not need to set// will check once and decide

        $debug('setting mobile');
        yield setMobile.$execute({
            mobileNumber: value.personalMobile
        });
        $debug('done setting mobile');

        //create copy and remove user creation and fetch userid from work flow.

        const verifyOtpData = {
            utmSource: value.trackingCode,
            utmMedium: value.trackingCodeMedium,
            utmCampaign: value.trackingCodeCampaign,
            utmTerm: value.trackingCodeTerm,
            utmContent: value.trackingCodeContent,
            sourceReferenceId: value.sourceReferenceId,
            sourceType: value.sourceType,
            systemChannel: value.systemChannel
        }
        if (isReapply) {
            const verifyOtpDataReapply = {
                utmSource: value.trackingCode,
                utmMedium: value.trackingCodeMedium,
                utmCampaign: value.trackingCodeCampaign,
                utmTerm: value.trackingCodeTerm,
                utmContent: value.trackingCodeContent,
                sourceReferenceId: value.sourceReferenceId,
                sourceType: value.sourceType,
                systemChannel: value.systemChannel,
                mobileNumber: value.personalMobile
            }
            ///need to fetch user details and set
            $debug('creating user and then application');
            yield verifyOtpIsReapply.$execute(verifyOtpDataReapply);
            $debug('done existing user and application reapply');
        } else {
            $debug('creating user and then application');
            yield verifyOtp.$execute(verifyOtpData);
            $debug('done creating user and application');
        }
        $debug('setting work details');
        yield setWorkDetails.$execute({
            employer: {
                cin: value.employerName,
                name: value.employerName,
            },
            monthlyTakeHomeSalary: value.income,
            officialEmail: value.workEmail
        });
        $debug('done setting work details');

        $debug('setting residence expenses');
        yield setResidenceExpenses.$execute({
            residenceType: value.residenceType,
            hasHomeLoan: value.homeLoanEmi !== 0 ? true : false,
            homeLoanEmi: value.homeLoanEmi,
            monthlyRent: value.monthlyRent
        });
        $debug('done setting residence expenses');
        // copy and remove pan number....
        const setDetailsData = {
            dateOfBirth: value.dateOfBirth,
            panNumber: value.permanentAccountNumber,
            aadhaarNumber: value.aadhaarNumber,
            currentAddressYear: value.currentAddressYear,

            currentResidentialAddress: {
                addressLine1: value.currentAddressLine1,
                addressLine2: value.currentAddressLine2,
                locality: value.currentLocation,
                pinCode: value.currentPinCode,
                city: value.currentCity,
                state: value.currentState
            },
            permanentResidentialAddressSameAsCurrent: true,
            permanentResidentialAddress: {
                addressLine1: value.permanentAddressLine1,
                addressLine2: value.permanentAddressLine2,
                locality: value.permanentLocation,
                pinCode: value.permanentPinCode,
                city: value.permanentCity,
                state: value.permanentState
            }
        }
        if (isReapply) {
            $debug('setting personal details for reapply');
            yield setDetailsIsReapply.$execute(setDetailsData);
            $debug('done setting personal details reapply');
        }
        else {
            $debug('setting personal details');
            yield setDetails.$execute(setDetailsData);
            $debug('done setting personal details');
        }
        const appDetails = yield updateApplication.$execute({
            sourceReferenceId: value.sourceReferenceId,
            sourceType: value.sourceType,
            systemChannel: value.systemChannel,
            heighestEducation: value.heighestEducation,
            educationalInstitution: value.educationalInstitution,
            designation: value.designation,
            workExperience: value.workExperience,
            employerAddress: {
                addressLine1: value.workAddressLine1,
                addressLine2: value.workAddressLine2,
                locality: value.workLocation,
                pinCode: value.workPinCode,
                city: value.workCity,
                state: value.workState
            },
            employementYear: value.employementYear,

        });

        let initialOffer;
        try {
            $debug('generating initial offer');
            initialOffer = yield generateInitialOffer.$execute({});
            $debug('done generating initial offer');
            $debug('initial offer response is ', initialOffer);

            $debug('selecting initial offer');
            yield selectInitialOffer.$execute({});
            $debug('done selecting initial offer');

            $debug('setting education details');
            yield setEducation.$execute({
                heighestEducation: value.heighestEducation,
                educationalInstitution: value.educationalInstitution
            });
            $debug('done setting education details');

            $debug('setting employment details');
            yield setEmployment.$execute({
                designation: value.designation,
                workExperience: value.workExperience,
                employerAddress: {
                    addressLine1: value.workAddressLine1,
                    addressLine2: value.workAddressLine2,
                    locality: value.workLocation,
                    pinCode: value.workPinCode,
                    city: value.workCity,
                    state: value.workState
                },
                employementYear: value.employementYear,
            });
            $debug('done setting employment details');
        } catch (e) {
            // case 1: Initial Offer Rejected / Failed
            if (e && (e === 'Rejected' || e === 'Failed')) {
                throw {
                    code: 400,
                    message: `Initial Offer got ${e}`
                }
            }

            // case 2: Some exception came in generating Initial Offer
            if (e && e.code === 424) {
                if (e.error && e.error.body) {
                    const { status } = e.error;
                    throw {
                        code: status.code,
                        message: status.text
                    }
                }
            }
        }

        const response = Object.assign({}, appDetails, initialOffer);

        return response;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || e.details[0].message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];