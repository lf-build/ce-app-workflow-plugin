const schema = {
    mobileNumber: {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
      }
};

function* set({
    $api,
    $configuration,
    value: {
        mobileNumber
    },
    $debug
}) {
    const {
        simulation: simulationEndpoint
    } = yield $configuration('endpoints');

    const payload = {
        "SearchParameters": {
            "//DCRequest/Fields/Field[@TelePhoneNumber1]": "Value('" + mobileNumber + "')"

        },
        "MinimumMatchRequiredForSearch": 1,
        "IsXml": true,
        "Document": "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><Execu" +
            "teXMLStringResponse xmlns=\"http://tempuri.org/\"><ExecuteXMLStringResult>&lt;DC" +
            "Response&gt; 	&lt;Status&gt;Success&lt;/Status&gt; 	&lt;Authentication&gt;	&lt;S" +
            "tatus&gt;Success&lt;/Status&gt;	&lt;Token&gt;3a3e9ae6-2313-4e28-8a6d-af6f14a7473" +
            "3&lt;/Token&gt; 	&lt;/Authentication&gt; 	&lt;ResponseInfo&gt;	&lt;ApplicationId" +
            "&gt;9822532&lt;/ApplicationId&gt;	&lt;SolutionSetInstanceId&gt;6ccfb7cb-7c49-43a" +
            "0-9077-4632be8fd308&lt;/SolutionSetInstanceId&gt;	&lt;CurrentQueue&gt;&lt;/Curre" +
            "ntQueue&gt; 	&lt;/ResponseInfo&gt; 	&lt;ContextData&gt;	&lt;Field key=\"TUEF_Sta" +
            "tus\"&gt;S&lt;/Field&gt;	&lt;Field key=\"TUEF_ResponseXML\"&gt;&amp;lt;CreditRep" +
            "ort&amp;gt;	&amp;lt;Header&amp;gt;	&amp;lt;SegmentTag&amp;gt;TUEF&amp;lt;/Segmen" +
            "tTag&amp;gt;	&amp;lt;Version&amp;gt;12&amp;lt;/Version&amp;gt;	&amp;lt;Reference" +
            "Number&amp;gt;9666712&amp;lt;/ReferenceNumber&amp;gt;	&amp;lt;MemberCode&amp;gt;" +
            "NB78271001_UAT&amp;lt;/MemberCode&amp;gt;	&amp;lt;SubjectReturnCode&amp;gt;1&amp" +
            ";lt;/SubjectReturnCode&amp;gt;	&amp;lt;EnquiryControlNumber&amp;gt;001000182059&" +
            "amp;lt;/EnquiryControlNumber&amp;gt;	&amp;lt;DateProcessed&amp;gt;13102016&amp;l" +
            "t;/DateProcessed&amp;gt;	&amp;lt;TimeProcessed&amp;gt;152152&amp;lt;/TimeProcess" +
            "ed&amp;gt;	&amp;lt;/Header&amp;gt;	&amp;lt;NameSegment&amp;gt;	&amp;lt;Length&am" +
            "p;gt;03&amp;lt;/Length&amp;gt;	&amp;lt;SegmentTag&amp;gt;N01&amp;lt;/SegmentTag&" +
            "amp;gt;	&amp;lt;ConsumerName1FieldLength&amp;gt;05&amp;lt;/ConsumerName1FieldLen" +
            "gth&amp;gt;	&amp;lt;ConsumerName1&amp;gt;ARUNA&amp;lt;/ConsumerName1&amp;gt;	&am" +
            "p;lt;ConsumerName2FieldLength&amp;gt;01&amp;lt;/ConsumerName2FieldLength&amp;gt;" +
            "	&amp;lt;ConsumerName2&amp;gt;R&amp;lt;/ConsumerName2&amp;gt;	&amp;lt;ConsumerNa" +
            "me3FieldLength&amp;gt;11&amp;lt;/ConsumerName3FieldLength&amp;gt;	&amp;lt;Consum" +
            "erName3&amp;gt;KUMARI&amp;lt;/ConsumerName3&amp;gt;	&amp;lt;DateOfBirthFieldLeng" +
            "th&amp;gt;08&amp;lt;/DateOfBirthFieldLength&amp;gt;	&amp;lt;DateOfBirth&amp;gt;0" +
            "9111984&amp;lt;/DateOfBirth&amp;gt;	&amp;lt;GenderFieldLength&amp;gt;02&amp;lt;/" +
            "GenderFieldLength&amp;gt;	&amp;lt;Gender&amp;gt;1&amp;lt;/Gender&amp;gt;	&amp;lt" +
            ";/NameSegment&amp;gt;	&amp;lt;IDSegment&amp;gt;	&amp;lt;Length&amp;gt;03&amp;lt;" +
            "/Length&amp;gt;	&amp;lt;SegmentTag&amp;gt;I01&amp;lt;/SegmentTag&amp;gt;	&amp;lt" +
            ";IDType&amp;gt;01&amp;lt;/IDType&amp;gt;	&amp;lt;IDNumberFieldLength&amp;gt;10&a" +
            "mp;lt;/IDNumberFieldLength&amp;gt;	&amp;lt;IDNumber&amp;gt;CAQPS1183J&amp;lt;/ID" +
            "Number&amp;gt;	&amp;lt;/IDSegment&amp;gt;	&amp;lt;TelephoneSegment&amp;gt;	&amp;" +
            "lt;Length&amp;gt;03&amp;lt;/Length&amp;gt;	&amp;lt;SegmentTag&amp;gt;T01&amp;lt;" +
            "/SegmentTag&amp;gt;	&amp;lt;TelephoneNumberFieldLength&amp;gt;10&amp;lt;/Telepho" +
            "neNumberFieldLength&amp;gt;	&amp;lt;TelephoneNumber&amp;gt;9820831574&amp;lt;/Te" +
            "lephoneNumber&amp;gt;	&amp;lt;TelephoneType&amp;gt;01&amp;lt;/TelephoneType&amp;" +
            "gt;	&amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/EnrichedThroughEnquiry&amp;g" +
            "t;	&amp;lt;/TelephoneSegment&amp;gt;	&amp;lt;EmailContactSegment&amp;gt;	&amp;lt" +
            ";Length&amp;gt;03&amp;lt;/Length&amp;gt;	&amp;lt;SegmentTag&amp;gt;C01&amp;lt;/S" +
            "egmentTag&amp;gt;	&amp;lt;EmailIDFieldLength&amp;gt;35&amp;lt;/EmailIDFieldLengt" +
            "h&amp;gt;	&amp;lt;EmailID&amp;gt;aruna@gmail.com&amp;lt;/EmailID&amp;gt;	&amp;lt" +
            ";/EmailContactSegment&amp;gt;	&amp;lt;ScoreSegment&amp;gt;	&amp;lt;Length&amp;gt" +
            ";10&amp;lt;/Length&amp;gt;	&amp;lt;ScoreName&amp;gt;CIBILTUSC2&amp;lt;/ScoreName" +
            "&amp;gt;	&amp;lt;ScoreCardName&amp;gt;04&amp;lt;/ScoreCardName&amp;gt;	&amp;lt;S" +
            "coreCardVersion&amp;gt;10&amp;lt;/ScoreCardVersion&amp;gt;	&amp;lt;ScoreDate&amp" +
            ";gt;13102016&amp;lt;/ScoreDate&amp;gt;	&amp;lt;Score&amp;gt;00850&amp;lt;/Score&" +
            "amp;gt;	&amp;lt;ReasonCode1FieldLength&amp;gt;02&amp;lt;/ReasonCode1FieldLength&" +
            "amp;gt;	&amp;lt;ReasonCode1&amp;gt;17&amp;lt;/ReasonCode1&amp;gt;	&amp;lt;/Score" +
            "Segment&amp;gt;	&amp;lt;Address&amp;gt;	&amp;lt;AddressSegmentTag&amp;gt;PA&amp;" +
            "lt;/AddressSegmentTag&amp;gt;	&amp;lt;Length&amp;gt;03&amp;lt;/Length&amp;gt;	&a" +
            "mp;lt;SegmentTag&amp;gt;A01&amp;lt;/SegmentTag&amp;gt;	&amp;lt;AddressLine1Field" +
            "Length&amp;gt;37&amp;lt;/AddressLine1FieldLength&amp;gt;	&amp;lt;AddressLine1&am" +
            "p;gt;B-920 CENTRE POINT APARTMENTS NEXT TO TMC DISTRICT&amp;lt;/AddressLine1&amp" +
            ";gt;	&amp;lt;AddressLine5FieldLength&amp;gt;06&amp;lt;/AddressLine5FieldLength&a" +
            "mp;gt;	&amp;lt;AddressLine5&amp;gt;BANGALORE&amp;lt;/AddressLine5&amp;gt;	&amp;l" +
            "t;StateCode&amp;gt;29&amp;lt;/StateCode&amp;gt;	&amp;lt;PinCodeFieldLength&amp;g" +
            "t;06&amp;lt;/PinCodeFieldLength&amp;gt;	&amp;lt;PinCode&amp;gt;560076&amp;lt;/Pi" +
            "nCode&amp;gt;	&amp;lt;AddressCategory&amp;gt;02&amp;lt;/AddressCategory&amp;gt;	" +
            "&amp;lt;DateReported&amp;gt;10082016&amp;lt;/DateReported&amp;gt;	&amp;lt;Enrich" +
            "edThroughEnquiry&amp;gt;Y&amp;lt;/EnrichedThroughEnquiry&amp;gt;	&amp;lt;/Addres" +
            "s&amp;gt;	&amp;lt;Address&amp;gt;	&amp;lt;AddressSegmentTag&amp;gt;PA&amp;lt;/Ad" +
            "dressSegmentTag&amp;gt;	&amp;lt;Length&amp;gt;03&amp;lt;/Length&amp;gt;	&amp;lt;" +
            "SegmentTag&amp;gt;A02&amp;lt;/SegmentTag&amp;gt;	&amp;lt;AddressLine1FieldLength" +
            "&amp;gt;18&amp;lt;/AddressLine1FieldLength&amp;gt;	&amp;lt;AddressLine1&amp;gt;B" +
            "-920 CENTRE POINT APARTMENTS NEXT TO TMC DISTRICT&amp;lt;/AddressLine1&amp;gt;	&" +
            "amp;lt;AddressLine2FieldLength&amp;gt;09&amp;lt;/AddressLine2FieldLength&amp;gt;" +
            "	&amp;lt;AddressLine2&amp;gt;MAHALAXMI&amp;lt;/AddressLine2&amp;gt;	&amp;lt;Addr" +
            "essLine5FieldLength&amp;gt;06&amp;lt;/AddressLine5FieldLength&amp;gt;	&amp;lt;Ad" +
            "dressLine5&amp;gt;MUMBAI&amp;lt;/AddressLine5&amp;gt;	&amp;lt;StateCode&amp;gt;2" +
            "7&amp;lt;/StateCode&amp;gt;	&amp;lt;PinCodeFieldLength&amp;gt;06&amp;lt;/PinCode" +
            "FieldLength&amp;gt;	&amp;lt;PinCode&amp;gt;400013&amp;lt;/PinCode&amp;gt;	&amp;l" +
            "t;AddressCategory&amp;gt;01&amp;lt;/AddressCategory&amp;gt;	&amp;lt;DateReported" +
            "&amp;gt;10012016&amp;lt;/DateReported&amp;gt;	&amp;lt;EnrichedThroughEnquiry&amp" +
            ";gt;Y&amp;lt;/EnrichedThroughEnquiry&amp;gt;	&amp;lt;/Address&amp;gt;	&amp;lt;Ac" +
            "count&amp;gt;	&amp;lt;Length&amp;gt;04&amp;lt;/Length&amp;gt;	&amp;lt;SegmentTag" +
            "&amp;gt;T001&amp;lt;/SegmentTag&amp;gt;	&amp;lt;Account_Summary_Segment_Fields&a" +
            "mp;gt;	&amp;lt;ReportingMemberShortNameFieldLength&amp;gt;13&amp;lt;/ReportingMe" +
            "mberShortNameFieldLength&amp;gt;	&amp;lt;/Account_Summary_Segment_Fields&amp;gt;" +
            "	&amp;lt;Account_NonSummary_Segment_Fields&amp;gt;	&amp;lt;ReportingMemberShortN" +
            "ameFieldLength&amp;gt;13&amp;lt;/ReportingMemberShortNameFieldLength&amp;gt;	&am" +
            "p;lt;ReportingMemberShortName&amp;gt;NOT DISCLOSED&amp;lt;/ReportingMemberShortN" +
            "ame&amp;gt;	&amp;lt;AccountType&amp;gt;02&amp;lt;/AccountType&amp;gt;	&amp;lt;Ow" +
            "enershipIndicator&amp;gt;1&amp;lt;/OwenershipIndicator&amp;gt;	&amp;lt;DateOpene" +
            "dOrDisbursed&amp;gt;19032013&amp;lt;/DateOpenedOrDisbursed&amp;gt;	&amp;lt;DateO" +
            "fLastPayment&amp;gt;05122016&amp;lt;/DateOfLastPayment&amp;gt;	&amp;lt;DateRepor" +
            "tedAndCertified&amp;gt;25072013&amp;lt;/DateReportedAndCertified&amp;gt;	&amp;lt" +
            ";HighCreditOrSanctionedAmountFieldLength&amp;gt;05&amp;lt;/HighCreditOrSanctione" +
            "dAmountFieldLength&amp;gt;	&amp;lt;HighCreditOrSanctionedAmount&amp;gt;2500000&a" +
            "mp;lt;/HighCreditOrSanctionedAmount&amp;gt;	&amp;lt;CurrentBalanceFieldLength&am" +
            "p;gt;05&amp;lt;/CurrentBalanceFieldLength&amp;gt;	&amp;lt;CurrentBalance&amp;gt;" +
            "125455&amp;lt;/CurrentBalance&amp;gt;	&amp;lt;PaymentHistory1FieldLength&amp;gt;" +
            "117&amp;lt;/PaymentHistory1FieldLength&amp;gt;	&amp;lt;PaymentHistory1&amp;gt;00" +
            "00000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "00000000000000000000000000000000000&amp;lt;/PaymentHistory1&amp;gt;	&amp;lt;Paym" +
            "entHistoryStartDate&amp;gt;05122016&amp;lt;/PaymentHistoryStartDate&amp;gt;	&amp" +
            ";lt;PaymentHistoryEndDate&amp;gt;01042013&amp;lt;/PaymentHistoryEndDate&amp;gt;	" +
            "&amp;lt;RateOfInterestFieldLength&amp;gt;05&amp;lt;/RateOfInterestFieldLength&am" +
            "p;gt;	&amp;lt;RateOfInterest&amp;gt;14.75&amp;lt;/RateOfInterest&amp;gt;	&amp;lt" +
            ";RepaymentTenureFieldLength&amp;gt;02&amp;lt;/RepaymentTenureFieldLength&amp;gt;" +
            "	&amp;lt;RepaymentTenure&amp;gt;60&amp;lt;/RepaymentTenure&amp;gt;	&amp;lt;EmiAm" +
            "ountFieldLength&amp;gt;05&amp;lt;/EmiAmountFieldLength&amp;gt;	&amp;lt;EmiAmount" +
            "&amp;gt;11000&amp;lt;/EmiAmount&amp;gt;	&amp;lt;PaymentFrequency&amp;gt;03&amp;l" +
            "t;/PaymentFrequency&amp;gt;	&amp;lt;/Account_NonSummary_Segment_Fields&amp;gt;	&" +
            "amp;lt;/Account&amp;gt;	&amp;lt;Account&amp;gt;	&amp;lt;Length&amp;gt;04&amp;lt;" +
            "/Length&amp;gt;	&amp;lt;SegmentTag&amp;gt;T002&amp;lt;/SegmentTag&amp;gt;	&amp;l" +
            "t;Account_Summary_Segment_Fields&amp;gt;	&amp;lt;ReportingMemberShortNameFieldLe" +
            "ngth&amp;gt;13&amp;lt;/ReportingMemberShortNameFieldLength&amp;gt;	&amp;lt;/Acco" +
            "unt_Summary_Segment_Fields&amp;gt;	&amp;lt;Account_NonSummary_Segment_Fields&amp" +
            ";gt;	&amp;lt;ReportingMemberShortNameFieldLength&amp;gt;13&amp;lt;/ReportingMemb" +
            "erShortNameFieldLength&amp;gt;	&amp;lt;ReportingMemberShortName&amp;gt;NOT DISCL" +
            "OSED&amp;lt;/ReportingMemberShortName&amp;gt;	&amp;lt;AccountType&amp;gt;10&amp;" +
            "lt;/AccountType&amp;gt;	&amp;lt;OwenershipIndicator&amp;gt;1&amp;lt;/OwenershipI" +
            "ndicator&amp;gt;	&amp;lt;DateOpenedOrDisbursed&amp;gt;10112015&amp;lt;/DateOpene" +
            "dOrDisbursed&amp;gt;	&amp;lt;DateReportedAndCertified&amp;gt;31122015&amp;lt;/Da" +
            "teReportedAndCertified&amp;gt;	&amp;lt;HighCreditOrSanctionedAmountFieldLength&a" +
            "mp;gt;05&amp;lt;/HighCreditOrSanctionedAmountFieldLength&amp;gt;	&amp;lt;HighCre" +
            "ditOrSanctionedAmount&amp;gt;50000&amp;lt;/HighCreditOrSanctionedAmount&amp;gt;	" +
            "&amp;lt;CurrentBalanceFieldLength&amp;gt;01&amp;lt;/CurrentBalanceFieldLength&am" +
            "p;gt;	&amp;lt;CurrentBalance&amp;gt;0&amp;lt;/CurrentBalance&amp;gt;	&amp;lt;Amo" +
            "untOverdue&amp;gt;0&amp;lt;/AmountOverdue&amp;gt;	&amp;lt;PaymentHistory1FieldLe" +
            "ngth&amp;gt;03&amp;lt;/PaymentHistory1FieldLength&amp;gt;	&amp;lt;PaymentHistory" +
            "1&amp;gt;00000000000000000000000000000000000&amp;lt;/PaymentHistory1&amp;gt;	&am" +
            "p;lt;PaymentHistoryStartDate&amp;gt;01122016&amp;lt;/PaymentHistoryStartDate&amp" +
            ";gt;	&amp;lt;PaymentHistoryEndDate&amp;gt;21122015&amp;lt;/PaymentHistoryEndDate" +
            "&amp;gt;	&amp;lt;RateOfInterestFieldLength&amp;gt;05&amp;lt;/RateOfInterestField" +
            "Length&amp;gt;	&amp;lt;RateOfInterest&amp;gt;10.50&amp;lt;/RateOfInterest&amp;gt" +
            ";	&amp;lt;RepaymentTenureFieldLength&amp;gt;02&amp;lt;/RepaymentTenureFieldLengt" +
            "h&amp;gt;	&amp;lt;RepaymentTenure&amp;gt;14&amp;lt;/RepaymentTenure&amp;gt;	&amp" +
            ";lt;/Account_NonSummary_Segment_Fields&amp;gt;	&amp;lt;/Account&amp;gt;	&amp;lt;" +
            "Enquiry&amp;gt;	&amp;lt;Length&amp;gt;04&amp;lt;/Length&amp;gt;	&amp;lt;SegmentT" +
            "ag&amp;gt;I001&amp;lt;/SegmentTag&amp;gt;	&amp;lt;DateOfEnquiryFields&amp;gt;081" +
            "02016&amp;lt;/DateOfEnquiryFields&amp;gt;	&amp;lt;EnquiringMemberShortNameFieldL" +
            "ength&amp;gt;11&amp;lt;/EnquiringMemberShortNameFieldLength&amp;gt;	&amp;lt;Enqu" +
            "iringMemberShortName&amp;gt;NOT DISCLOSED&amp;lt;/EnquiringMemberShortName&amp;g" +
            "t;	&amp;lt;EnquiryPurpose&amp;gt;05&amp;lt;/EnquiryPurpose&amp;gt;	&amp;lt;Enqui" +
            "ryAmountFieldLength&amp;gt;06&amp;lt;/EnquiryAmountFieldLength&amp;gt;	&amp;lt;E" +
            "nquiryAmount&amp;gt;800000&amp;lt;/EnquiryAmount&amp;gt;	&amp;lt;/Enquiry&amp;gt" +
            ";	&amp;lt;End&amp;gt;	&amp;lt;SegmentTag&amp;gt;ES07&amp;lt;/SegmentTag&amp;gt;	" +
            "&amp;lt;TotalLength&amp;gt;0022969&amp;lt;/TotalLength&amp;gt;	&amp;lt;/End&amp;" +
            "gt;	&amp;lt;/CreditReport&gt;&lt;/Field&gt;	&lt;Field key=\"TUEF_ErrorResponse\"" +
            "&gt;&lt;/Field&gt;	&lt;Field key=\"VerificationResp\"&gt;&amp;lt;Output&amp;gt;&" +
            "amp;lt;data&amp;gt;	&amp;lt;ClientId&amp;gt;21&amp;lt;/ClientId&amp;gt;	&amp;lt;" +
            "ApplicationId&amp;gt;9822532&amp;lt;/ApplicationId&amp;gt;	&amp;lt;FirstName&amp" +
            ";gt;ARUNA&amp;lt;/FirstName&amp;gt;	&amp;lt;MiddleName&amp;gt;&amp;lt;/MiddleNam" +
            "e&amp;gt;	&amp;lt;LastName&amp;gt;KUMARI&amp;lt;/LastName&amp;gt;	&amp;lt;Gender" +
            "&amp;gt;Female&amp;lt;/Gender&amp;gt;	&amp;lt;DateofBirth&amp;gt;09111984&amp;lt" +
            ";/DateofBirth&amp;gt;	&amp;lt;PAN&amp;gt;CAQPS1183J&amp;lt;/PAN&amp;gt;	&amp;lt;" +
            "VoterId&amp;gt;UBV2099604&amp;lt;/VoterId&amp;gt;	&amp;lt;UID&amp;gt;89823094564" +
            "5&amp;lt;/UID&amp;gt;	&amp;lt;DrivingLicense&amp;gt;&amp;lt;/DrivingLicense&amp;" +
            "gt;	&amp;lt;RationCard&amp;gt;&amp;lt;/RationCard&amp;gt;	&amp;lt;Passport&amp;g" +
            "t;&amp;lt;/Passport&amp;gt;	&amp;lt;Telephone1&amp;gt;9820831547&amp;lt;/Telepho" +
            "ne1&amp;gt;	&amp;lt;Address1Line&amp;gt;B-920 CENTRE POINT APARTMENTS NEXT TO TM" +
            "C DISTRICT&amp;lt;/Address1Line&amp;gt;	&amp;lt;Address1Pincd&amp;gt;560076&amp;" +
            "lt;/Address1Pincd&amp;gt;	&amp;lt;Address1Statecd&amp;gt;29&amp;lt;/Address1Stat" +
            "ecd&amp;gt;	&amp;lt;Address1State&amp;gt;KARNATAKA&amp;lt;/Address1State&amp;gt;" +
            "	&amp;lt;Add1City&amp;gt;BANGALORE&amp;lt;/Add1City&amp;gt;	&amp;lt;Address2Line" +
            "&amp;gt;&amp;lt;/Address2Line&amp;gt;	&amp;lt;Address2Pincd&amp;gt;&amp;lt;/Addr" +
            "ess2Pincd&amp;gt;	&amp;lt;Address2Statecd&amp;gt;&amp;lt;/Address2Statecd&amp;gt" +
            ";	&amp;lt;Address2State&amp;gt;&amp;lt;/Address2State&amp;gt;	&amp;lt;/data&amp;" +
            "gt;	&amp;lt;DataSource&amp;gt;	&amp;lt;Source&amp;gt;CIR&amp;lt;/Source&amp;gt;	" +
            "&amp;lt;Name&amp;gt;	&amp;lt;FirstName&amp;gt;PROTYUSH&amp;lt;/FirstName&amp;gt;" +
            "	&amp;lt;MiddleName&amp;gt;R&amp;lt;/MiddleName&amp;gt;	&amp;lt;LastName&amp;gt;" +
            "SINHA  &amp;lt;/LastName&amp;gt;	&amp;lt;NameMatch&amp;gt;96.97&amp;lt;/NameMatc" +
            "h&amp;gt;	&amp;lt;/Name&amp;gt;	&amp;lt;Gender&amp;gt;Male&amp;lt;/Gender&amp;gt" +
            ";	&amp;lt;GenderMatch&amp;gt;true&amp;lt;/GenderMatch&amp;gt;	&amp;lt;DateofBirt" +
            "h&amp;gt;09111986&amp;lt;/DateofBirth&amp;gt;	&amp;lt;DateofBirthMatch&amp;gt;tr" +
            "ue&amp;lt;/DateofBirthMatch&amp;gt;	&amp;lt;PAN&amp;gt;ADZPR4147H&amp;lt;/PAN&am" +
            "p;gt;	&amp;lt;PANMatch&amp;gt;true&amp;lt;/PANMatch&amp;gt;	&amp;lt;VoterId&amp;" +
            "gt;URH0817686&amp;lt;/VoterId&amp;gt;	&amp;lt;VoterIdMatch&amp;gt;false&amp;lt;/" +
            "VoterIdMatch&amp;gt;	&amp;lt;PassportMatch&amp;gt;N/A&amp;lt;/PassportMatch&amp;" +
            "gt;	&amp;lt;DrivingLicenseMatch&amp;gt;N/A&amp;lt;/DrivingLicenseMatch&amp;gt;	&" +
            "amp;lt;AadharNumberMatch&amp;gt;N/A&amp;lt;/AadharNumberMatch&amp;gt;	&amp;lt;Ra" +
            "tionCardMatch&amp;gt;N/A&amp;lt;/RationCardMatch&amp;gt;	&amp;lt;Addresses&amp;g" +
            "t;	&amp;lt;Address&amp;gt;	&amp;lt;AddressLine1&amp;gt;501 FIFTH FLOOR PAM VILLE" +
            " DMONTE PARK    MUMBAI&amp;lt;/AddressLine1&amp;gt;	&amp;lt;PinCode&amp;gt;57007" +
            "6&amp;lt;/PinCode&amp;gt;	&amp;lt;StateCode&amp;gt;TAMIL NADU&amp;lt;/StateCode&" +
            "amp;gt;	&amp;lt;Match&amp;gt;100.0&amp;lt;/Match&amp;gt;	&amp;lt;Match1&amp;gt;3" +
            "0.0&amp;lt;/Match1&amp;gt;	&amp;lt;AddressCategory&amp;gt;02&amp;lt;/AddressCate" +
            "gory&amp;gt;	&amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/EnrichedThroughEnqu" +
            "iry&amp;gt;	&amp;lt;DateReported&amp;gt;10082016&amp;lt;/DateReported&amp;gt;	&a" +
            "mp;lt;/Address&amp;gt;	&amp;lt;Address&amp;gt;	&amp;lt;AddressLine1&amp;gt;ANNET" +
            " TECHNOLOGIES MAHALAXMI   MUMBAI&amp;lt;/AddressLine1&amp;gt;	&amp;lt;PinCode&am" +
            "p;gt;400013&amp;lt;/PinCode&amp;gt;	&amp;lt;StateCode&amp;gt;27&amp;lt;/StateCod" +
            "e&amp;gt;	&amp;lt;Match&amp;gt;0.0&amp;lt;/Match&amp;gt;	&amp;lt;Match1&amp;gt;0" +
            ".0&amp;lt;/Match1&amp;gt;	&amp;lt;AddressCategory&amp;gt;03&amp;lt;/AddressCateg" +
            "ory&amp;gt;	&amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/EnrichedThroughEnqui" +
            "ry&amp;gt;	&amp;lt;DateReported&amp;gt;10012016&amp;lt;/DateReported&amp;gt;	&am" +
            "p;lt;/Address&amp;gt;	&amp;lt;Address&amp;gt;	&amp;lt;AddressLine1&amp;gt;CHENNA" +
            "I CHENNAI   CHENNAI&amp;lt;/AddressLine1&amp;gt;	&amp;lt;PinCode&amp;gt;600002&a" +
            "mp;lt;/PinCode&amp;gt;	&amp;lt;StateCode&amp;gt;33&amp;lt;/StateCode&amp;gt;	&am" +
            "p;lt;Match&amp;gt;0.0&amp;lt;/Match&amp;gt;	&amp;lt;Match1&amp;gt;0.0&amp;lt;/Ma" +
            "tch1&amp;gt;	&amp;lt;AddressCategory&amp;gt;03&amp;lt;/AddressCategory&amp;gt;	&" +
            "amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/EnrichedThroughEnquiry&amp;gt;	&a" +
            "mp;lt;DateReported&amp;gt;18062015&amp;lt;/DateReported&amp;gt;	&amp;lt;/Address" +
            "&amp;gt;	&amp;lt;/Addresses&amp;gt;	&amp;lt;PhoneNumber&amp;gt;	&amp;lt;Telephon" +
            "e&amp;gt;	&amp;lt;Number&amp;gt;9874561230&amp;lt;/Number&amp;gt;	&amp;lt;Type&a" +
            "mp;gt;01&amp;lt;/Type&amp;gt;	&amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/En" +
            "richedThroughEnquiry&amp;gt;	&amp;lt;Match&amp;gt;NO_MATCH,NO_MATCH&amp;lt;/Matc" +
            "h&amp;gt;	&amp;lt;/Telephone&amp;gt;	&amp;lt;Telephone&amp;gt;	&amp;lt;Number&am" +
            "p;gt;66231356&amp;lt;/Number&amp;gt;	&amp;lt;Type&amp;gt;03&amp;lt;/Type&amp;gt;" +
            "	&amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/EnrichedThroughEnquiry&amp;gt;	" +
            "&amp;lt;Match&amp;gt;NO_MATCH,NO_MATCH&amp;lt;/Match&amp;gt;	&amp;lt;/Telephone&" +
            "amp;gt;	&amp;lt;Telephone&amp;gt;	&amp;lt;Number&amp;gt;9845614731&amp;lt;/Numbe" +
            "r&amp;gt;	&amp;lt;Type&amp;gt;01&amp;lt;/Type&amp;gt;	&amp;lt;EnrichedThroughEnq" +
            "uiry&amp;gt;Y&amp;lt;/EnrichedThroughEnquiry&amp;gt;	&amp;lt;Match&amp;gt;NO_MAT" +
            "CH,NO_MATCH&amp;lt;/Match&amp;gt;	&amp;lt;/Telephone&amp;gt;	&amp;lt;Telephone&a" +
            "mp;gt;	&amp;lt;Number&amp;gt;45614731&amp;lt;/Number&amp;gt;	&amp;lt;Type&amp;gt" +
            ";02&amp;lt;/Type&amp;gt;	&amp;lt;EnrichedThroughEnquiry&amp;gt;Y&amp;lt;/Enriche" +
            "dThroughEnquiry&amp;gt;	&amp;lt;Match&amp;gt;NO_MATCH,NO_MATCH&amp;lt;/Match&amp" +
            ";gt;	&amp;lt;/Telephone&amp;gt;	&amp;lt;/PhoneNumber&amp;gt;	&amp;lt;/DataSource" +
            "&amp;gt;&amp;lt;VoterId&amp;gt;	&amp;lt;Id&amp;gt;UBV2099604&amp;lt;/Id&amp;gt;	" +
            "&amp;lt;Source&amp;gt;Input&amp;lt;/Source&amp;gt;	&amp;lt;ErrorCode&amp;gt;S&am" +
            "p;lt;/ErrorCode&amp;gt;	&amp;lt;Name&amp;gt;Manoj Chander &amp;lt;/Name&amp;gt;	" +
            "&amp;lt;NameMatch&amp;gt;53.57&amp;lt;/NameMatch&amp;gt;	&amp;lt;GuardianName&am" +
            "p;gt;Ramesh Chander &amp;lt;/GuardianName&amp;gt;	&amp;lt;GuardianType&amp;gt;F&" +
            "amp;lt;/GuardianType&amp;gt;	&amp;lt;Age&amp;gt;23&amp;lt;/Age&amp;gt;	&amp;lt;G" +
            "ender&amp;gt;M&amp;lt;/Gender&amp;gt;	&amp;lt;GenderMatch&amp;gt;false&amp;lt;/G" +
            "enderMatch&amp;gt;	&amp;lt;AddressLine&amp;gt;P-266/24  MEHRAULI   MEHRAULI Delh" +
            "i Section No &amp;amp;amp; Name -1- WARD 2 MEHRAULI,DELHI&amp;lt;/AddressLine&am" +
            "p;gt;	&amp;lt;AddressPincode&amp;gt;110030&amp;lt;/AddressPincode&amp;gt;	&amp;l" +
            "t;AddressState&amp;gt;Delhi&amp;lt;/AddressState&amp;gt;	&amp;lt;AddressMatch&am" +
            "p;gt;0.0&amp;lt;/AddressMatch&amp;gt;	&amp;lt;TimeTrail&amp;gt;GetDBRecordsTime:" +
            "2586.6388|OETime:43.0665|NameMatchTime:1.4474&amp;lt;/TimeTrail&amp;gt;	&amp;lt;" +
            "/VoterId&amp;gt; &amp;lt;VoterId&amp;gt;	&amp;lt;Id&amp;gt;URH0817686&amp;lt;/Id" +
            "&amp;gt;	&amp;lt;Source&amp;gt;CIR&amp;lt;/Source&amp;gt;	&amp;lt;ErrorCode&amp;" +
            "gt;S&amp;lt;/ErrorCode&amp;gt;	&amp;lt;Name&amp;gt;SURENDAR DHARAMSOTHU&amp;lt;/" +
            "Name&amp;gt;	&amp;lt;NameMatch&amp;gt;0&amp;lt;/NameMatch&amp;gt;	&amp;lt;Guardi" +
            "anName&amp;gt;DHEENAIAH &amp;lt;/GuardianName&amp;gt;	&amp;lt;GuardianType&amp;g" +
            "t;F&amp;lt;/GuardianType&amp;gt;	&amp;lt;Age&amp;gt;34&amp;lt;/Age&amp;gt;	&amp;" +
            "lt;Gender&amp;gt;M&amp;lt;/Gender&amp;gt;	&amp;lt;GenderMatch&amp;gt;false&amp;l" +
            "t;/GenderMatch&amp;gt;	&amp;lt;AddressLine&amp;gt;6-50  BANDARUPALLI  MULUGU WAR" +
            "ANGAL 1.Jeevantarao Palli,Block-6&amp;lt;/AddressLine&amp;gt;	&amp;lt;AddressPin" +
            "code&amp;gt;506343&amp;lt;/AddressPincode&amp;gt;	&amp;lt;AddressState&amp;gt;An" +
            "dhra Pradesh&amp;lt;/AddressState&amp;gt;	&amp;lt;AddressMatch&amp;gt;0.0&amp;lt" +
            ";/AddressMatch&amp;gt;	&amp;lt;TimeTrail&amp;gt;GetDBRecordsTime:2586.6388|OETim" +
            "e:43.0665|NameMatchTime:1.4474&amp;lt;/TimeTrail&amp;gt;	&amp;lt;/VoterId&amp;gt" +
            ";&amp;lt;AadhaarIdBA&amp;gt;	&amp;lt;Id&amp;gt;898230945645&amp;lt;/Id&amp;gt;	&" +
            "amp;lt;Source&amp;gt;Input&amp;lt;/Source&amp;gt;	&amp;lt;ErrorCode&amp;gt;S&amp" +
            ";lt;/ErrorCode&amp;gt;	&amp;lt;POI&amp;gt;998-N&amp;lt;/POI&amp;gt;	&amp;lt;POA&" +
            "amp;gt;998-N&amp;lt;/POA&amp;gt;	&amp;lt;PFA&amp;gt;998-N&amp;lt;/PFA&amp;gt;	&a" +
            "mp;lt;TimeTrail&amp;gt;POITimeTaken:6365.6534|POATimeTaken:1593.149|PFATimeTaken" +
            ":1599.5903&amp;lt;/TimeTrail&amp;gt;	&amp;lt;/AadhaarIdBA&amp;gt;	&amp;lt;PAN&am" +
            "p;gt;	&amp;lt;PANName&amp;gt;Aruna Kumari&amp;lt;/PANName&amp;gt;	&amp;lt;PANNam" +
            "eMatch&amp;gt;100.0&amp;lt;/PANNameMatch&amp;gt;	&amp;lt;PANNo&amp;gt;CAQPS1183J" +
            "&amp;lt;/PANNo&amp;gt;	&amp;lt;PANNoMatch&amp;gt;100.0&amp;lt;/PANNoMatch&amp;gt" +
            ";	&amp;lt;/PAN&amp;gt;&amp;lt;CIBIL&amp;gt;	&amp;lt;CibilName&amp;gt;&amp;lt;/Ci" +
            "bilName&amp;gt;	&amp;lt;CibilNameMatch&amp;gt;&amp;lt;/CibilNameMatch&amp;gt;	&a" +
            "mp;lt;CIBILGender&amp;gt;&amp;lt;/CIBILGender&amp;gt;	&amp;lt;CIBILGenderMatch&a" +
            "mp;gt;&amp;lt;/CIBILGenderMatch&amp;gt;	&amp;lt;CIBILPassport&amp;gt;&amp;lt;/CI" +
            "BILPassport&amp;gt;	&amp;lt;CIBILPassportMatch&amp;gt;&amp;lt;/CIBILPassportMatc" +
            "h&amp;gt;	&amp;lt;CIBILDrivingLicense&amp;gt;&amp;lt;/CIBILDrivingLicense&amp;gt" +
            ";	&amp;lt;CIBILDrivingLicenseMatch&amp;gt;&amp;lt;/CIBILDrivingLicenseMatch&amp;" +
            "gt;	&amp;lt;CIBILAadharNumber&amp;gt;&amp;lt;/CIBILAadharNumber&amp;gt;	&amp;lt;" +
            "CIBILAadharNumberMatch&amp;gt;&amp;lt;/CIBILAadharNumberMatch&amp;gt;	&amp;lt;CI" +
            "BILRationCard&amp;gt;&amp;lt;/CIBILRationCard&amp;gt;	&amp;lt;CIBILRationCardMat" +
            "ch&amp;gt;&amp;lt;/CIBILRationCardMatch&amp;gt;	&amp;lt;CIBILVoterID&amp;gt;&amp" +
            ";lt;/CIBILVoterID&amp;gt;	&amp;lt;CIBILVoterIDMatch&amp;gt;&amp;lt;/CIBILVoterID" +
            "Match&amp;gt;	&amp;lt;CibilAddress1&amp;gt;&amp;lt;/CibilAddress1&amp;gt;	&amp;l" +
            "t;CibilAddress1Match&amp;gt;&amp;lt;/CibilAddress1Match&amp;gt;	&amp;lt;CibilAdd" +
            "ress2&amp;gt;&amp;lt;/CibilAddress2&amp;gt;	&amp;lt;CibilAddress2Match&amp;gt;&a" +
            "mp;lt;/CibilAddress2Match&amp;gt;	&amp;lt;CibilPhone1&amp;gt;&amp;lt;/CibilPhone" +
            "1&amp;gt;	&amp;lt;CibilPhone1Match&amp;gt;&amp;lt;/CibilPhone1Match&amp;gt;	&amp" +
            ";lt;CibilPhone2&amp;gt;&amp;lt;/CibilPhone2&amp;gt;	&amp;lt;CibilPhone2Match&amp" +
            ";gt;&amp;lt;/CibilPhone2Match&amp;gt;	&amp;lt;CibilPhone3&amp;gt;&amp;lt;/CibilP" +
            "hone3&amp;gt;	&amp;lt;CibilPhone3Match&amp;gt;&amp;lt;/CibilPhone3Match&amp;gt;	" +
            "&amp;lt;CibilPhone4&amp;gt;&amp;lt;/CibilPhone4&amp;gt;	&amp;lt;CibilPhone4Match" +
            "&amp;gt;&amp;lt;/CibilPhone4Match&amp;gt;	&amp;lt;CibilDOB&amp;gt;&amp;lt;/Cibil" +
            "DOB&amp;gt;	&amp;lt;CibilDOBMatch&amp;gt;0&amp;lt;/CibilDOBMatch&amp;gt;	&amp;lt" +
            ";/CIBIL&amp;gt;&amp;lt;/Output&gt;&lt;/Field&gt;	&lt;Field key=\"NTC_Score\"&gt;" +
            "&lt;/Field&gt;	&lt;Field key=\"GroupSegment\"&gt;&amp;lt;ErrorSegment&amp;gt;&am" +
            "p;lt;Response&amp;gt;&amp;lt;/Response&amp;gt;&amp;lt;/ErrorSegment&gt;&lt;/Fiel" +
            "d&gt;	&lt;Field key=\"CIBIL_Detect_Response_XML\"&gt;&lt;/Field&gt;	&lt;Field ke" +
            "y=\"CIBIL_Detect_ErrorResponse\"&gt;&lt;/Field&gt; 	&lt;/ContextData&gt; &lt;/DC" +
            "Response&gt;</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Env" +
            "elope>"
    };

    const simulatedResponse = yield $api.put(`${simulationEndpoint}/cibil/add`, payload);
    return simulatedResponse;
};

module.exports = [schema, set];