const schema = {    
    applicationNumber: 'string:min=2,max=100,required'
};


function* set({
    value: {        
        applicationNumber
    },
    $api,
    $debug,
    $configuration,
}) {
    
    try {
        const {
            'application-filters': applicationFiltersEndpoint
        } = yield $configuration('endpoints');
        
        const response = yield $api.get(`${applicationFiltersEndpoint}/getapplicationtaginfo/${applicationNumber}`);
        return response && response.body;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];