const {
    dbAction
} = require('@sigma-infosolutions/orbit-base/storage');

const schema = {
    'applicationNumber': 'string: min=7, max=7, required',
    'requestedAmount': 'number:min=50000,max=1500000',
    'purposeOfLoan': {
        '@type': 'string',
        valid: ['travel', 'vehiclepurchase', 'medical', 'loanrefinancing', 'wedding', 'homeimprovement', 'business', 'education', 'assetacquisition', 'agriculture', 'other'],
        // required: true
    },
    'otherPurposeDescription': 'string: min=2, max=100',
    'salutation': {
        '@type': 'string',
        valid: [
            'Mr', 'Mrs', 'Ms'
        ],
        // required: true
    },
    'maritalStatus': {
        '@type': 'string',
        valid: [
            'single', 'married'
        ],
        // required: true
    },
    'firstName': 'string: min=1, max=100',
    'middleName': 'string: min=1, max=100',
    'lastName': 'string: min=1, max=100',
    'personalEmail': 'email: min=2, max=100',
    'personalMobile': {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    },
    'employerName': 'string: min=2, max=100, required',
    'cinNumber': 'string: min=2, max=100',
    'workEmail': 'email: max=100, required',
    'income': 'number: min=0, max=1000000, required',
    'residenceType': {
        '@type': 'string',
        valid: ['OwnedSelf', 'Owned-Family', 'Rented-Family', 'Rented-Alone', 'Rented-Friends', 'PG-Hostel', 'Company-Acco'],
        required: true
    },
    'monthlyRent': 'number: min=0, max=1000000',
    'homeLoanEmi': 'number: min=0, max=1000000',

    'dateOfBirth': 'string: min=0, max=100, required',
    'aadhaarNumber': 'string: min=12, max=12, required',
    'permanentAccountNumber': 'string: min=10, max=10, required',

    'currentAddressLine1': 'string: min=3, max=40, required',
    'currentAddressLine2': 'string: min=0, max=40',
    'currentCity': 'string: min=3, max=40, required',
    'currentLocation': 'string: min=3, max=40, required',
    'currentPinCode': 'string: min=6, max=6, required',
    'currentState': 'string: min=3, max=40, required',

    'permanentAddressLine1': 'string: min=3, max=40, required',
    'permanentAddressLine2': 'string: min=0, max=40',
    'permanentLocation': 'string: min=3, max=40, required',
    'permanentCity': 'string: min=3, max=40, required',
    'permanentPinCode': 'string: min=6, max=6, required',
    'permanentState': 'string: min=3, max=40, required',

    'heighestEducation': {
        '@type': 'string',
        valid: ['highschool', 'bachelordegree', 'masterdegree', 'doctratedegree'],
        required: true
    },
    'educationalInstitution': 'string:min=2,max=100,required',

    'designation': 'string:min=2,max=100,required',
    'workExperience': 'number:min=0,max=10,required',

    'workAddressLine1': 'string: min=3, max=40, required',
    'workAddressLine2': 'string: min=0, max=40',
    'workLocation': 'string: min=3, max=40, required',
    'workCity': 'string: min=3, max=40, required',
    'workPinCode': 'string: min=6, max=6, required',
    'workState': 'string: min=3, max=40, required',

    'trackingCode': 'string: min=1,max=250',
    'trackingCodeMedium': 'string: min=1,max=250',

    'trackingCodeCampaign': 'string: min=1,max=250',
    'trackingCodeTerm': 'string: min=1,max=250',
    'trackingCodeContent': 'string: min=1,max=250',

    'sodexoCode': 'string: min=1,max=250',

    'currentAddressYear': 'string:min=0 max=2',
    'employementYear': 'string:min=0 max=2',

    'sourceReferenceId': 'string: min=0, max=100, required',
    'sourceType': 'string: min=0, max=100, required',
    'systemChannel': 'string: min=0, max=100, required',

    'fromNavigatore': 'boolean'
};

function* set({
    value,
    $api,
    $debug,
    $set,
    $configuration,
    $stages: {
        authorization: {
            identity: {
                check
            }
        },
        'on-boarding': {
            opportunity: {
                'set-amount': setAmount,
                'set-reason': setReason,
                'set-basic-information': setBasicInfo
            },
            'sign-up': {
                'set-mobile-for-crm': setMobile,
                'verify-otp-for-crm': verifyOtp
            },
            qualify: {
                'set-work-details': setWorkDetails,
                'set-residence-expenses': setResidenceExpenses,
                'set-details': setDetails
            },
            verify: {
                'set-education-for-crm': setEducation,
                'set-employment-for-crm': setEmployment
            },
            application: {
                'update-application-for-crm': updateApplication
            },
            offer: {
                'generate-initial': generateInitialOffer,
                'select-initial-offer': selectInitialOffer
            }
        }
    }
}) {

    try {

        const {
            'application-filters': applicationFiltersEndpoint,
            'application-processor': applicationProcessorEndpoint
        } = yield $configuration('endpoints');

        // checking if home loan EMI and monthly rent both are passed
        if (value.monthlyRent !== 0 && value.homeLoanEmi !== 0) {
            throw {
                code: 400,
                message: 'Please enter value for either homeLoanEmi or monthlyRent'
            }
        }

        // checking for duplicacy.

        // personal email.
        const dupEmail = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Email/${value.personalEmail}`);
        if (dupEmail && dupEmail.body && dupEmail.body.length > 0) {
            const appNos = dupEmail.body.reduce((pre, curr) => {
                pre.push(curr.applicationNumber);
                return pre;
            }, []);

            if (appNos && appNos.indexOf(value.applicationNumber) === -1) {
                throw {
                    code: 400,
                    message: 'Application with same email id already exists'
                }
            }
        }

        // pan number
        const dupPan = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Pan/${value.permanentAccountNumber}`);
        if (dupPan && dupPan.body && dupPan.body.length > 0) {
            const appNos = dupPan.body.reduce((pre, curr) => {
                pre.push(curr.applicationNumber);
                return pre;
            }, []);

            if (appNos && appNos.indexOf(value.applicationNumber) === -1) {
                throw {
                    code: 400,
                    message: 'Application with same permanent account number already exists',
                    appNos,
                }
            }
        }

        $debug(`Fetching the Lead Created details for Borrower: ${value.personalMobile}`);
        const readAllStates = (db) => db
            .collection('store')
            .find({
                'on-boarding.sign-up.mobile.mobileNumber': value.personalMobile
            })
            .sort({
                _id: -1
            })
            .toArray();
        let states = yield dbAction(readAllStates);

        if (states.length === 0) {
            throw {
                code: 404,
                message: `No application found with mobile number ${value.personalMobile}`
            }
        }

        let [persistedState] = states;
        console.log('Lead Created workflow Id ', persistedState._id);

        // getting amount
        const {
            opportunity,
            'sign-up': signUp
        } = persistedState['on-boarding'];

        const {
            amount,
            reason: {
                reason,
                reasonText
            },
            basicInformation: {
                title,
                maritalStatus,
                firstName,
                middleName,
                lastName,
                personalEmail
            }
        } = opportunity;

        const {
            signupStatus: {
                username,
                exsists
            },
            mobile: {
                mobileNumber,
                verification: {
                    status: mobileVerificationStatus,
                    date: mobileVerificationDate
                }
            },
            user,
            application: {
                applicationNumber,
                trackingCode,
                trackingCodeMedium,
                trackingCodeCampaign,
                trackingCodeTerm,
                trackingCodeContent,
                applicantId
            }
        } = signUp;

        if (value.applicationNumber !== applicationNumber) {
            throw {
                code: 400,
                message: `Application Number: ${value.applicationNumber}, is not associated with Mobile Number: ${value.personalMobile}`
            }
        }

        // function to get the optional field value or undefined so that proper value can be passed.
        const getOptionalFieldValue = (payloadValue, storeValue) => {
            if (payloadValue && payloadValue !== null) {
                return payloadValue;
            } else if (storeValue && storeValue !== null) {
                return storeValue;
            } else {
                return undefined;
            }
        }

        $debug('setting amount');
        yield setAmount.$execute({
            amount: value.requestedAmount || amount
        });
        $debug('done setting amount');

        $debug('setting reason');
        const resText = getOptionalFieldValue(value.otherPurposeDescription, reasonText);
        if (resText && resText !== null) {
            yield setReason.$execute({
                reason: value.purposeOfLoan || reason,
                reasonText: resText
            });
        } else {
            yield setReason.$execute({
                reason: value.purposeOfLoan || reason,
            });
        }
        $debug('done setting reason');

        $debug('setting basic information');
        const midName = getOptionalFieldValue(value.middleName, middleName);
        if (midName && midName !== null) {
            yield setBasicInfo.$execute({
                title: value.salutation || title,
                maritalStatus: value.maritalStatus || maritalStatus,
                firstName: value.firstName || firstName,
                middleName: midName,
                lastName: value.lastName || lastName,
                personalEmail: value.personalEmail || personalEmail,
            });
        } else {
            yield setBasicInfo.$execute({
                title: value.salutation || title,
                maritalStatus: value.maritalStatus || maritalStatus,
                firstName: value.firstName || firstName,
                lastName: value.lastName || lastName,
                personalEmail: value.personalEmail || personalEmail,
            });
        }
        $debug('done setting basic information');

        $debug('setting work details');
        yield setWorkDetails.$execute({
            employer: {
                cin: value.employerName,
                name: value.employerName,
            },
            monthlyTakeHomeSalary: value.income,
            officialEmail: value.workEmail
        });
        $debug('done setting work details');

        $debug('setting residence expenses');
        yield setResidenceExpenses.$execute({
            residenceType: value.residenceType,
            hasHomeLoan: value.homeLoanEmi !== 0 ? true : false,
            homeLoanEmi: value.homeLoanEmi,
            monthlyRent: value.monthlyRent
        });
        $debug('done setting residence expenses');

        $debug('setting personal details');
        yield setDetails.$execute({
            dateOfBirth: value.dateOfBirth,
            panNumber: value.permanentAccountNumber,
            aadhaarNumber: value.aadhaarNumber,
            currentResidentialAddress: {
                addressLine1: value.currentAddressLine1,
                addressLine2: value.currentAddressLine2,
                locality: value.currentLocation,
                pinCode: value.currentPinCode,
                city: value.currentCity,
                state: value.currentState
            },
            permanentResidentialAddressSameAsCurrent: true,
            permanentResidentialAddress: {
                addressLine1: value.permanentAddressLine1,
                addressLine2: value.permanentAddressLine2,
                locality: value.permanentLocation,
                pinCode: value.permanentPinCode,
                city: value.permanentCity,
                state: value.permanentState
            }
        });
        $debug('done setting personal details');

        const appDetails = yield updateApplication.$execute({
            sourceReferenceId: value.sourceReferenceId,
            sourceType: value.sourceType,
            systemChannel: value.systemChannel,
            heighestEducation: value.heighestEducation,
            educationalInstitution: value.educationalInstitution,
            designation: value.designation,
            workExperience: value.workExperience,
            employerAddress: {
                addressLine1: value.workAddressLine1,
                addressLine2: value.workAddressLine2,
                locality: value.workLocation,
                pinCode: value.workPinCode,
                city: value.workCity,
                state: value.workState
            },
            currentAddressYear: value.currentAddressYear,
            employementYear: value.employementYear,

        });

        let initialOffer;
        // when fromNavigatore don't have a value or false, then only generate initial offer
        // and accept initial offer call should happen.
        if (!value.fromNavigatore) {
            try {
                $debug('generating initial offer');
                initialOffer = yield generateInitialOffer.$execute({});
                $debug('done generating initial offer');
                $debug('initial offer response is ', initialOffer);

                $debug('selecting initial offer');
                yield selectInitialOffer.$execute({});
                $debug('done selecting initial offer');

                $debug('setting education details');
                yield setEducation.$execute({
                    heighestEducation: value.heighestEducation,
                    educationalInstitution: value.educationalInstitution
                });
                $debug('done setting education details');

                $debug('setting employment details');
                yield setEmployment.$execute({
                    designation: value.designation,
                    workExperience: value.workExperience,
                    employerAddress: {
                        addressLine1: value.workAddressLine1,
                        addressLine2: value.workAddressLine2,
                        locality: value.workLocation,
                        pinCode: value.workPinCode,
                        city: value.workCity,
                        state: value.workState
                    }
                });
                $debug('done setting employment details');
            } catch (e) {
                // case 1: Initial Offer Rejected / Failed
                if (e && (e === 'Rejected' || e === 'Failed')) {
                    throw {
                        code: 400,
                        message: `Initial Offer got ${e}`
                    }
                }

                // case 2: Some exception came in generating Initial Offer
                if (e && e.code === 424) {
                    if (e.error && e.error.body) {
                        const { status } = e.error;
                        throw {
                            code: status.code,
                            message: status.text
                        }
                    }
                }
            }
        }

        const response = Object.assign({}, appDetails, initialOffer);

        return response;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || e || e.details[0].message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];