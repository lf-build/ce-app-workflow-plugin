var schema = {
    pageNo: 'string:min=1,max=100,required',
    pageSize: 'string:min=1,max=100,required'
};

function* set({
    $api,
    value: {
        pageNo,
        pageSize
    },
    $debug,
    $configuration
}) {
    try {
        const {
            'saved-filters': savedFiltersEndpoint
        } = yield $configuration('endpoints');

        $debug(`calling - ${savedFiltersEndpoint}/application/Search/review?pageSize=${pageSize}&page=${pageNo}`);
        
        const response = yield $api.post(`${savedFiltersEndpoint}/application/Search/review?pageSize=${pageSize}&page=${pageNo}`, {
            'sorts': {
                'Submitted': -1
            }
        });

        $debug(`completed calling - ${savedFiltersEndpoint}/application/Search/review?pageSize=${pageSize}&page=${pageNo}`);
        
        return response && response.body;
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
}

module.exports = [schema, set];