const schema = {
    applicationNumber: 'string: min=7, max=7, required'
};

function* set({
    $api,
    $set,
    $setStatus,
    $configuration,
    $debug,
    value: {
        applicationNumber
    }
}) {

    const { perfios: perfiosEndpoint } = yield $configuration('endpoints');;
    let status = undefined;

    try {
        const response = yield $api.get(`${perfiosEndpoint}/application/${applicationNumber}/status`);
        status = response && response.body;
        $debug('Net Banking transaction status is : ', status);
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }

    return status;
};

module.exports = [schema, set];
