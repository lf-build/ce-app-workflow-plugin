const schema = {
    applicationNumber: 'string: min=7, max=7, required'
};

function* set({
    $api,
    $configuration,
    $debug,
    $set,
    value: {
        applicationNumber
    }
}) {
    const {
        lendo: lendoEndpoint,
    } = yield $configuration('endpoints');

    try {

        $debug(`${lendoEndpoint}/application/${applicationNumber}/${applicationNumber}/score`);
        const lendoResponse = yield $api.get(`${lendoEndpoint}/application/${applicationNumber}/${applicationNumber}/score`);

        $debug('Lendo initialized');

        return lendoResponse.body;

    } catch (e) {
        $debug('Lendo not initialied / failed', e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];