const schema = {
    applicationNumber: 'string:min=7,max=7,required',
    personalEmail: 'email: min=2, max=100, required',
    dateOfBirth: 'string: min=2, max=100, required',
    returnUrl: 'string: min=2, max=100, required'
};

function* set({
    $api,
    $set,
    $setStatus,
    $configuration,
    $debug,
    value: {
        applicationNumber,
        personalEmail,
        dateOfBirth,
        returnUrl
    }
}) {

    const { perfios: perfiosEndpoint } = yield $configuration('endpoints');

    const todaysDt = new Date();
    const to = `${todaysDt.getFullYear()}-${todaysDt.getMonth() < 9 ? '0' + (todaysDt.getMonth() + 1) : todaysDt.getMonth() + 1}`;
    const fromDt = todaysDt.setMonth(todaysDt.getMonth() - 7);
    const fDt = new Date(fromDt);
    // const from = `${fDt.getFullYear()}-${fDt.getMonth() < 8 ? '0' + (fDt.getMonth() + 2) : fDt.getMonth() + 2}`;

    let from = undefined;
    if (fDt.getMonth() < 8) {
        from = `${fDt.getFullYear()}-${'0' + (fDt.getMonth() + 2)}`;
    } else {
        // here this if will be executed on in case when from month based on current date is December,
        // this if will handle from date i.e. {year}-{month} - here month will come 13 if not handle by if.
        if (fDt.getMonth() === 11) {
            from = to.split('-')[0] + '-01';
        } else {
            from = `${fDt.getFullYear()}-${(fDt.getMonth() + 2)}`;
        }
    }

    let form = undefined;

    const startProcessPayload = {
        yearMonthFrom: from,
        yearMonthTo: to,
        EmailId: personalEmail,
        ReturnUrl: returnUrl,
        DOB: dateOfBirth
    }

    $debug('start process payload ', startProcessPayload);

    try {
        form = yield $api.post(`${perfiosEndpoint}/application/${applicationNumber}/start-process`, startProcessPayload);
        $debug('Net Banking form fetched');
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }

    return form && form.body;
};

module.exports = [schema, set];
