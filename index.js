const fs = require('fs');
const path = require('path');
const workflowRoot = path.join(__dirname, 'workflow') ;
const stages = fs.readdirSync(workflowRoot)
                   .map(stage => [stage, path.join(workflowRoot, stage)])
                   .filter(([stage, stageRoot]) => fs.statSync(stageRoot).isDirectory())
                   .reduce((a, [stage, stageRoot]) => {

                       a[stage] = fs.readdirSync(stageRoot)
                                .map(fact => [fact, path.join(stageRoot, fact)])
                                .filter(([fact, factRoot]) =>  fs.statSync(factRoot).isDirectory())
                                .reduce((a, [fact, factRoot]) => {
                                  a[fact]= fs.readdirSync(factRoot)
                                             .filter(action => action.endsWith('.js'))
                                             .map(action => [action.replace('.js', ''), path.join(factRoot, action)])
                                             .filter(([action, actionRoot]) => !action.startsWith('.') && !action.endsWith('.specs') && fs.statSync(actionRoot).isFile())
                                             .reduce((a, [action, actionRoot]) => {
                                                a[action] = require(actionRoot);
                                               return a;
                                             }, {});
                                  return a;
                                }, {});
                     return a;
                   }, {});

module.exports =  stages; // require('./workflow');
